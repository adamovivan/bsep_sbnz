import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LogFilterComponent } from './components/log-filter-view/log-filter.component';
import { AlarmViewComponent } from './components/alarm-view/alarm-view.component';
import { HomeComponent } from './components/template-view/home.component';
import { ReportComponent } from './components/report/report.component';

const routes: Routes = [
  { path: 'report', component: ReportComponent },
  { path: 'home', component: HomeComponent },
  { path: 'logs', component: LogFilterComponent },
  { path: 'alarms', component: AlarmViewComponent },
  { path: '**', redirectTo: '/home' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
