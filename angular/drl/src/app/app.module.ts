import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material.module';
import { SidenavComponent } from './sidenav/sidenav.component';
import { NavbarComponent } from './navbar/navbar.component';
import { HomeComponent } from './components/template-view/home.component';
import { CommonsService } from './services/commons.service';
import { DataService } from './services/data.service';
import { OperationComponent } from './components/operation/operation.component';
import { ConditionComponent } from './components/condition/condition.component';
import { HttpModule } from '@angular/http';
import { ReportComponent } from './components/report/report.component';
import { ReportService } from './services/report.service';
import { ToastrService } from 'ngx-toastr';
import { HttpClientModule } from '@angular/common/http';

import { LogFilterComponent } from './components/log-filter-view/log-filter.component';
import { LogTableComponent } from './components/log-table/log-table.component';
import { JsogService } from 'jsog-typescript';
import { AlarmViewComponent } from './components/alarm-view/alarm-view.component';
import { AlarmTableComponent } from './components/alarm-table/alarm-table.component';

location['apiUrl'] = 'https://localhost:8088';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SidenavComponent,
    NavbarComponent,
    OperationComponent,
    ConditionComponent,
    ReportComponent,
    LogFilterComponent,
    LogTableComponent,
    AlarmViewComponent,
    AlarmTableComponent
  ],
  imports: [
    HttpClientModule,
    MaterialModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpModule
  ],
  providers: [
    CommonsService,
    DataService,
    ReportService,
    ToastrService,
    DataService,
    JsogService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
