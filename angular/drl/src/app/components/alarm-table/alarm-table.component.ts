import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTable, MatTableDataSource } from '@angular/material';
import { DataService } from 'src/app/services/data.service';
import { Alarm } from '../model/alarm.model';

@Component({
  selector: 'alarm-table',
  templateUrl: './alarm-table.component.html',
  styleUrls: ['./alarm-table.component.scss']
})
export class AlarmTableComponent implements OnInit {

  displayedColumns: string[];
  dataSource: any;
  dataService: any;
  data = []

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatTable, { static: true }) table: MatTable<any>;

  constructor(dataService: DataService) {
    this.dataService = dataService
    this.displayedColumns = dataService.alarmFieldNames
    this.dataSource = new MatTableDataSource<Alarm>([]);
    dataService.getAlarms()
  }

  ngOnInit() {
    this.dataSource.paginator = this.paginator
    this.data = this.dataService.logs
  }

  ngAfterViewInit() {
    this.dataService.setAlarmTableRef(this.table)
    this.dataService.setAlarmTableDataSource(this.dataSource)
  }

  ngOnChanges() {
    this.table.renderRows()
  }

}
