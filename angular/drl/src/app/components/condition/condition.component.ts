import { Component, OnInit, Input, OnChanges, Output, EventEmitter } from '@angular/core';
import { Condition } from '../model/condition.model';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'condition',
  templateUrl: './condition.component.html',
  styleUrls: ['./condition.component.scss']
})
export class ConditionComponent implements OnInit{

  @Input() data: Condition;
  @Input() index: number;
  @Output() removedIndexEvent = new EventEmitter<number>();

  constructor(private dataService: DataService) {
  }

  ngOnInit() {
  }

  remove() {
    console.log("You removed condition:", this.data, "Index:", this.index)
    this.removedIndexEvent.emit(this.index)
  }

  getKeys(map) {
    return Array.from(map.keys());
  }

}
