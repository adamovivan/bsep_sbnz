import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DataService } from 'src/app/services/data.service';
import { CommonsService } from 'src/app/services/commons.service';
import { Log } from '../model/log.model';

@Component({
  selector: 'app-log-filter',
  templateUrl: './log-filter.component.html',
  styleUrls: ['./log-filter.component.scss']
})
export class LogFilterComponent implements OnInit {

  logFilterForm: FormGroup;
  logs: Log[];

  constructor(
    private _formBuilder: FormBuilder,
    private dataService: DataService,
    private commonsService: CommonsService) {
    this.logFilterForm = this._formBuilder.group({
      filterValue: [''],
      dateValue: [''],
      fieldName: [dataService.logFieldNames[0], Validators.required],
    })
  }

  ngOnInit() {
  }

  submit() {
    this.dataService.searchLogs(this.logFilterForm)
    console.log("Search form:", this.logFilterForm.value)
  }

}
