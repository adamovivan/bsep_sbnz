import { Component, OnInit, ViewChild, Input, AfterViewInit, OnChanges } from '@angular/core';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { DataService } from 'src/app/services/data.service';
import { Log } from '../model/log.model';
import { MatTable } from '@angular/material';


@Component({
  selector: 'log-table',
  templateUrl: './log-table.component.html',
  styleUrls: ['./log-table.component.scss']
})
export class LogTableComponent implements OnInit, AfterViewInit, OnChanges {

  displayedColumns: string[];
  dataSource: any;
  dataService: any;
  data = []

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatTable, { static: true }) table: MatTable<any>;

  constructor(dataService: DataService) {
    this.dataService = dataService
    this.displayedColumns = ['timeStamp', 'ipAddress', 'hostName', 'sourceName', 'processId', 'logSeverity', 'logType', 'content']
    this.dataSource = new MatTableDataSource<Log>([]);
  }

  ngOnInit() {
    this.dataSource.paginator = this.paginator
    this.data = this.dataService.logs
  }

  ngAfterViewInit() {
    this.dataService.setLogTableRef(this.table)
    this.dataService.setLogTableDataSource(this.dataSource)
  }

  ngOnChanges() {
    this.table.renderRows()
  }



}
