export class Alarm {
    public message: Date;
    public resolved: boolean;
    public resolvedAt: string;
    public resolvedBy: string;
    public username: string;
    public alarmSeverity: string;
    public alarmType: string;
}