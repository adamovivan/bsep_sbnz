export class Condition {
    public fieldName: string;
    public value: string;
    public conditionOperator: string;
    public logicalOperator: string;
    public valid: boolean;

    constructor(fieldName: string, value: string, conditionOperator: string,
        logicalOperator: string, valid: boolean) {
        this.fieldName = fieldName
        this.value = value;
        this.conditionOperator = conditionOperator;
        this.logicalOperator = logicalOperator;
        this.valid = valid;
    }

    public checkValidity(): boolean {
        if (this.fieldName != "" && this.value != "" && this.conditionOperator != "" && this.logicalOperator != "") {
            console.log("VALID:", this)
            this.valid = true
        } else {
            this.valid = false
        }
        return this.valid
    }

}