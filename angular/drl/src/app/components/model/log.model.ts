export class Log {
    public timestamp: Date;
    public ipAddress: string;
    public hostName: string;
    public sourceName: string;
    public processId: string;
    public logSeverity: string;
    public logType: string;
    public content: string;
}