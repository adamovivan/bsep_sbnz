
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { HttpResponse } from '@angular/common/http';
import { ReportService } from 'src/app/services/report.service';

//import * as CanvasJS from 'src\assets\canvas\canvasjs.min.js';
import * as CanvasJS from 'src/assets/canvas/canvasjs.min';

//declare var CanvasJS: any;

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss']
})
export class ReportComponent implements OnInit {

  totalLogs: number = -1;
  startDate: string = "2018-01-01";
  endDate: string = "2019-06-18";
  showButton: boolean;

  showHostChart: boolean;
  showSystemChart: boolean;
  showTotalChart: boolean;

  hostData: any;
  systemData: any;
  totalData: any;

  hostChart: any;
  systemChart: any;
  totalChart: any;

  constructor(private reportService: ReportService) {
   }

  ngOnInit() {
    this.showButton = true;

    this.showSystemChart = false;
    this.showHostChart = false;
    this.showTotalChart = false;

    this.hostData = [];
    this.systemData = [{label: 'zgadija', y:12}];
    this.totalData = [];

    // setTimeout(() => {
    //   console.log("asdsd");
    // }, 10000)

    this.totalChart = new CanvasJS.Chart("totalChartContainer", {
      
      animationEnabled: true,
      exportEnabled: true,
      title: {
        text: "Total logs Chart"
      },
      data: [{
        type: "pie",
        showInLegend: true,
			  legendText: "{indexLabel}",
        //indexLabel: "{label} {y}",
        dataPoints: this.totalData,
      }]
    });

    //this.totalChart.render();
      
    this.hostChart = new CanvasJS.Chart("hostChartContainer", {
      animationEnabled: true,
      exportEnabled: true,
      title: {
        text: "Host logs Chart"
      },
      data: [{
        type: "pie",
        showInLegend: true,
			  legendText: "{indexLabel}",
        dataPoints: this.hostData,
      }]
    });
    
    this.hostChart.render();


  this.systemChart = new CanvasJS.Chart("systemChartContainer", {
    animationEnabled: true,
    exportEnabled: true,
    title: {
      text: "System logs Chart"
    },
    data: [{
      type: "pie",
      showInLegend: true,
      legendText: "{indexLabel}",
      dataPoints: this.systemData,
    }]
  });
  
  this.systemChart.render();
}

  getReport(){

    this.showSystemChart = true;
    this.showHostChart = true;
    this.showTotalChart = true;

    this.hostData.splice(0,this.hostData.length);
    this.systemData.splice(0,this.systemData.length);
    this.totalData.splice(0,this.totalData.length);

    this.totalChart.render();
    this.hostChart.render();
    this.systemChart.render();
    
    if(this.startDate == null || this.endDate == null)
      return
      //this.toastrService.error("You did not select both dates!")
    else
    {
      if (this.startDate > this.endDate)
        return
        //this.toastrService.error("Your end date is before your start date. Please change that!")
      else
      {
        this.reportService.getReport(this.startDate, this.endDate).subscribe(
          response => {

              this.totalLogs = 15; //response['totalLogs'];
              
              this.hostData = Array.from(response['logPerHost']);
              this.systemData = Array.from(response['logsPerSystem']);  //response['logsPerSystem']
              //this.hostData = response['logPerHost'];

              this.prepareNewCharts();

              console.log(this.systemData);
              console.log(this.hostData);
            
          },
          err => { //this.toastrService.error("Invalid date format!");
            this.totalLogs = null;}
        );
      }
    };
  }

  prepareNewCharts(){
    this.systemChart.destroy();
    this.hostChart.destroy();

    this.systemChart = new CanvasJS.Chart("systemChartContainer", {
      animationEnabled: true,
      exportEnabled: true,
      title: {
        text: "System logs Chart"
      },
      data: [{
        type: "pie",
        showInLegend: true,
        legendText: "{indexLabel}",
        dataPoints: this.systemData,
      }]
      
    });

    this.hostChart = new CanvasJS.Chart("hostChartContainer", {
      animationEnabled: true,
      exportEnabled: true,
      title: {
        text: "Host logs Chart"
      },
      data: [{
        type: "pie",
        showInLegend: true,
			  legendText: "{indexLabel}",
        dataPoints: this.hostData,
      }]
    });

    this.hostChart.render();
    this.systemChart.render();
  }

  formWeek(dateString:string)
  {
    var start = dateString.split(",")[1]
    var end = dateString.split(",")[0]

    return start.split("T")[0] + "->" + end.split("T")[0];
  }
  
}
