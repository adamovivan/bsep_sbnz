import { Component, OnInit, AfterViewInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Condition } from '../model/condition.model';
import { DataService } from 'src/app/services/data.service';
import { CommonsService } from 'src/app/services/commons.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, AfterViewInit {

  ruleForm: FormGroup;
  conditions: Condition[];

  constructor(
    private _formBuilder: FormBuilder,
    private dataService: DataService,
    private commonsService: CommonsService) {

    this.ruleForm = this._formBuilder.group({
      name: ['', Validators.required],
      intervalValue: [''],
      intervalMeasure: ['s'],
      repeats: [''],
    })

  }

  ngOnInit() {
    this.conditions = []
  }

  ngAfterViewInit() {
    // this.conditions.push(new Condition('field1', 'val1', 'EQUAL_TO', 'AND', true))
    // this.conditions.push(new Condition('field2', 'val2', 'GREATER_THAN', 'NONE', true))
  }

  addCondition(condition: Condition) {
    // Check if operation selected
    this.conditions.push(condition)
  }

  removeItem(index: number) {
    // remove element at index position
    if (index > -1) {
      this.conditions.splice(index, 1);
      this.commonsService.openSnackBar('Rule removed!', 'Close')
    }
  }

  addConditionClick() {
    if (this.dataService.checkValidState(this.conditions)) {
      this.addCondition(new Condition('', '', 'EQUAL_TO', 'NONE', false))
      this.commonsService.openSnackBar('New condition added!', 'Close')
    } else {
      this.commonsService.openSnackBar('Conditions not filled or operator not selected!', 'Close')
    }
  }

  submit() {
    if (this.dataService.checkValidState(this.conditions) && this.ruleForm.valid) {
      console.log("RuleForm:", this.ruleForm.value, "Conditions:", this.conditions)
      this.dataService.sendForms(this.ruleForm, this.conditions).subscribe();
      // TODO handle response
      this.commonsService.openSnackBar('Form submitted!', 'Close')
      this.conditions = [new Condition('', '', 'EQUAL_TO', 'NONE', false)]
      this.ruleForm.reset({ intervalMeasure: 's' })
    } else {
      this.commonsService.openSnackBar('Conditions not filled or operator not selected!', 'Close')
    }
  }

}
