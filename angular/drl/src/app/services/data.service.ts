import { Injectable } from '@angular/core';
import { Condition } from '../components/model/condition.model';
import { Http, Headers, Response, RequestOptions, RequestMethod } from '@angular/http';
import { FormGroup } from '@angular/forms';
import { map } from 'rxjs/operators';
import { JsogService } from 'jsog-typescript';
import { Log } from '../components/model/log.model';
import { MatTable, MatTableDataSource } from '@angular/material';
import { HttpErrorResponse } from '@angular/common/http';
import { Alarm } from '../components/model/alarm.model';
import { CommonsService } from './commons.service';

export interface FieldSelection {
  value: string;
  viewValue: string;
}

export interface FieldGroup {
  disabled?: boolean;
  name: string;
  fieldNames: FieldSelection[];
}

@Injectable()
export class DataService {

  measureValues: string[];
  fieldNames: FieldGroup[];
  conditionOperators: any[];
  logicalOperators: any[];
  logFieldNames: string[];
  alarmFieldNames: string[];
  // Log view
  logTable: MatTable<any>;
  logDataSource: MatTableDataSource<Log>;
  logs: Log[];
  // Alarm view
  alarmTable: MatTable<any>;
  alarmDataSource: MatTableDataSource<Alarm>;
  alarms: Alarm[];

  constructor(
    private http: Http,
    private jsog: JsogService,
    private commonsService: CommonsService
  ) {
    this.init()
  }

  checkValidState(conditions: Condition[]): boolean {
    console.log('Check validity for array:', conditions)
    for (let i = 0; i < conditions.length; i++) {
      // If some of conditions is not filled
      if (conditions[i].checkValidity() == false)
        return false
      // If logical operator between conditions is NONE
      if (conditions[i].logicalOperator == 'NONE' && i != conditions.length - 1)
        return false
    }
    return true
  }

  searchLogs(searchForm: FormGroup) {
    return this.http.post(location['apiUrl'] + "/api/log/search", searchForm.value).pipe(map(
      (response: Response) => {
        const data = this.jsog.deserialize(response.json());
        return data;
      }
    )).subscribe(
      (data: Log[]) => {
        console.log(data)
        this.logs = data;
        this.logDataSource.data = data;
        this.logTable.renderRows()
      },
      (error: HttpErrorResponse) => {
        console.log("ERROR RESPONSE!")
        this.commonsService.openSnackBar("Error, search form might not be valid!", "Close")
        this.logDataSource.data = []
      }
    )
  }

  getAlarms() {
    return this.http.get(location['apiUrl'] + "/api/alarm").pipe(map(
      (response: Response) => {
        const data = this.jsog.deserialize(response.json());
        return data;
      }
    )).subscribe(
      (data: Alarm[]) => {
        console.log(data)
        this.alarms = data;
        this.alarmDataSource.data = data;
        this.alarmTable.renderRows()
      },
      (error: HttpErrorResponse) => {
        console.log("ERROR RESPONSE!")
        this.commonsService.openSnackBar("Error, search form might not be valid!", "Close")
        this.alarmDataSource.data = []
      }
    )
  }

  sendForms(ruleForm: FormGroup, conditions: Condition[]) {
    let obj = {
      name: ruleForm.value.name,
      intervalValue: ruleForm.value.intervalValue,
      intervalMeasure: ruleForm.value.intervalMeasure,
      repeats: ruleForm.value.repeats,
      conditions: conditions
    }
    console.log("To send:", obj)
    return this.http.post(location['apiUrl'] + "/api/createRule", obj).pipe(map(
      (response: Response) => {
        console.log('usao');
        return response;
      }
    ))
  }

  setLogTableDataSource(dataSource: MatTableDataSource<Log>) {
    this.logDataSource = dataSource
  }

  setLogTableRef(table: MatTable<any>) {
    this.logTable = table
  }

  setAlarmTableDataSource(dataSource: MatTableDataSource<Alarm>) {
    this.alarmDataSource = dataSource
  }

  setAlarmTableRef(table: MatTable<any>) {
    this.alarmTable = table
  }

  get measures() {
    return this.measureValues;
  }

  init() {
    this.alarmFieldNames = [
      'message',
      'resolved',
      'resolvedAt',
      'resolvedBy',
      'username',
      'alarmSeverity',
      'alarmType'
    ]
    this.logFieldNames = [
      'REGEX',
      'timeStamp',
      'ipAddress',
      'hostName',
      'sourceName',
      'processId',
      'logSeverity',
      'logType',
      'content',
    ]
    this.measureValues = ['s', 'm', 'h']
    this.fieldNames = [
      {
        name: 'Username',
        fieldNames: [
          {
            viewValue: 'Username',
            value: 'username'
          }
        ]
      },
      {
        name: 'Ip address',
        fieldNames: [
          {
            viewValue: 'Ip Address',
            value: 'ipAddress'
          }
        ]
      },
      {
        name: 'Timestamp',
        fieldNames: [
          {
            viewValue: 'Timestamp',
            value: 'timestamp'
          }
        ]
      },
      {
        name: 'Host name',
        fieldNames: [
          {
            viewValue: 'Host name',
            value: 'hostName'
          }
        ]
      },
      {
        name: 'Source name',
        fieldNames: [
          {
            viewValue: 'Source name',
            value: 'sourceName'
          }
        ]
      },
      {
        name: 'Proccess id',
        fieldNames: [
          {
            viewValue: 'Proccess id',
            value: 'proccessId'
          }
        ]
      },
      {
        name: 'Log severity',
        fieldNames: [
          {
            viewValue: 'Log severity',
            value: 'logSeverity'
          }
        ]
      },
      {
        name: 'Log type',
        fieldNames: [
          {
            viewValue: 'Log type',
            value: 'logType'
          }
        ]
      },
    ]
    this.conditionOperators = [
      {
        name: '!=',
        value: 'NOT_EQUAL_TO'
      },
      {
        name: '==',
        value: 'EQUAL_TO'
      },
      {
        name: '>',
        value: 'GREATER_THAN'
      },
      {
        name: '<',
        value: 'LESS_THAN'
      },
      {
        name: '>=',
        value: 'GREATER_THAN_OR_EQUAL_TO'
      },
      {
        name: '<=',
        value: 'LESS_THAN_OR_EQUAL_TO'
      }
    ]

    this.logicalOperators = [
      {
        name: 'AND',
        value: 'AND'
      },
      {
        name: 'OR',
        value: 'OR'
      },
      {
        name: 'NOT',
        value: 'NOT'
      },
      {
        name: 'NONE',
        value: 'NONE'
      }
    ]
  }


}
