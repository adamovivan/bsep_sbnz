import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Http, Response } from '@angular/http';
import { map } from 'rxjs/operators';
import { JsogService } from 'jsog-typescript';


@Injectable({
  providedIn: 'root'
})
export class ReportService {

  constructor(private http: Http, private jsog: JsogService) { }

  getReport(startDate: string, endDate: string){
    let request = "https://localhost:8088/api/log/report/" + startDate + "/" + endDate;
    //return this.http.get(request);
    return this.http.get(request).pipe(map(
      (response: Response) => {
        const data = this.jsog.deserialize(response.json());
        return data;
      }
    ))
  }
}
