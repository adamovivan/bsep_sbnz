import { BrowserModule } from '@angular/platform-browser';
import { MaterialModule } from './material.module';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { TokenInterceptorService } from './services/token-interceptor.service';
import { TokenUtilsService } from './services/token-utils.service';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SidenavComponent } from './sidenav/sidenav.component';
import { NavbarComponent } from './navbar/navbar.component';
import { CommonsService } from './services/commons.service';
import { HomeComponent } from './components/home/home.component';
import { TreeViewComponent } from './components/tree-view/tree-view.component'

import { ToastrModule } from 'ngx-toastr';
import { TreeService } from './services/tree.service';
import { HttpModule } from '@angular/http';
import { JsogService } from 'jsog-typescript';
import { UserService } from './services/user.service'; 

import { TreeFormComponent } from './components/tree-form/tree-form.component';
import { LogInComponent } from './components/log-in/log-in.component';

location['apiUrl'] = 'https://localhost:8080';

@NgModule({
  declarations: [
    AppComponent,
    SidenavComponent,
    NavbarComponent,
    HomeComponent,
    TreeViewComponent,
    TreeFormComponent,
    LogInComponent
  ],
  imports: [
    MaterialModule,
    HttpModule,
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    CommonModule,
    ToastrModule.forRoot()
  ],
  providers: [
    CommonsService,
    TreeService,
    JsogService,
    UserService,
    TokenUtilsService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
