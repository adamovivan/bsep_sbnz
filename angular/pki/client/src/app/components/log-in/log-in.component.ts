import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

import { UserService } from '../../services/user.service'
import { User } from '../../model/user.model';

@Component({
  selector: 'siem-signin',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.scss']
})
export class LogInComponent implements OnInit {

  private user: User;

  constructor(private authService: UserService,
    private router: Router,
    private toastr: ToastrService) { this.user = { username:'pera', password:'123'} }

  ngOnInit() { }

  signin() {
    this.authService.authenticate(this.user).subscribe(
      response => {
        this.toastr.success(`Welcome ${this.user.username}`);
        //if (this.authService.checkPermission(CAN_GET_LOG)) {
        //  this.router.navigateByUrl('logs');
        //}
        this.router.navigate(['home']);

      },
      err => { });
  }
}
