import { Component, OnInit } from '@angular/core';
import { TreeService } from '../../../app/services/tree.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CommonsService } from '../../../app/services/commons.service';
import { Node } from '../../../app/model/node.model';

@Component({
  selector: 'tree-form',
  templateUrl: './tree-form.component.html',
  styleUrls: ['./tree-form.component.scss']
})
export class TreeFormComponent implements OnInit {

  certificateForm = new FormGroup({
    'issuerPrincipalName': new FormControl(''), // disabled -> (auto filled always)
    'issuerKeyStorePassword': new FormControl('', [Validators.required]),
    'issuerPrivateKeyPassword': new FormControl('', [Validators.required]),
    'commonName': new FormControl('', [Validators.required]),
    'organization': new FormControl('', [Validators.required]),
    'country': new FormControl('', [Validators.required]),
    'city': new FormControl('', [Validators.required]),
    'startDate': new FormControl('', [Validators.required]),
    'endDate': new FormControl('', [Validators.required]),
    'ipAddress': new FormControl(''),         // 
    'level': new FormControl(''),             //
    'issuerSerialNumber': new FormControl('') // REQUIRED FOR CA ADD!
  });

  nodesConnection: boolean;
  treeState: string;
  minDate: Date = new Date();

  constructor(
    private treeService: TreeService,
    private commonsService: CommonsService) {
    treeService.setCertificateForm(this.certificateForm)
  }

  ngOnInit() {
    this.treeService.selectionModelStateChange.subscribe(
      state => {
        this.treeState = state
        console.log("NEW STATE: ", state)
        // New state interactions ...................
        if (state == 'ADD_CA' || state == 'ADD_CERT') {
          this.certificateForm.reset()
        }
      }
    )

    this.treeService.nodesConnectionChange.subscribe(
      connected => this.nodesConnection = connected
    )
  }

  onCreateCertificate() {
    console.log("Form value:", this.certificateForm.value);
    // UNPACK FORM HERE like: this.form.get('[key]').value
    // let node = new Node();
    // // Fetch from backend
    // node.name = 'SERIAL NUMBER';
    // node.children = [];
    // node.parentId = this.treeService.selectedNode.node.id
    // // node.id
    // /////////////////////
    // node.type = 'CERTIFICATE';
    // node.data = {
    //   'issuerPrincipalName': this.certificateForm.get('issuerPrincipalName').value,
    //   'issuerKeyStorePassword': this.certificateForm.get('issuerKeyStorePassword').value,
    //   'issuerPrivateKeyPassword': this.certificateForm.get('issuerPrivateKeyPassword').value,
    //   'commonName': this.certificateForm.get('commonName').value,
    //   'organization': this.certificateForm.get('organization').value,
    //   'country': this.certificateForm.get('country').value,
    //   'city': this.certificateForm.get('city').value,
    //   'startDate': this.certificateForm.get('startDate').value,
    //   'endDate': this.certificateForm.get('endDate').value,
    // };
    this.certificateForm.patchValue({
      'issuerSerialNumber': this.treeService.selectedNode.node.data.serialNumber
    })
    console.log("Form:", this.certificateForm.value)
    if (this.certificateForm.valid) {
      this.treeService.createCertificate().subscribe()
      this.commonsService.openSnackBar("Generate certificate test", "OK")
      window.location.reload();
    } else {
      this.commonsService.openSnackBar("Form not valid!", "Close")
    }
  }

  onCreateCA() {
    // MERGED WITH CERTIFICATE FORM
    // console.log("Form status:", this.caForm.status);
    // console.log("FORM:", this.caForm);
    // let node = new Node();
    // node.name = this.caForm.get('commonName').value;
    // node.type = 'LEAF_CA';
    // node.children = [];
    // node.parentId = this.treeService.selectedNode.node.id;
    // node.data = {
    //   'commonName': this.caForm.get('commonName').value,
    //   'organization': this.caForm.get('organization').value,
    //   'country': this.caForm.get('country').value,
    //   'city': this.caForm.get('city').value,
    // };
    // this.treeService.addNode(node);

    // Send level of CA
    this.certificateForm.patchValue({
      'level': this.treeService.selectedNode.level + 1,
      'issuerSerialNumber': this.treeService.selectedNode.node.data.serialNumber
    })
    console.log("Form value:", this.certificateForm.value)
    if (this.certificateForm.valid) {
      this.treeService.createCA().subscribe()
      this.commonsService.openSnackBar("Generated CA!", "OK")
      window.location.reload();
    } else {
      this.commonsService.openSnackBar("Form not valid!", "Close")
    }
  }

  onRevoke() {
    this.treeService.revokeCertificate().subscribe(
      data => {
        window.location.reload();
        this.commonsService.openSnackBar("Certificate revoked!","Close")
      }
    )
    console.log("Revoke certificate here!")
  }

}
