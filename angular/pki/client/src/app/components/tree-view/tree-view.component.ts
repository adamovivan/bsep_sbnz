import { Component, OnInit } from '@angular/core';
import { TreeService } from '../../services/tree.service';
import { SelectionModel } from '@angular/cdk/collections';
import { FlatTreeControl } from '@angular/cdk/tree';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Tree } from '../../../app/model/tree.model';
import { MatTreeFlatDataSource, MatTreeFlattener } from '@angular/material/tree';
import { Node } from '../../../app/model/node.model';
import { CommonsService } from '../../../app/services/commons.service';

/** Flat to-do item node with expandable and level information */
export class ExpansionNode {
  name: string;
  node: Node;
  level: number;
  expandable: boolean;
}

/**
 * Checklist database, it can build a tree structured Json object.
 * Each node in Json object represents a to-do item or a category.
 * If a node is a category, it has children items and new items can be added under the category.
 */
@Injectable()
export class ChecklistDatabase {
  dataChange = new BehaviorSubject<Node[]>([]);

  get data(): Node[] { return this.dataChange.value; }

  treeData: Tree;
  constructor(public treeService: TreeService) {
    treeService.getTree().subscribe(
      (data: Tree) => {
        this.treeData = data;
        this.initialize();
      })
  }

  initialize() {
    // Build the tree nodes from Json object.
    const data = this.buildFileTree(this.treeData);
    // Notify the change.
    this.dataChange.next(data);
  }

  /**
   * Build the tree structure.
   */
  buildFileTree(tree: Tree): Node[] {
    let retval: Node[] = [];
    retval.push(tree.root)
    return retval
  }

  /** Add an item to list */
  insertItem(parent: Node, node: Node) {
    console.log("insertItem to parent:", parent, name)
    // UNPACK NODE DATA FROM FORM
    if (parent.children) {
      parent.children.push(node);
      this.dataChange.next(this.data);
    }
  }

  updateItem(node: Node, name: string) {
    console.log("updateItem:", node, name)
    node.name = name;
    this.dataChange.next(this.data);
  }
}

/**
 * Tree with checkboxes
 */
@Component({
  selector: 'tree-view',
  templateUrl: 'tree-view.component.html',
  styleUrls: ['tree-view.component.scss'],
  providers: [ChecklistDatabase]
})
export class TreeViewComponent implements OnInit {

  tree: Tree;

  /** Map from flat node to nested node. This helps us finding the nested node to be modified */
  flatNodeMap = new Map<ExpansionNode, Node>();

  /** Map from nested node to flattened node. This helps us to keep the same object for selection */
  nestedNodeMap = new Map<Node, ExpansionNode>();

  /** A selected parent node to be inserted */
  selectedParent: ExpansionNode | null = null;

  /** The new item's name */
  newItemName = '';

  treeControl: FlatTreeControl<ExpansionNode>;

  treeFlattener: MatTreeFlattener<Node, ExpansionNode>;

  dataSource: MatTreeFlatDataSource<Node, ExpansionNode>;

  /** The selection for checklist */
  checklistSelection = new SelectionModel<ExpansionNode>(true /* multiple */);

  constructor(
    private database: ChecklistDatabase,
    private treeService: TreeService,
    private commons: CommonsService
  ) {
    this.treeFlattener = new MatTreeFlattener(this.transformer, this.getLevel,
      this.isExpandable, this.getChildren);
    this.treeControl = new FlatTreeControl<ExpansionNode>(this.getLevel, this.isExpandable);
    this.dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);

    database.dataChange.subscribe(data => {
      this.dataSource.data = data;
    });
  }

  ngOnInit() {
    this.initTree();
    this.treeService.nodeAdd.subscribe(
      (node: Node) => {
        this.addNewNode(node)
      }
    )
    // Bind selection model
    this.treeService.treeSelectionModel = this.checklistSelection
  }

  initTree() {
    // Get tree structure
    this.treeService.getTree().subscribe(
      (data: Tree) => {
        this.tree = data
        console.log("Tree in component:", this.tree)
      }
    );
    // Get node connections
    this.treeService.getTreeConnections().subscribe(
      data => {
        console.log(this.treeService.treeConnections)
      }
    )
  }

  getLevel = (node: ExpansionNode) => node.level;

  isExpandable = (node: ExpansionNode) => node.expandable;

  getChildren = (node: Node): Node[] => node.children;

  hasChild = (_: number, _nodeData: ExpansionNode) => _nodeData.expandable;

  hasNoContent = (_: number, _nodeData: ExpansionNode) => _nodeData.name === '';

  /**
   * Transformer to convert nested node to flat node. Record the nodes in maps for later use.
   */
  transformer = (node: Node, level: number) => {
    const existingNode = this.nestedNodeMap.get(node);
    const flatNode = existingNode && existingNode.name === node.name
      ? existingNode
      : new ExpansionNode();
    flatNode.name = node.name;
    flatNode.node = node;
    flatNode.level = level;
    flatNode.expandable = (node.type != 'CERTIFICATE');
    this.flatNodeMap.set(flatNode, node);
    this.nestedNodeMap.set(node, flatNode);
    return flatNode;
  }

  /** Whether all the descendants of the node are selected. */
  descendantsAllSelected(node: ExpansionNode): boolean {
    const descendants = this.treeControl.getDescendants(node);
    const descAllSelected = descendants.every(child =>
      this.checklistSelection.isSelected(child)
    );
    return descAllSelected;
  }

  /** Toggle the item selection.*/
  todoItemSelectionToggle(node: ExpansionNode): void {
    this.checklistSelection.toggle(node);
    let status = this.checklistSelection.isSelected(node)
    console.log("Selection checked:", status)
    console.log("Selection toggle:", node)
    this.treeService.updateTreeState()

    console.log("Updating form!")
    this.treeService.updateCertificateForm(node.node)
  }

  /** Check root node checked state and change it accordingly */
  checkRootNodeSelection(node: ExpansionNode): void {
    // Not needed for now !?
  }

  /* Get the parent node of a node */
  getParentNode(node: ExpansionNode): ExpansionNode | null {
    const currentLevel = this.getLevel(node);
    if (currentLevel < 1) {
      return null;
    }

    const startIndex = this.treeControl.dataNodes.indexOf(node) - 1;
    for (let i = startIndex; i >= 0; i--) {
      const currentNode = this.treeControl.dataNodes[i];

      if (this.getLevel(currentNode) < currentLevel) {
        return currentNode;
      }
    }
    return null;
  }

  addNewNode(node: Node) {
    this.addNewItem(node);
    this.saveNode();
  }

  /** Drop menu on (+) button click.          MISLIM DA NE TREBA    */
  addNewItem(node: Node) {
    console.log("addNewItem:", node);
    // const parentNode = this.flatNodeMap.get(node);
    const parentNode = this.treeService.selectedNode.node
    this.database.insertItem(parentNode!, node);
    // this.treeControl.expand(node);
  }

  /** On [Save] button click */
  saveNode() {
    let node = this.treeService.selectedNode // always a parent node
    console.log("saveNode:", node, 'TEST NAME');
    const nestedNode = this.flatNodeMap.get(node);
    this.database.updateItem(nestedNode!, node.name);
  }

  menuSelectedNode(exNode: ExpansionNode) {
    console.log("Selected node:", exNode.node);
    this.treeService.setSelectedNode(exNode);
  }
  // Opens add certificate form
  onAddCertificate() {
    this.treeService.setTreeState('ADD_CERT');
    this.treeService.updateIssuerInfo()
  }
  // Opens add ca form
  onAddCA() {
    this.treeService.setTreeState('ADD_CA');
    this.treeService.updateIssuerInfo()
  }
  // Remove node
  onRemove() {
    console.log('Node to remove:', this.treeService.selectedNode)
    // if (this.treeService.selectedNode.node.children != null ||
    //   this.treeService.selectedNode.node.children.length != 0) {
    //   this.commons.openSnackBar('Please remove all leaf nodes first!', 'OK');
    // }
    // else {
      // Call to back end for easier remove!!
      this.treeService.removeNode(this.treeService.selectedNode.node.data.serialNumber).subscribe(
        data => {
          this.initTree()
          window.location.reload();
          this.commons.openSnackBar('Remove successfull','OK')
        }
      )
    // }
  }

}
