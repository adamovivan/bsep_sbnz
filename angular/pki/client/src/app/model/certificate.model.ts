export class Certificate {
    public issuerPrincipalName: string
    public issuerKeyStorePassword: string
    public issuerPrivateKeyPassword: string
    public commonName: string
    public organization: string
    public country: string
    public city: string
    public startDate: Date
    public endDate: Date
}