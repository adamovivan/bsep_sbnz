export class Node {
    public id: number;
    public name: string;
    public type: string;
    public data: any;
    public parentId: any;
    public children: Node[];
    public revoked: boolean;
}