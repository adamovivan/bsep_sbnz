import { Component, OnInit } from '@angular/core';
import { CommonsService } from '../services/commons.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  constructor(private commonsService: CommonsService) { }

  ngOnInit() {
  }

  toggleSideNav() {
    this.commonsService.toggleSideNav();
  }

}
