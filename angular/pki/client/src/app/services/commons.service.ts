import { Injectable } from '@angular/core';
import { MatSnackBar, MatSidenav } from '@angular/material';

@Injectable()
export class CommonsService {

  constructor(private snackBar: MatSnackBar) { }

  private sidenav: MatSidenav;

  // SnackBar
  public openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      panelClass: ['snack-dark-mode'],
      duration: 5000,
    });
  }

  public setSidenav(sidenav: MatSidenav) {
    this.sidenav = sidenav;
  }

  public toggleSideNav() {
    return this.sidenav.toggle();
  }

}
