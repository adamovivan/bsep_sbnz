import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { JsogService } from 'jsog-typescript';
import { map } from 'rxjs/operators';
import { SelectionModel } from '@angular/cdk/collections';
import { Tree } from '../model/tree.model';
import { ExpansionNode } from '../components/tree-view/tree-view.component';
import { Subject } from 'rxjs';
import { Node } from '../model/node.model';
import { FormGroup } from '@angular/forms';

@Injectable()
export class TreeService {

  tree: Tree;
  selectedNode: ExpansionNode;
  treeConnections: any[];
  treeSelectionModel: SelectionModel<ExpansionNode>;

  selectionModelState: string = 'NONE';
  selectionModelStateChange: Subject<string> = new Subject<string>();

  nodesConnected: boolean = false
  nodesConnectionChange: Subject<boolean> = new Subject<boolean>();

  nodeAdd: Subject<Node> = new Subject<Node>();

  connectableTypes = ['ROOT_CA', 'IM_CA', 'LEAF_CA']
  nonMenuTypes = ['ROOT_CA', 'CERTIFICATE']

  certificateForm: FormGroup;
  caForm: FormGroup;

  currentNodeSelection: Node;

  constructor(
    private http: Http,
    private jsog: JsogService
  ) { }

  getTree() {
    return this.http.get(location['apiUrl'] + '/tree').pipe(map(
      (response: Response) => {
        const data = this.jsog.deserialize(response.json());
        return data;
      }
    ))
  }

  getTreeConnections() {
    return this.http.get(location['apiUrl'] + '/tree/connections').pipe(map(
      (response: Response) => {
        const data = this.jsog.deserialize(response.json());
        this.treeConnections = <any[]>data;
        return data;
      }
    ))
  }

  // Remove CA !!!!!!!
  removeNode(serialNumber: string) {
    return this.http.post(location['apiUrl'] + '/certificate/removeCaBySerialNumber', serialNumber).pipe(map(
      (response: Response) => {
        const data = this.jsog.deserialize(response.json());
        return data;
      }
    ))
  }

  // Add new CA
  createCA() {
    return this.http.post(location['apiUrl'] + '/certificate/createCa', this.certificateForm.value).pipe(map(
      (response: Response) => {
        const data = this.jsog.deserialize(response.json());
        return data;
      }
    ))
  }

  // Issue new certificate
  createCertificate() {
    return this.http.post(location['apiUrl'] + '/certificate/createCert', this.certificateForm.value).pipe(map(
      (response: Response) => {
        const data = this.jsog.deserialize(response.json());
        return data;
      }
    ))
  }

  // Revoke certificate
  revokeCertificate() {
    return this.http.post(location['apiUrl'] + '/certificate/revokeCertificate/' + this.currentNodeSelection.data.serialNumber
      , {}).pipe(map(
        (response: Response) => {
          const data = this.jsog.deserialize(response.json());
          return data;
        }
      ))
  }

  selectedNodeRevoked(): boolean {
    if (this.currentNodeSelection == undefined)
      return false
    return this.currentNodeSelection.revoked
  }

  // Certificate Form Controls
  setCertificateForm(form: FormGroup) {
    this.certificateForm = form;
  }

  updateCertificateForm(node: Node) {
    console.log("Selected node:", node)
    this.currentNodeSelection = node;

    this.certificateForm.patchValue({
      'issuerPrincipalName': node.data.issuedBy,
      'commonName': node.data.principalName,
      'organization': node.data.principalName,
      'country': node.data.principalName,
      'city': node.data.principalName,
      'startDate': new Date(node.data.validFrom),
      'endDate': new Date(node.data.validTo)
    })
  }

  setSelectedNode(node: ExpansionNode) {
    this.selectedNode = node
  }

  updateIssuerInfo() {
    this.certificateForm.patchValue({
      'issuerPrincipalName': this.selectedNode.node.name
    })
  }

  setSelectionModelState(newState: string) {
    this.selectionModelState = newState
    this.selectionModelStateChange.next(newState);
  }

  setNodesState(val: boolean) {
    this.nodesConnected = val
    this.nodesConnectionChange.next(val)
  }

  addNode(node: Node) {
    this.nodeAdd.next(node)
  }

  updateTreeState() {
    // Generate certificate
    if (this.caSelected()) {
      this.setSelectionModelState('CA')
    }
    // View certificate
    else if (this.certificateSelected()) {
      this.setSelectionModelState('CRT')
    }
    // Connect / Disconnect nodes
    else if (this.twoCAsSelected()) {
      this.setSelectionModelState('2CA')
      this.setNodesState(this.twoCAsConnected())
    }
    // ?
    else {
      this.setSelectionModelState('NONE')
    }
  }

  // Temporary state set (will be overriden by any of above selections)
  setTreeState(newState: string) {
    this.setSelectionModelState(newState)
  }

  isConnectableType(type: string): boolean {
    return this.connectableTypes.indexOf(type) >= 0;
  }

  isCertificableType(type: string): boolean {
    return this.connectableTypes.indexOf(type) >= 1;
  }

  isNoMenuType(type: string) {
    return this.nonMenuTypes.indexOf(type) >= 0;
  }

  certificateSelected(): boolean {
    if (this.treeSelectionModel.selected.length == 1 &&
      this.treeSelectionModel.selected[0].node.type == 'CERTIFICATE') {
      return true;
    }
    return false;
  }

  caSelected(): boolean {
    if (this.treeSelectionModel.selected.length == 1 &&
      this.isCertificableType(this.treeSelectionModel.selected[0].node.type)) {
      return true;
    }
    return false;
  }

  twoCAsSelected(): boolean {
    if (this.treeSelectionModel.selected.length == 2) {
      if (this.isConnectableType(this.treeSelectionModel.selected[0].node.type) &&
        this.isConnectableType(this.treeSelectionModel.selected[1].node.type)) {
        return true;
      }
    }
    return false;
  }

  twoCAsConnected(): boolean {
    let retval: boolean = false;
    let id1 = this.treeSelectionModel.selected[0].node.id;
    let id2 = this.treeSelectionModel.selected[1].node.id;

    this.treeConnections.forEach(element => {
      if (element.key == id1 && element.value == id2 ||
        element.key == id2 && element.value == id1) {
        retval = true;
      }
    })
    return retval;
  }

}
