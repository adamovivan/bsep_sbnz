import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { User } from '../model/user.model';
import { Authentication } from '../model/auth.model';
import { TokenUtilsService } from './token-utils.service';


const authenticatedUserKey = 'authenticatedUser';

@Injectable({
  providedIn: 'root'
})
export class UserService{


  constructor(private http: HttpClient, private toastr: ToastrService, private tokenUtils: TokenUtilsService) {
    //super(http, ['/api/user'], toastr);
  }

  authenticate(body: User): Observable<Authentication> {
    //,{ withCredentials: true }
    return this.http.post<Authentication>(location['apiUrl'] + '/api/auth', body).pipe(
      tap(res => {
        localStorage.setItem(authenticatedUserKey, JSON.stringify({
          user: res.id,
          permissions: this.tokenUtils.getRoles(res.token),
          token: res.token
        }));
        console.log(localStorage);
      })
      ,catchError(this.handleError<Authentication>())
    );
  }
  /*getUser(): Observable<User> {
    if (this.userName === '') {
      return this.getCurrentUser();
    } else {
      return this.getByUsername(this.userName);
    }
  }

  getByUsername(username: String) {
    return this.http.get<User>(this.url(['getByUsername/' + username]));
  }*/

  getAuthenticatedUser() {
    return JSON.parse(localStorage.getItem(authenticatedUserKey));
  }

  getAuthenticatedUserId() {
    return this.getAuthenticatedUser().id;
  }

  getToken(): String {
    const authenticatedUser = this.getAuthenticatedUser();
    const token = authenticatedUser && authenticatedUser.token;
    return token ? token : '';
  }

  getPermissions(): Array<String> {
    const authenticatedUser = this.getAuthenticatedUser();
    const permissions = authenticatedUser && authenticatedUser.permissions;
    return permissions ? permissions : new Array<String>();
  }

  checkPermission(permission: string): boolean {
    return this.getPermissions().includes(permission);
  }

  isAuthenticated(): boolean {
    return this.getToken() !== '';
  }

  signout(): void {
    localStorage.removeItem(authenticatedUserKey);
  }

  getCurrentUser() {
    return this.http.get<User>(location['apiUrl'] + '/auth/currentUser');
  }

  protected handleError<E>(operation = 'operation', result?: E) {
    return (response: any): Observable<E> => {
      //console.log(response);
      // Get error object from response and its error message      

      if (response.error) {
        if(response.error.message){
          this.toastr.error(response.error.message);
        }else if((typeof response.error === 'string' || response.error instanceof String)
         && !response.error.startsWith("Error occured")) {
            this.toastr.error(response.error);
        }else {
          this.toastr.error('Server is down!');
        }
      } else {
        this.toastr.info('Schedule does not exist!');
      }
      return throwError(result as E || response.statusText);
    };
  }

}
