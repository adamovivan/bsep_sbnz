import { Component, OnInit, ViewChild, AfterViewInit, ViewEncapsulation} from '@angular/core';
import { MatSidenav } from '@angular/material';
import { CommonsService } from '../services/commons.service';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SidenavComponent implements OnInit, AfterViewInit {

  @ViewChild('sidenav') public sidenav: MatSidenav;

  constructor(private commonsService: CommonsService) { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.commonsService.setSidenav(this.sidenav);
  }

  panelClick($event: Event) {
    $event.stopPropagation();
  }

}
