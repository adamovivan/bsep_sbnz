package com.tim9.hong_kong_instance;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.FileNotFoundException;

@SpringBootApplication
public class HongKongInstanceApplication {

    public static void main(String[] args) {
        SpringApplication.run(HongKongInstanceApplication.class, args);
    }

}
