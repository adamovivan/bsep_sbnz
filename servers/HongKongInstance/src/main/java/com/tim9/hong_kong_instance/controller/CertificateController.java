package com.tim9.hong_kong_instance.controller;


import com.tim9.hong_kong_instance.dto.CertificateDTOs;
import com.tim9.hong_kong_instance.dto.request.CreateCertificateRequest;
import com.tim9.hong_kong_instance.dto.response.SimpleResponse;
import com.tim9.hong_kong_instance.service.CertificateService;
import com.tim9.hong_kong_instance.service.SFTPService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.security.cert.CertificateEncodingException;


@RestController
@RequestMapping("certificate")
public class CertificateController {

    @Autowired
    private CertificateService certificateService;

    @Value("${server.ssl.key-store}")
    private String keystorePath;

    @Value("${issued-certs-path}")
    private String issuedCertsPath;

    @Autowired
    private SFTPService sftpService;


    //    @PreAuthorize("hasAuthority('CREATE_CERTIFICATE')")
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public ResponseEntity<SimpleResponse> createCertificate(@RequestBody CreateCertificateRequest ccRequest) throws Exception {
        try {
            if (certificateService.createCertificate(ccRequest)) {
                return ResponseEntity.ok().body(new SimpleResponse("CertificateData successfully added."));
            }
        } catch (CertificateEncodingException | IOException e) {
            return ResponseEntity.badRequest().body(new SimpleResponse(false, "Something went wrong."));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new SimpleResponse(false, e.getMessage()));
        }
        return ResponseEntity.badRequest().body(new SimpleResponse(false, "Something went wrong."));
    }

    //    @PreAuthorize("hasAuthority('READ_CERTIFICATE')")
    @RequestMapping(value = "/getIssuedCertificates", method = RequestMethod.GET)
    public ResponseEntity<CertificateDTOs> getIssuedCertificates() {
        CertificateDTOs certificateDTOs = certificateService.getCertificates(issuedCertsPath);
        return ResponseEntity.ok().body(certificateDTOs);
    }

    @RequestMapping(value = "/getCACertificate", method = RequestMethod.GET)
    public ResponseEntity<CertificateDTOs> getCACertificate() {
        CertificateDTOs certificateDTOs = certificateService.getCertificates(keystorePath);
        return ResponseEntity.ok().body(certificateDTOs);
    }

    @RequestMapping(value = "/getTruststore", method = RequestMethod.GET)
    public ResponseEntity<CertificateDTOs> getTruststore() {
        CertificateDTOs certificateDTOs = certificateService.getTruststore();
        return ResponseEntity.ok().body(certificateDTOs);
    }

    @RequestMapping(value = "/receiveCertificate/{nodeName}", method = RequestMethod.GET)
    public ResponseEntity<Boolean> receiveCertificate(@PathVariable String nodeName){
        Boolean valid = sftpService.receiveCertificate(nodeName);
        return ResponseEntity.ok().body(valid);
    }

    @RequestMapping(value = "/sendCertificate/{nodeName}", method = RequestMethod.GET)
    public ResponseEntity<Boolean> sendCertificate(@PathVariable String nodeName){
        Boolean valid = sftpService.sendCertificate(nodeName);
        return ResponseEntity.ok().body(valid);
    }

    @RequestMapping(value = "/exportCertificate", method = RequestMethod.GET)
    public ResponseEntity<SimpleResponse> exportCertificate() {
//        certificateService.importCertificateToTruststore(null,null,null,null);
        certificateService.exportCertificate(keystorePath, "password", "cn=localhost,ou=hk,o=megatravel,l=hk,st=hk,c=cn", "./files/pki/certificates/exported-cert.cer");
        return ResponseEntity.ok().body(new SimpleResponse("Exported!"));
    }

}
