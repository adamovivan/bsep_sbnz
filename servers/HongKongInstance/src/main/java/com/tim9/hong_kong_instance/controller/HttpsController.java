package com.tim9.hong_kong_instance.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.awt.*;
import java.net.URI;
import java.net.URISyntaxException;


@RestController
@RequestMapping("test")
public class HttpsController {

    @PostMapping(value = "/message", produces = MediaType.APPLICATION_JSON_VALUE)
    public Boolean testPost(@RequestBody String msg) {
        System.out.println("Message got: " + msg);
        return Boolean.TRUE;
    }

    @GetMapping(value = "/message", produces = MediaType.APPLICATION_JSON_VALUE)
    public String testGet() {
        return "POYY";
    }

    @Autowired
    private RestTemplate restTemplate;

    @GetMapping(value = "/testHttps")
    public void testServer() throws URISyntaxException {
        System.out.println("MESSAGE:");
        String response = restTemplate.getForObject("https://localhost:8081/test/message", String.class);
        System.out.println(response);

//        RestTemplate restTemplate = new RestTemplate();
//        HttpEntity<String> request = new HttpEntity<>("WORKS!");
//        restTemplate.postForObject("https://localhost:8082/test/message", request, String.class);
    }

}
