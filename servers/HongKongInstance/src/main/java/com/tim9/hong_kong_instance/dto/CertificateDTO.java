package com.tim9.hong_kong_instance.dto;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;

public class CertificateDTO implements Serializable {
    private CertificateDTO parentCertificate;
    private int version;
    private String issuedBy;
    private String issuedTo;
    private byte[] publicKey;
    private String principalName;
    private String serialNumber;
    private Date validFrom;
    private Date validTo;
    private byte[] signature;

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public CertificateDTO getParentCertificate() {
        return parentCertificate;
    }

    public void setParentCertificate(CertificateDTO parentCertificate) {
        this.parentCertificate = parentCertificate;
    }

    public String getIssuedBy() {
        return issuedBy;
    }

    public void setIssuedBy(String issuedBy) {
        this.issuedBy = issuedBy;
    }

    public String getIssuedTo() {
        return issuedTo;
    }

    public void setIssuedTo(String issuedTo) {
        this.issuedTo = issuedTo;
    }

    public String getPrincipalName() {
        return principalName;
    }

    public void setPrincipalName(String principalName) {
        this.principalName = principalName;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public Date getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }

    public Date getValidTo() {
        return validTo;
    }

    public void setValidTo(Date validTo) {
        this.validTo = validTo;
    }

    public byte[] getSignature() {
        return signature;
    }

    public void setSignature(byte[] signature) {
        this.signature = signature;
    }

    public byte[] getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(byte[] publicKey) {
        this.publicKey = publicKey;
    }

    @Override
    public String toString() {
        return "CertificateDTO{" +
                "parentCertificate=" + parentCertificate +
                ", version=" + version +
                ", issuedBy='" + issuedBy + '\'' +
                ", issuedTo='" + issuedTo + '\'' +
                ", publicKey=" + Arrays.toString(publicKey) +
                ", principalName='" + principalName + '\'' +
                ", serialNumber='" + serialNumber + '\'' +
                ", validFrom=" + validFrom +
                ", validTo=" + validTo +
                ", signature=" + Arrays.toString(signature) +
                '}';
    }
}
