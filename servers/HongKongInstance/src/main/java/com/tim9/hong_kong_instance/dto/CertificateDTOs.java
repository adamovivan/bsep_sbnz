package com.tim9.hong_kong_instance.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CertificateDTOs implements Serializable {
    private ArrayList<CertificateDTO> cerificateList;

    public CertificateDTOs(List<CertificateDTO> cerificateList){
        this.setCerificateList(cerificateList);
    }

    public ArrayList<CertificateDTO> getCerificateList() {
        return cerificateList;
    }

    public void setCerificateList(List<CertificateDTO> cerificateList) {
        this.cerificateList = (ArrayList<CertificateDTO>) cerificateList;
    }
}
