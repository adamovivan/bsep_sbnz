package com.tim9.hong_kong_instance.model;

import com.tim9.hong_kong_instance.model.enums.NodeType;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class CertificateAuthority implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = true)
    private Long parentId;      // ID from parent CA

    @Column(nullable = false)
    private String issuerPrincipalName;

    @Column(nullable = false)
    private String commonName;

    @Column(nullable = false)
    private String organization;

    @Column(nullable = false)
    private String country;

    @Column(nullable = false)
    private String city;

    // can't be CERTIFICATE !!!
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private NodeType nodeType;

    public CertificateAuthority() {
    }

    public CertificateAuthority(Long parentId, String issuerPrincipalName, String commonName, String organization,
                                String country, String city, NodeType nodeType) {
        this.parentId = parentId;
        this.issuerPrincipalName = issuerPrincipalName;
        this.commonName = commonName;
        this.organization = organization;
        this.country = country;
        this.city = city;
        this.nodeType = nodeType;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIssuerPrincipalName() {
        return issuerPrincipalName;
    }

    public void setIssuerPrincipalName(String issuerPrincipalName) {
        this.issuerPrincipalName = issuerPrincipalName;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getCommonName() {
        return commonName;
    }

    public void setCommonName(String commonName) {
        this.commonName = commonName;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public NodeType getNodeType() {
        return nodeType;
    }

    public void setNodeType(NodeType nodeType) {
        this.nodeType = nodeType;
    }

    @Override
    public String toString() {
        return "CertificateAuthority{" +
                "parentId='" + parentId + '\'' +
                ", commonName='" + commonName + '\'' +
                ", organization='" + organization + '\'' +
                ", country='" + country + '\'' +
                ", city='" + city + '\'' +
                '}';
    }
}
