package com.tim9.hong_kong_instance.model.enums;

public enum CertificateStatus {
    GOOD,
    REVOKED,
    UNKNOWN
}
