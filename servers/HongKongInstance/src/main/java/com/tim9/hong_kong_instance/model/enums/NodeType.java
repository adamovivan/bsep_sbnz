package com.tim9.hong_kong_instance.model.enums;

public enum NodeType {
    ROOT_CA,
    IM_CA,
    LEAF_CA,
    CERTIFICATE
}
