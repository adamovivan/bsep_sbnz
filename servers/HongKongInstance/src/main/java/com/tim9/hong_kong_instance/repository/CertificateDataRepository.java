package com.tim9.hong_kong_instance.repository;

import com.tim9.hong_kong_instance.model.CertificateData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CertificateDataRepository extends JpaRepository<CertificateData, Long> {

    CertificateData findBySerialNumber(String serialNumber);
    CertificateData findByPrincipalName(String principalName);

}
