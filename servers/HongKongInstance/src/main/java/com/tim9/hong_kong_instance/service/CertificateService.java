package com.tim9.hong_kong_instance.service;

import com.tim9.hong_kong_instance.dto.CertificateDTO;
import com.tim9.hong_kong_instance.dto.CertificateDTOs;
import com.tim9.hong_kong_instance.dto.request.CreateCertificateRequest;
import com.tim9.hong_kong_instance.repository.CertificateDataRepository;
import com.tim9.hong_kong_instance.security.pki.certificates.CertificateGenerator;
import com.tim9.hong_kong_instance.security.pki.data.IssuerData;
import com.tim9.hong_kong_instance.security.pki.data.SubjectData;
import com.tim9.hong_kong_instance.security.pki.keystores.KeyStoreReader;
import com.tim9.hong_kong_instance.security.pki.keystores.KeyStoreWriter;
import com.tim9.hong_kong_instance.util.Base64Utility;
import com.tim9.hong_kong_instance.util.HashUtility;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.*;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CertificateService {

    @Autowired
    private CertificateDataRepository certificateDataRepository;

    @Autowired
    private CertificateGenerator certGenerator;

    @Autowired
    private KeyStoreWriter ksWriter;

    @Autowired
    private KeyStoreReader ksReader;

    @Value("${server.ssl.key-store-password}")
    private String keystorePass;

    @Value("${server.ssl.key-store}")
    private String keystorePath;

    @Value("${server.ssl.trust-store}")
    private String truststorePath;

    @Value("${issued-certs-path}")
    private String issuedCertsPath;

    @Value("${server.ssl.trust-store-password}")
    private String truststorePass;

    public CertificateDTOs getCertificates(String filePath) {
        List<Certificate[]> certificates = ksReader.readAllCertificates(filePath, keystorePass);
        List<CertificateDTO> certificateDTOs = certificates.stream().map(this::certificatesToCertificateDTO).collect(Collectors.toList());
        return new CertificateDTOs(certificateDTOs);

    }

    public boolean createCertificate(CreateCertificateRequest ccRequest) throws Exception {
        String privateKeyPasswordSalt = certificateDataRepository.findByPrincipalName(ccRequest.getIssuerPrincipalName()).getPrivateKeyPasswordSalt();

        String privateKeyPassword = Base64Utility.encode(HashUtility.hashPassword(ccRequest.getIssuerPrivateKeyPassword(),
                Base64Utility.decode(privateKeyPasswordSalt)));

        Certificate[] certificates = ksReader.readCertificates(keystorePath, keystorePass, ccRequest.getIssuerPrincipalName());

        if (certificates == null || certificates.length == 0) {
            return false;
        }

        PrivateKey privateKey = ksReader.readPrivateKey(keystorePath, keystorePass, ccRequest.getIssuerPrincipalName(), privateKeyPassword);
        KeyPair keyPair = certGenerator.generateKeyPair();

        IssuerData issuerData = certGenerator.generateIssuerDate(privateKey,
                (X509Certificate) certificates[0]);
        SubjectData subjectData = certGenerator.generateSubjectData(keyPair.getPublic(), ccRequest);

        X509Certificate x509Certificate = certGenerator.generateCertificate(subjectData, issuerData);
        Certificate[] certsExtended = ArrayUtils.insert(0, certificates, x509Certificate);

        ksWriter.loadKeyStore(issuedCertsPath, keystorePass.toCharArray());
        ksWriter.write(x509Certificate.getSubjectX500Principal().getName(), keyPair.getPrivate(), privateKeyPassword.toCharArray(), certsExtended);
        ksWriter.saveKeyStore(issuedCertsPath, keystorePass.toCharArray());

        return true;
    }

    public CertificateDTO certificatesToCertificateDTO(Certificate[] certificates) {

        CertificateDTO parent = null;
        CertificateDTO certDTO = null;

        for (int i = certificates.length - 1; i >= 0; i--) {
            X509Certificate x509Cert = (X509Certificate) certificates[i];

            certDTO = mapX509CertificateToCertificateDTO(x509Cert);
            certDTO.setParentCertificate(parent);

            parent = certDTO;
        }

        return certDTO;
    }

    public CertificateDTO certificatesToCertificateDTO(Certificate certificate) {
        X509Certificate x509Cert = (X509Certificate) certificate;
        return mapX509CertificateToCertificateDTO(x509Cert);
    }

    private CertificateDTO mapX509CertificateToCertificateDTO(X509Certificate x509Cert) {

        CertificateDTO certDTO = new CertificateDTO();
        certDTO.setVersion(x509Cert.getVersion());
        certDTO.setIssuedBy(x509Cert.getIssuerX500Principal().getName());
        certDTO.setIssuedTo(x509Cert.getSubjectX500Principal().getName());
        certDTO.setPublicKey(x509Cert.getPublicKey().getEncoded());
        certDTO.setPrincipalName(x509Cert.getSubjectX500Principal().getName());
        certDTO.setSerialNumber(x509Cert.getSerialNumber().toString());
        certDTO.setValidFrom(x509Cert.getNotBefore());
        certDTO.setValidTo(x509Cert.getNotAfter());
        certDTO.setSignature(x509Cert.getSignature());

        return certDTO;
    }

    public CertificateDTOs getTruststore() {
        List<Certificate> certificates = ksReader.readCertificates(truststorePath, truststorePass);
        List<CertificateDTO> certificateDTOs = certificates.stream().map(this::certificatesToCertificateDTO).collect(Collectors.toList());
        return new CertificateDTOs(certificateDTOs);
    }

    public boolean exportCertificate(String keystorePath, String keystorePass, String alias, String outputCertPath) {
        try {
            FileInputStream is = new FileInputStream(keystorePath);

            KeyStore keystore = KeyStore.getInstance(KeyStore.getDefaultType());
            keystore.load(is, keystorePass.toCharArray());

            X509Certificate cert = (X509Certificate) keystore.getCertificate(alias);

            File file = new File(outputCertPath);
            byte[] buf = cert.getEncoded();

            FileOutputStream os = new FileOutputStream(file);
            os.write(buf);
            os.close();
        } catch (IOException | NoSuchAlgorithmException | CertificateException | KeyStoreException e) {
            e.printStackTrace();
        }

        return true;
    }

    public boolean importCertificateToTruststore(String truststorePath, String truststorePass, String alias, String importCertPath){

        // TODO delete
        truststorePath = "./files/pki/keystores/truststore.jks";
        truststorePass = "password";
        alias = "london";
        importCertPath = "./files/pki/certificates/london.cer";

        try {
            KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
            BufferedInputStream in = new BufferedInputStream(new FileInputStream(truststorePath));
            trustStore.load(in, truststorePass.toCharArray());

            InputStream fis = new FileInputStream(importCertPath);
            BufferedInputStream bis = new BufferedInputStream(fis);

            CertificateFactory cf = CertificateFactory.getInstance("X.509");

            while (bis.available() > 0) {
                Certificate cert = cf.generateCertificate(bis);
                trustStore.setCertificateEntry(alias, cert);
            }

            trustStore.store(new FileOutputStream(truststorePath), truststorePass.toCharArray());

        } catch (IOException | NoSuchAlgorithmException | CertificateException | KeyStoreException e) {
            e.printStackTrace();
        }

        return true;
    }

}
