package com.tim09.london_instance;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LondonInstanceApplication {

    public static void main(String[] args) {
        SpringApplication.run(LondonInstanceApplication.class, args);
    }

}
