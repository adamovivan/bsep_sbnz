package com.tim09.london_instance.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;


@RestController
@RequestMapping("test")
public class HttpsController {

    @Autowired
    private RestTemplate restTemplate;

    @PostMapping(value = "/message")
    public String testPost(@RequestBody String msg) {
        System.out.println("Message got: " + msg);
        return "POYY";
    }

    @GetMapping(value = "/message")
    public String testGet() {
        return "POYY";
    }

    @GetMapping(value = "/testHttps")
    public void testServer() {
        System.out.println("TEST");
        // GET
        System.out.println("MESSAGE:");
        String response = restTemplate.getForObject("https://localhost:8082/test/message", String.class);
        System.out.println(response);

        // POST
//        HttpEntity<String> request = new HttpEntity<>("WORKS!");
//        restTemplate.postForObject("https://localhost:8082/test/message", request, Boolean.class);
    }

}