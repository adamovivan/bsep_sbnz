package com.tim09.london_instance.model.enums;

public enum CertificateStatus {
    GOOD,
    REVOKED,
    UNKNOWN
}
