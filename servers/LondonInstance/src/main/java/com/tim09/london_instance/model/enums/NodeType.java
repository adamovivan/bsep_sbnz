package com.tim09.london_instance.model.enums;

public enum NodeType {
    ROOT_CA,
    IM_CA,
    LEAF_CA,
    CERTIFICATE
}
