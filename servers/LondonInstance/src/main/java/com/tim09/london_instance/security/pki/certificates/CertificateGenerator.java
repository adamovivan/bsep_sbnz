package com.tim09.london_instance.security.pki.certificates;

import com.tim09.london_instance.dto.request.CreateCertificateRequest;
import com.tim09.london_instance.security.pki.data.IssuerData;
import com.tim09.london_instance.security.pki.data.SubjectData;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x500.X500NameBuilder;
import org.bouncycastle.asn1.x500.style.BCStyle;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.X509v3CertificateBuilder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.cert.jcajce.JcaX509CertificateHolder;
import org.bouncycastle.cert.jcajce.JcaX509v3CertificateBuilder;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.springframework.stereotype.Component;

import java.math.BigInteger;
import java.security.*;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Random;

@Component
public class CertificateGenerator {

	public CertificateGenerator() {}

	public X509Certificate generateCertificate(SubjectData subjectData, IssuerData issuerData) {
		try {
			//Posto klasa za generisanje sertifiakta ne moze da primi direktno privatni kljuc pravi se builder za objekat
			//Ovaj objekat sadrzi privatni kljuc izdavaoca sertifikata i koristiti se za potpisivanje sertifikata
			//Parametar koji se prosledjuje je algoritam koji se koristi za potpisivanje sertifiakta
			JcaContentSignerBuilder builder = new JcaContentSignerBuilder("SHA256WithRSAEncryption");
			//Takodje se navodi koji provider se koristi, u ovom slucaju Bouncy Castle
			builder = builder.setProvider(new BouncyCastleProvider());

			//Formira se objekat koji ce sadrzati privatni kljuc i koji ce se koristiti za potpisivanje sertifikata
			ContentSigner contentSigner = builder.build(issuerData.getPrivateKey());

			//Postavljaju se podaci za generisanje sertifiakta
			X509v3CertificateBuilder certGen = new JcaX509v3CertificateBuilder(issuerData.getX500name(),
					new BigInteger(subjectData.getSerialNumber()),
					subjectData.getStartDate(),
					subjectData.getEndDate(),
					subjectData.getX500name(),
					subjectData.getPublicKey());
			//Generise se sertifikat
			X509CertificateHolder certHolder = certGen.build(contentSigner);

			//Builder generise sertifikat kao objekat klase X509CertificateHolder
			//Nakon toga je potrebno certHolder konvertovati u sertifikat, za sta se koristi certConverter
			JcaX509CertificateConverter certConverter = new JcaX509CertificateConverter();
			certConverter = certConverter.setProvider(new BouncyCastleProvider());

			//Konvertuje objekat u sertifikat
			return certConverter.getCertificate(certHolder);
		} catch (CertificateEncodingException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (OperatorCreationException e) {
			e.printStackTrace();
		} catch (CertificateException e) {
			e.printStackTrace();
		}
		return null;
	}

	public KeyPair generateKeyPair() {
		try {
			KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
			SecureRandom random = SecureRandom.getInstance("SHA1PRNG", "SUN");
			keyGen.initialize(2048, random);
			return keyGen.generateKeyPair();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (NoSuchProviderException e) {
			e.printStackTrace();
		}
		return null;
	}

	public SubjectData generateSubjectData(PublicKey publicKey, CreateCertificateRequest ccRequest) throws Exception {

		String serialNumber = new BigInteger(128, new Random()).toString();

		return new SubjectData(publicKey,
				buildName(ccRequest.getCommonName(),
						ccRequest.getOrganization(),
						ccRequest.getCountry(),
						ccRequest.getCity()),
				serialNumber,
				ccRequest.getStartDate(),
				ccRequest.getEndDate()
		);
	}

	public IssuerData generateIssuerDate(PrivateKey privateKey, X509Certificate x509Certificate) throws CertificateEncodingException {
		return new IssuerData(privateKey, (new JcaX509CertificateHolder(x509Certificate).getSubject()));
	}

	public X500Name buildName(String commonName, String organization, String country, String city) {

		X500NameBuilder nameBuilder = new X500NameBuilder();

		if (!commonName.isEmpty()) {
			nameBuilder.addRDN(BCStyle.CN, commonName);
		}
		if (!organization.isEmpty()) {
			nameBuilder.addRDN(BCStyle.O, organization);
		}
		if (!country.isEmpty()) {
			nameBuilder.addRDN(BCStyle.C, country);
		}
		if (!city.isEmpty()) {
			nameBuilder.addRDN(BCStyle.L, city);
		}

		return nameBuilder.build();
	}
}
