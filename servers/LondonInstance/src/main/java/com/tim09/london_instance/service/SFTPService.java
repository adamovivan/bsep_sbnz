package com.tim09.london_instance.service;

import com.jcraft.jsch.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.util.Vector;

@Service
public class SFTPService {

    @Autowired
    private CertificateService certificateService;

    @Value("${server.ssl.key-store}")
    private String keystorePath;


    public Boolean receiveCertificate(String nodeName){
        try {
            JSch jsch = new JSch();
            Session session = jsch.getSession("Filip-PC", "192.168.0.11", 22);
            session.setPassword("zgadija");
            session.setConfig("StrictHostKeyChecking", "no");
            session.connect();

            Channel channel = session.openChannel( "sftp" );
            channel.connect();

            ChannelSftp sftpChannel = (ChannelSftp) channel;

            sftpChannel.get("sftp/" + nodeName + ".cer", "files/pki/certificates/" + nodeName + ".cer");

            certificateService.importCertificateToTruststore(null,null,null,null);

            sftpChannel.exit();
            session.disconnect();
            return true;
        } catch (JSchException e) {
            e.printStackTrace();
        } catch (SftpException e) {
            e.printStackTrace();
        }
        return false;
    }


    public Boolean sendCertificate(String nodeName){
        try {
            JSch jsch = new JSch();
            Session session = jsch.getSession("Filip-PC", "192.168.0.11", 22);
            session.setPassword("zgadija");
            session.setConfig("StrictHostKeyChecking", "no");
            session.connect();

            Channel channel = session.openChannel( "sftp" );
            channel.connect();

            ChannelSftp sftpChannel = (ChannelSftp) channel;

            certificateService.exportCertificate(keystorePath, "password", "cn=localhost,ou=" + nodeName + ",o=megatravel,l=" + nodeName + ",st=" + nodeName + ",c=en", "./files/pki/certificates/" + nodeName + ".cer");

            sftpChannel.put("files/pki/certificates/" + nodeName + ".cer", "sftp/" + nodeName + ".cer");

            sftpChannel.exit();
            session.disconnect();
            return true;
        } catch (JSchException e) {
            e.printStackTrace();
        } catch (SftpException e) {
            e.printStackTrace();
        }
        return false;
    }
}

