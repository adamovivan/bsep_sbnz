package com.tim09.mega_travel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MegaTravelApplication {
	public static void main(String[] args) {
		SpringApplication.run(MegaTravelApplication.class, args);
	}
}
