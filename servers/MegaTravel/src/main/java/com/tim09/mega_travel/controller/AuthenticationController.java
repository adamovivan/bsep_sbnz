package com.tim09.mega_travel.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tim09.mega_travel.controller.validation.ValidationUtil;
import com.tim09.mega_travel.dto.request.AuthenticationRequest;
import com.tim09.mega_travel.dto.response.AuthenticationResponse;
import com.tim09.mega_travel.model.User;
import com.tim09.mega_travel.security.spring.TokenUtils;
import com.tim09.mega_travel.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@RestController
//@CrossOrigin(origins = "https://localhost:4200")
@CrossOrigin(origins = "*")
@RequestMapping("/api/auth")
public class AuthenticationController extends ValidationUtil {

    private final AuthenticationManager authenticationManager;

    private final UserDetailsService userDetailsService;

    private final UserService userService;

    private final TokenUtils tokenUtils;

    @Autowired
    public AuthenticationController(AuthenticationManager authenticationManager, UserDetailsService userDetailsService, UserService userService, TokenUtils tokenUtils) {
        this.authenticationManager = authenticationManager;
        this.userDetailsService = userDetailsService;
        this.userService = userService;
        this.tokenUtils = tokenUtils;
    }


    /**
     * POST /api/auth
     */
    //@CrossOrigin
    @PostMapping
    public ResponseEntity<AuthenticationResponse> authenticate(@Valid @RequestBody String authenticationRequestJSON) {
        System.out.println("TESTIS");
        try {

            validateJSON(authenticationRequestJSON, "user.json");
            ObjectMapper mapper = new ObjectMapper();
            AuthenticationRequest authenticationRequest = mapper.readValue(authenticationRequestJSON, AuthenticationRequest.class);

            UsernamePasswordAuthenticationToken authToken =
                    new UsernamePasswordAuthenticationToken(authenticationRequest.getUsername(), authenticationRequest.getPassword());

            Authentication authentication = authenticationManager.authenticate(authToken);

            UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getUsername());
            //LoggedUserDTO user = UserConverter.fromLoggedEntity(userService.findByUsername(userDetails.getUsername()));
            User user = userService.findByUsername(userDetails.getUsername());
            String token = tokenUtils.generateToken(userDetails);

            return new ResponseEntity<>(new AuthenticationResponse(user.getId(), token), HttpStatus.OK);
        } catch (Exception e){
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }
}
