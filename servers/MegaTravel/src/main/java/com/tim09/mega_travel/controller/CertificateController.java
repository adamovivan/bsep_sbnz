package com.tim09.mega_travel.controller;

import com.tim09.mega_travel.controller.exception.CertificateNotFoundException;
import com.tim09.mega_travel.dto.CertificateDTOs;
import com.tim09.mega_travel.dto.request.CreateCertificateRequest;
import com.tim09.mega_travel.dto.response.CertificateStatusResponse;
import com.tim09.mega_travel.dto.response.SimpleResponse;
import com.tim09.mega_travel.model.CaAddress;
import com.tim09.mega_travel.repository.CaAddressRepository;
import com.tim09.mega_travel.service.CertificateService;
import com.tim09.mega_travel.service.SFTPService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;

@RestController
//@CrossOrigin(origins = "https://localhost:4200")
@CrossOrigin(origins = "*")
@RequestMapping("certificate")
public class CertificateController {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private CertificateService certificateService;

    @Autowired
    private SFTPService sftpService;

    @Autowired
    private CaAddressRepository caAddressRepository;

    @Value("${server.ssl.key-store}")
    private String keystorePath;

    @Value("${issued-certs-path}")
    private String issuedCertsPath;

    @GetMapping(value = "/connect/{serialNumber1}/{serialNumber2}")
    public void connect(@PathVariable String serialNumber1, @PathVariable String serialNumber2) {
        CaAddress caAddress1 = caAddressRepository.findCaAddressBySerialNumber(serialNumber1);  // london
        CaAddress caAddress2 = caAddressRepository.findCaAddressBySerialNumber(serialNumber2);  // hk
        System.out.println(caAddress1.getAddress() + "/certificate/sendCertificate/" + caAddress1.getCaName());
        restTemplate.getForObject(caAddress1.getAddress() + "/certificate/sendCertificate/" + caAddress1.getCaName(), Object.class);
        restTemplate.getForObject(caAddress2.getAddress() + "/certificate/sendCertificate/" + caAddress2.getCaName(), Object.class);

        restTemplate.getForObject(caAddress1.getAddress() + "/certificate/receiveCertificate/" + caAddress2.getCaName(), Object.class);
        restTemplate.getForObject(caAddress2.getAddress() + "/certificate/receiveCertificate/" + caAddress1.getCaName(), Object.class);
    }

    //    @PreAuthorize("hasAuthority('CREATE_CERTIFICATE')")
    @RequestMapping(value = "/createCert", method = RequestMethod.POST)
    public ResponseEntity<SimpleResponse> createCert(@RequestBody CreateCertificateRequest ccRequest) throws Exception {
        try {
            String caAddress = certificateService.getCaAddressBySerialNumber(ccRequest.getIssuerSerialNumber());
            HttpEntity<CreateCertificateRequest> request = new HttpEntity<>(ccRequest);
            ResponseEntity<Object> response = restTemplate
                    .exchange(caAddress + "/certificate/create", HttpMethod.POST, request, Object.class);
            System.out.println(response.getStatusCode());
            if (response.getStatusCodeValue() == 200)
                return ResponseEntity.ok().body(new SimpleResponse("CertificateData successfully added."));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new SimpleResponse(false, e.getMessage()));
        }
        return ResponseEntity.badRequest().body(new SimpleResponse(false, "Something went wrong."));
    }

    @RequestMapping(value = "/createCa", method = RequestMethod.POST)
    public ResponseEntity<SimpleResponse> createCA(@RequestBody CreateCertificateRequest ccRequest) throws Exception {
        try {
            X509Certificate generatedCert;
            if ((generatedCert = certificateService.createCertificate(ccRequest)) != null) {
                ccRequest.setSerialNumber(generatedCert.getSerialNumber().toString());
                certificateService.addCaAddressFromRequest(ccRequest);
                return ResponseEntity.ok().body(new SimpleResponse("CertificateData successfully added."));
            }
        } catch (CertificateEncodingException | IOException e) {
            return ResponseEntity.badRequest().body(new SimpleResponse(false, "Something went wrong."));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new SimpleResponse(false, e.getMessage()));
        }
        return ResponseEntity.badRequest().body(new SimpleResponse(false, "Something went wrong."));
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public ResponseEntity<SimpleResponse> createCertificate(@RequestBody CreateCertificateRequest ccRequest) throws Exception {
        try {
            if (certificateService.createCertificate(ccRequest) != null) {
                return ResponseEntity.ok().body(new SimpleResponse("CertificateData successfully added."));
            }
        } catch (CertificateEncodingException | IOException e) {
            return ResponseEntity.badRequest().body(new SimpleResponse(false, "Something went wrong."));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new SimpleResponse(false, e.getMessage()));
        }
        return ResponseEntity.badRequest().body(new SimpleResponse(false, "Something went wrong."));
    }


    @RequestMapping(value = "/addCaAddress", method = RequestMethod.POST)
    public ResponseEntity<SimpleResponse> createCA(@RequestBody CaAddress caAddress) throws Exception {
        if (certificateService.addCaAddress(caAddress)) {
            return ResponseEntity.ok().body(new SimpleResponse("CA info successfully added."));
        }
        return ResponseEntity.badRequest().body(new SimpleResponse(false, "Something went wrong."));
    }

    @RequestMapping(value = "/removeCaBySerialNumber", method = RequestMethod.POST)
    public ResponseEntity<SimpleResponse> removeCA(@RequestBody String serialNumber) throws Exception {
        if (certificateService.removeCaAddress(serialNumber)) {
            return ResponseEntity.ok().body(new SimpleResponse("CA info successfully added."));
        }
        return ResponseEntity.badRequest().body(new SimpleResponse(false, "Something went wrong."));
    }

//    @PreAuthorize("hasAuthority('READ_CERTIFICATE')")
//    @RequestMapping(value = "/getAll/{certificateAuthority}", method = RequestMethod.GET)
//    public ResponseEntity<List<CertificateDTO>> getAllCertificates(@PathVariable("certificateAuthority") String certificateAuthority){
//        List<CertificateDTO> certificateDTOs = certificateService.getAllCertificates(certificateAuthority);
//        return ResponseEntity.ok().body(certificateDTOs);
//    }

    @RequestMapping(value = "/sendCertChain", method = RequestMethod.GET)
    public ResponseEntity<Boolean> sendCertChain() {
        Boolean valid = sftpService.testSftp();
        return ResponseEntity.ok().body(valid);
    }


    @RequestMapping(value = "/checkValidity/{certificateAuthority}", method = RequestMethod.GET)
    public ResponseEntity<Boolean> checkValidity(@PathVariable("certificateAuthority") String certificateAuthority) {
        Boolean valid = certificateService.checkValidity(certificateAuthority);
        return ResponseEntity.ok().body(valid);
    }

    @RequestMapping(value = "/revokeCertificate/{serialNumber}", method = RequestMethod.POST)
    public ResponseEntity<SimpleResponse> revokeCertificate(@PathVariable("serialNumber") String serialNumber) {
        try {
            certificateService.revokeCertificate(serialNumber);
        } catch (CertificateNotFoundException e) {
            return ResponseEntity.badRequest().body(new SimpleResponse(false, "Certificate does not exist."));
        }

        return ResponseEntity.ok().body(new SimpleResponse("Certificate successfully revoked."));
    }

    @RequestMapping(value = "/getRevocationStatus/{serialNumber}", method = RequestMethod.GET)
    public ResponseEntity<CertificateStatusResponse> getRevocationStatus(@PathVariable("serialNumber") String serialNumber) {
        return ResponseEntity.ok().body(new CertificateStatusResponse(serialNumber, certificateService.getCertificateRevocationStatus(serialNumber)));
    }

    @RequestMapping(value = "/getIssuedCertificates", method = RequestMethod.GET)
    public ResponseEntity<CertificateDTOs> getIssuedCertificates() {
        CertificateDTOs certificateDTOs = certificateService.getCertificates(issuedCertsPath);
        return ResponseEntity.ok().body(certificateDTOs);
    }

    @RequestMapping(value = "/getCACertificate", method = RequestMethod.GET)
    public ResponseEntity<CertificateDTOs> getCACertificate() {
        CertificateDTOs certificateDTOs = certificateService.getCertificates(keystorePath);
        return ResponseEntity.ok().body(certificateDTOs);
    }
}
