package com.tim09.mega_travel.controller;

import com.tim09.mega_travel.dto.response.SimpleResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("test")
public class TestController {

    @PreAuthorize("hasAuthority('CREATE_CERTIFICATE')")
    @RequestMapping(value = "/role-user", method = RequestMethod.GET)
    public ResponseEntity<SimpleResponse> getTest(){
        return ResponseEntity.ok().body(new SimpleResponse("This is response. Role USER."));
    }

    @PreAuthorize("hasAuthority('READ_CERTIFICATE')")
    @RequestMapping(value = "/role-user2", method = RequestMethod.GET)
    public ResponseEntity<SimpleResponse> getTest2(){
        return ResponseEntity.ok().body(new SimpleResponse("This is response. Role USER2."));
    }
}
