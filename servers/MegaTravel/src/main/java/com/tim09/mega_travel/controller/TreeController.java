package com.tim09.mega_travel.controller;

import com.tim09.mega_travel.model.Tree;
import com.tim09.mega_travel.service.TreeService;
import javafx.util.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/tree")
public class TreeController {

    @Autowired
    TreeService treeService;

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public Tree getTree() {
//        Node level1A = new Node("1", "London CA", NodeType.IM_CA);
//        level1A.setParentId("0");
//        Node level1B = new Node("2", "Berlin CA", NodeType.IM_CA);
//        level1B.setParentId("0");
//        Node level2A = new Node("3", "HK CA", NodeType.LEAF_CA);
//        level2A.setParentId("1");
//        // Certificate mocks
//        CreateCertificateRequest cr1 = new CreateCertificateRequest();
//
//        Node cert1 = new Node("4", "LONDON CERT1 MOCK", NodeType.CERTIFICATE);
//        cert1.setParentId("1");
//        cert1.setData(cr1);
//        Node cert2 = new Node("5", "BERLIN CERT1 MOCK", NodeType.CERTIFICATE);
//        cert2.setParentId("2");
//        cert2.setData(cr1);
//        // Form tree
//        Tree testTree = new Tree(new Node("0", "Root CA", NodeType.ROOT_CA));
//        testTree.addNode(level1A);
//        testTree.addNode(level1B);
//        testTree.addNode(level2A);
//        testTree.addNode(cert1);
//        testTree.addNode(cert2);
        //return testTree;

        //return new Tree();
        Tree t2 = treeService.loadTree();
        t2.printTree();
        return t2;
    }

    @GetMapping(value = "/connections", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Pair<String, String>> getTreeConnections() {
        ArrayList<Pair<String, String>> pairList = new ArrayList<>();
        treeService.makePairs(pairList);
        return pairList;
    }

}
