package com.tim09.mega_travel.controller;

import com.tim09.mega_travel.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
//@CrossOrigin(origins = {"http://localhost:8080", "http://localhost:4200"})
@CrossOrigin(origins = "*")
@RequestMapping("users")
public class UserController {

    @Autowired
    private UserService userService;

    public UserController() { }

    @GetMapping("/currentUser")
    public ResponseEntity findCurrentUser() {
        try {
            return new ResponseEntity<>(userService.findCurrentUser(), HttpStatus.OK);
        } catch (Exception fe){
            return new ResponseEntity(HttpStatus.FORBIDDEN);
        }
    }
}
