package com.tim09.mega_travel.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CertificateDTOs implements Serializable {
    private ArrayList<CertificateDTO> cerificateList;

    public CertificateDTOs(){
    }

    public CertificateDTOs(List<CertificateDTO> cerificateList){
        this.setCerificateList(cerificateList);
    }

    public ArrayList<CertificateDTO> getCerificateList() {
        return cerificateList;
    }

    public void setCerificateList(List<CertificateDTO> cerificateList) {
        this.cerificateList = (ArrayList<CertificateDTO>) cerificateList;
    }
}
