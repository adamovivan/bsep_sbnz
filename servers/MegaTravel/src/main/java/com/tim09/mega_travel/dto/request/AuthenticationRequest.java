package com.tim09.mega_travel.dto.request;

import javax.validation.constraints.NotEmpty;


public class AuthenticationRequest {

    @NotEmpty
    private String username;

    @NotEmpty
    private String password;

    public AuthenticationRequest() {
    }

    public AuthenticationRequest(String username, String password) {
        this.setUsername(username);
        this.setPassword(password);
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}