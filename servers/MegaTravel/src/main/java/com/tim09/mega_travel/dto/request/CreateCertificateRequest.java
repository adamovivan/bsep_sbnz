package com.tim09.mega_travel.dto.request;

import java.util.Date;


public class CreateCertificateRequest {
    private String issuerPrincipalName;
    private String issuerKeyStorePassword;
    private String issuerPrivateKeyPassword;
    private String commonName;
    private String organization;
    private String country;
    private String city;
    private Date startDate;
    private Date endDate;

    private String issuerSerialNumber;
    private String ipAddress;
    private Integer level;
    private String serialNumber;

    public String getIssuerSerialNumber() {
        return issuerSerialNumber;
    }

    public void setIssuerSerialNumber(String issuerSerialNumber) {
        this.issuerSerialNumber = issuerSerialNumber;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getIssuerKeyStorePassword() {
        return issuerKeyStorePassword;
    }

    public void setIssuerKeyStorePassword(String issuerKeyStorePassword) {
        this.issuerKeyStorePassword = issuerKeyStorePassword;
    }

    public String getIssuerPrivateKeyPassword() {
        return issuerPrivateKeyPassword;
    }

    public void setIssuerPrivateKeyPassword(String issuerPrivateKeyPassword) {
        this.issuerPrivateKeyPassword = issuerPrivateKeyPassword;
    }

    public String getCommonName() {
        return commonName;
    }

    public void setCommonName(String commonName) {
        this.commonName = commonName;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getIssuerPrincipalName() {
        return issuerPrincipalName;
    }

    public void setIssuerPrincipalName(String issuerPrincipalName) {
        this.issuerPrincipalName = issuerPrincipalName;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        return "CreateCertificateRequest{" +
                "issuerPrincipalName='" + issuerPrincipalName + '\'' +
                ", issuerKeyStorePassword='" + issuerKeyStorePassword + '\'' +
                ", issuerPrivateKeyPassword='" + issuerPrivateKeyPassword + '\'' +
                ", commonName='" + commonName + '\'' +
                ", organization='" + organization + '\'' +
                ", country='" + country + '\'' +
                ", city='" + city + '\'' +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", ipAddress='" + ipAddress + '\'' +
                ", level=" + level +
                ", serialNumber='" + serialNumber + '\'' +
                '}';
    }
}
