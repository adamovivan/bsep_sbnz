package com.tim09.mega_travel.dto.response;

import com.tim09.mega_travel.model.enums.CertificateStatus;

public class CertificateStatusResponse {
    private String serialNumber;
    private CertificateStatus certificateStatus;

    public CertificateStatusResponse(String serialNumber, CertificateStatus certificateStatus) {
        this.serialNumber = serialNumber;
        this.certificateStatus = certificateStatus;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public CertificateStatus getCertificateStatus() {
        return certificateStatus;
    }

    public void setCertificateStatus(CertificateStatus certificateStatus) {
        this.certificateStatus = certificateStatus;
    }
}
