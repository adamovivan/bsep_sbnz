package com.tim09.mega_travel.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class CaAddress implements Serializable, Comparable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String serialNumber;

    @Column
    private String caName;

    @Column(nullable = false)
    private String address;

    @Column
    private int lvl;

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCaName() {
        return caName;
    }

    public void setCaName(String caName) {
        this.caName = caName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getLvl() {
        return lvl;
    }

    public void setLvl(int lvl) {
        this.lvl = lvl;
    }

    @Override
    public String toString() {
        return "CaAddress{" +
                "id=" + id +
                ", caName='" + caName + '\'' +
                ", address='" + address + '\'' +
                ", lvl=" + lvl +
                '}';
    }

    @Override
    public int compareTo(Object o) {
        return Long.compare(this.getLvl(),((CaAddress)o).getLvl());
    }
}
