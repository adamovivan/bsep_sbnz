package com.tim09.mega_travel.model;

import com.tim09.mega_travel.model.enums.CertificateStatus;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class CertificateData implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String serialNumber;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private CertificateStatus certificateStatus;

    @Column
    private String principalName;

    @Column
    private String keyStorePasswordSalt;

    @Column
    private String privateKeyPasswordSalt;


    public CertificateData() {
    }

    public CertificateData(Long id, String serialNumber, String principalName, CertificateStatus certificateStatus) {
        this.id = id;
        this.serialNumber = serialNumber;
        this.principalName = principalName;
        this.certificateStatus = certificateStatus;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getPrincipalName() {
        return principalName;
    }

    public void setPrincipalName(String principalName) {
        this.principalName = principalName;
    }

    public CertificateStatus getCertificateStatus() {
        return certificateStatus;
    }

    public void setCertificateStatus(CertificateStatus certificateStatus) {
        this.certificateStatus = certificateStatus;
    }

    public String getKeyStorePasswordSalt() {
        return keyStorePasswordSalt;
    }

    public void setKeyStorePasswordSalt(String keyStorePasswordSalt) {
        this.keyStorePasswordSalt = keyStorePasswordSalt;
    }

    public String getPrivateKeyPasswordSalt() {
        return privateKeyPasswordSalt;
    }

    public void setPrivateKeyPasswordSalt(String privateKeyPasswordSalt) {
        this.privateKeyPasswordSalt = privateKeyPasswordSalt;
    }

    @Override
    public String toString() {
        return "CertificateData{" +
                "id=" + id +
                ", serialNumber='" + serialNumber + '\'' +
                ", principalName='" + principalName + '\'' +
                ", certificateStatus=" + certificateStatus +
                ", keyStorePasswordSalt='" + keyStorePasswordSalt + '\'' +
                ", privateKeyPasswordSalt='" + privateKeyPasswordSalt + '\'' +
                '}';
    }
}
