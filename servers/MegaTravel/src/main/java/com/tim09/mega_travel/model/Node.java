package com.tim09.mega_travel.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.tim09.mega_travel.dto.CertificateDTO;
import com.tim09.mega_travel.model.enums.NodeType;
import com.tim09.mega_travel.service.CaService;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import java.util.ArrayList;
import java.util.List;

@JsonIdentityInfo(generator = JSOGGenerator.class)
public class Node {
    private String id;
    private String name;
    private NodeType type;
    private Integer level;
    private Object data;
    private String parentId;
    private List<Node> children;
    private boolean revoked;

    public Node() {
        this.parentId = "-1";
        this.level = 0;
        this.children = new ArrayList<>();
    }

    public Node(String id, String name, NodeType type) {
        this();
        this.id = id;
        this.name = name;
        this.type = type;
    }

    public Node(String id, String name, Object data, NodeType type, String parentId) {
        this();
        this.id = id;
        this.name = name;
        this.data = data;
        this.type = type;
        this.parentId = parentId;
    }

    public Node(CertificateDTO certificateDTO, NodeType nodeType) {
        this();
        this.id = certificateDTO.getSerialNumber();
        if (certificateDTO.getParentCertificate() != null)
            this.parentId = certificateDTO.getParentCertificate().getSerialNumber();
        this.name = certificateDTO.getIssuedTo();
        this.type = nodeType;
        this.data = certificateDTO;

    }

    public void addNodeByParentId(Node n, Integer level) {
        if (this.id.equals(n.getParentId())) {
            // Set/Update level
            n.setLevel(level);
            // Don't add if duplicate
            if (!duplicateChild(id, this.children))
                this.children.add(n);
        }
        for (Node child : this.children) {
            child.addNodeByParentId(n, level + 1);
        }
    }

    // Build tree from DB
//    public void buildFromDB(CaService caService, Integer level) {
//        List<Node> children = caService.getChildren(this.getId());
//        this.children.addAll(children);
//        for (Node child : this.children) {
//            child.buildFromDB(caService, level + 1);
//        }
//    }

    private boolean duplicateChild(String id, List<Node> children) {
        for (Node node : children) {
            if (id.equals(node.getId()))
                return true;
        }
        return false;
    }

    public void printNodes() {
        System.out.println(this.toString());
        for (Node child : this.children) {
            child.printNodes();
        }
    }

    public String getId() {
        return id;
    }

    public boolean isRevoked() {
        return revoked;
    }

    public void setRevoked(boolean revoked) {
        this.revoked = revoked;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public NodeType getType() {
        return type;
    }

    public void setType(NodeType type) {
        this.type = type;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public List<Node> getChildren() {
        return children;
    }

    public void setChildren(List<Node> children) {
        this.children = children;
    }

    @Override
    public String toString() {
        return "Node{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", parentId=" + parentId +
                '}';
    }
}