package com.tim09.mega_travel.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

@JsonIdentityInfo(generator = JSOGGenerator.class)
public class Tree {

    private Node root;

    public Tree() {
    }

//    public Tree buildFromDB(CaService caService) {
//        if (root != null) {
//            this.build(caService);
//            return this;
//        }
//        return null;
//    }

    public Tree(Node node) {
        root = node;
    }

    // Add node to tree (search from root node)
    public void addNode(Node newNode) {
        this.root.addNodeByParentId(newNode, 1);
    }

    // Build tree from database
//    private void build(CaService caService) {
//        this.root.buildFromDB(caService, 1);
//    }

    // Print complete tree
    public void printTree() {
        this.root.printNodes();
    }

    public Node getRoot() {
        return root;
    }

    public void setRoot(Node root) {
        this.root = root;
    }

}
