package com.tim09.mega_travel.model;

public interface TreeNode {
    Long getParentId();
    Long getId();
}
