package com.tim09.mega_travel.model;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String username;

    private String password;

    private Date lastPasswordReset;

    private int failedSignIns;

    private Date lastFailedSignIn;

    @ManyToMany(mappedBy = "users", fetch = FetchType.EAGER)
    private Set<Role> roles;

    public User() { }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getLastPasswordReset() {
        return lastPasswordReset;
    }

    public void setLastPasswordReset(Date lastPasswordReset) {
        this.lastPasswordReset = lastPasswordReset;
    }

    public int getFailedSignIns() {
        return failedSignIns;
    }

    public void setFailedSignIns(int failedSignIns) {
        this.failedSignIns = failedSignIns;
    }

    public Date getLastFailedSignIn() {
        return lastFailedSignIn;
    }

    public void setLastFailedSignIn(Date lastFailedSignIn) {
        this.lastFailedSignIn = lastFailedSignIn;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }
}
