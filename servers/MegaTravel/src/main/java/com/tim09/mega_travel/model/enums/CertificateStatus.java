package com.tim09.mega_travel.model.enums;

public enum CertificateStatus {
    GOOD,
    REVOKED,
    UNKNOWN
}
