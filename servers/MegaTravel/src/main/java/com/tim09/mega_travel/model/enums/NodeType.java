package com.tim09.mega_travel.model.enums;

public enum NodeType {
    ROOT_CA,
    IM_CA,
    LEAF_CA,
    CERTIFICATE
}
