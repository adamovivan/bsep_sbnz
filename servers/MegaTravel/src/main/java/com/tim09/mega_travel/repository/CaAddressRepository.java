package com.tim09.mega_travel.repository;

import com.tim09.mega_travel.model.CaAddress;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CaAddressRepository extends JpaRepository<CaAddress, Long> {
    public CaAddress findCaAddressBySerialNumber(String serialNumber);
}
