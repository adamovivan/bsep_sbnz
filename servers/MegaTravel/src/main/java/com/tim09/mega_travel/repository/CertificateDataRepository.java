package com.tim09.mega_travel.repository;

import com.tim09.mega_travel.model.CertificateData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CertificateDataRepository extends JpaRepository<CertificateData, Long> {

    CertificateData findBySerialNumber(String serialNumber);
    CertificateData findByPrincipalName(String principalName);

}
