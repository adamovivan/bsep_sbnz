package com.tim09.mega_travel.security.certificate.pki.communication;

import com.tim09.mega_travel.util.Base64Utility;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import javax.crypto.*;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.ByteBuffer;
import java.security.*;
import java.util.Base64;

public class SecureCommunication {

    public void testIt() {
        Security.addProvider(new BouncyCastleProvider());

        String message = "Ovo su podaci koje Alisa treba da posalje Bobu";

        //Sender private and public key pair (Alice)
        KeyPair senderKeyPair = generateKeyPair();

        //Receiver private and public key pair (Bob)
        KeyPair receiverKeyPair = generateKeyPair();

        //System.out.println("Public key: " + Base64Utility.encode(publicKey.getEncoded()));

        byte[] signature = sign(message.getBytes(), senderKeyPair.getPrivate());

        byte[] encrypted = encryptPGP(message, receiverKeyPair.getPublic());

        //System.out.println("Encrypted message: " +  Base64Utility.encode(encrypted));

        byte[] decrypted = decryptPGP(encrypted, receiverKeyPair.getPrivate());

        System.out.println(new String(decrypted));
        if (verify(decrypted, signature, senderKeyPair.getPublic()))
            System.out.println("Verified!");
        else
            System.out.println("Not verified.");
    }


    private SecretKey generateSecretKey() {
        SecureRandom secureRandom = new SecureRandom();
        byte[] key = new byte[16];
        secureRandom.nextBytes(key);
        SecretKey secretKey = new SecretKeySpec(key, "AES");

        return secretKey;

    }


    private KeyPair generateKeyPair() {
        try {
            KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
            SecureRandom random = SecureRandom.getInstance("SHA1PRNG", "SUN");
            keyGen.initialize(4096, random);

            return keyGen.generateKeyPair();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        }
        return null;
    }


    private byte[] decryptRSA(byte[] cipherText, PrivateKey key) {
        try {
            Cipher rsaCipherDec = Cipher.getInstance("RSA/ECB/PKCS1Padding", "BC");
            rsaCipherDec.init(Cipher.DECRYPT_MODE, key);

            byte[] plainText = rsaCipherDec.doFinal(cipherText);
            return plainText;
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        }
        return null;
    }


    private byte[] encryptRSA(byte[] encrypyedSecretKey, PublicKey publicKey) {

        try {
            Cipher rsaCipherEnc = Cipher.getInstance("RSA/ECB/PKCS1Padding", "BC");
            rsaCipherEnc.init(Cipher.ENCRYPT_MODE, publicKey);

            byte[] cipherText = rsaCipherEnc.doFinal(encrypyedSecretKey);
            return cipherText;

        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        }
        return null;


    }


    private byte[] encryptPGP(String plainText, PublicKey publicKey) {
        SecretKey secretKey = generateSecretKey();
        System.out.println("Secret key: " +   Base64Utility.encode(secretKey.getEncoded()));
        SecureRandom secureRandom = new SecureRandom();
        byte[] iv = new byte[12]; //NEVER REUSE THIS IV WITH SAME KEY
        secureRandom.nextBytes(iv);

        try {
            final Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding");
            GCMParameterSpec parameterSpec = new GCMParameterSpec(128, iv); //128 bit auth tag length
            cipher.init(Cipher.ENCRYPT_MODE, secretKey, parameterSpec);

            byte[] cipherText = cipher.doFinal(plainText.getBytes());

            byte[] encryptedSessionKey = encryptRSA(secretKey.getEncoded(), publicKey);

            ByteBuffer byteBuffer = ByteBuffer.allocate(4 + 4 + iv.length + cipherText.length + encryptedSessionKey.length);
            byteBuffer.putInt(iv.length);
            byteBuffer.putInt(cipherText.length);
            byteBuffer.put(iv);
            byteBuffer.put(cipherText);
            byteBuffer.put(encryptedSessionKey);
            byte[] cipherMessage = byteBuffer.array();

            String base64CipherMessage = Base64.getEncoder().encodeToString(cipherMessage);

            return base64CipherMessage.getBytes();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        }
        return null;
    }

    private byte[] decryptPGP(byte[] cipherText1, PrivateKey key) {
        byte[] cipherMessage = Base64.getDecoder().decode(cipherText1);
        ByteBuffer byteBuffer = ByteBuffer.wrap(cipherMessage);
        int ivLength = byteBuffer.getInt();
        if(ivLength < 12 || ivLength >= 16) { // check input parameter
            throw new IllegalArgumentException("invalid iv length");
        }
        int cipherTextLength = byteBuffer.getInt();
        byte[] iv = new byte[ivLength];
        byteBuffer.get(iv);

        byte[] cipherText = new byte[cipherTextLength];
        byteBuffer.get(cipherText);

        byte[] encodedKey = new byte[byteBuffer.remaining()];
        byteBuffer.get(encodedKey);

        encodedKey = decryptRSA(encodedKey, key);

        try {
            final Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding");
            cipher.init(Cipher.DECRYPT_MODE, new SecretKeySpec(encodedKey, "AES"), new GCMParameterSpec(128, iv));
            return cipher.doFinal(cipherText);

        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        }
        return null;
    }

    private byte[] sign(byte[] data, PrivateKey privateKey) {
        try {
            Signature sig = Signature.getInstance("SHA1withRSA");
            sig.initSign(privateKey);
            sig.update(data);

            return sig.sign();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (SignatureException e) {
            e.printStackTrace();
        }
        return null;
    }

    private boolean verify(byte[] data, byte[] signature, PublicKey publicKey) {
        try {
            Signature sig = Signature.getInstance("SHA1withRSA");
            sig.initVerify(publicKey);
            sig.update(data);

            return sig.verify(signature);
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (SignatureException e) {
            e.printStackTrace();
        }
        return false;
    }
}

