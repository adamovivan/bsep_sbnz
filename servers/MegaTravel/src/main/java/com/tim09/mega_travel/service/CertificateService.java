package com.tim09.mega_travel.service;

import com.tim09.mega_travel.controller.exception.CertificateNotFoundException;
import com.tim09.mega_travel.dto.CertificateDTO;
import com.tim09.mega_travel.dto.CertificateDTOs;
import com.tim09.mega_travel.dto.request.CreateCertificateRequest;
import com.tim09.mega_travel.model.CaAddress;
import com.tim09.mega_travel.model.CertificateData;
import com.tim09.mega_travel.model.enums.CertificateStatus;
import com.tim09.mega_travel.repository.CaAddressRepository;
import com.tim09.mega_travel.repository.CertificateDataRepository;
import com.tim09.mega_travel.security.certificate.pki.certificates.CertificateGenerator;
import com.tim09.mega_travel.security.certificate.pki.data.IssuerData;
import com.tim09.mega_travel.security.certificate.pki.data.SubjectData;
import com.tim09.mega_travel.security.certificate.pki.keystores.KeyStoreReader;
import com.tim09.mega_travel.security.certificate.pki.keystores.KeyStoreWriter;
import com.tim09.mega_travel.util.Base64Utility;
import com.tim09.mega_travel.util.Constants;
import com.tim09.mega_travel.util.HashUtility;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CertificateService {

    @Autowired
    private CertificateDataRepository certificateDataRepository;

    @Autowired
    private CertificateGenerator certGenerator;

    @Autowired
    private KeyStoreWriter ksWriter;

    @Autowired
    private KeyStoreReader ksReader;

    @Autowired
    CaAddressRepository caAddressRepository;

    @Value("${server.ssl.key-store-password}")
    private String keystorePass;

    @Value("${server.ssl.key-store}")
    private String keystorePath;

    @Value("${issued-certs-path}")
    private String issuedCertsPath;

    public String getCaAddressBySerialNumber(String serialNumber) {
        CaAddress caAddress = caAddressRepository.findCaAddressBySerialNumber(serialNumber);
        return caAddress.getAddress();
    }

    public List<CaAddress> getSortedCaAddresses() {
        List<CaAddress> addresses = caAddressRepository.findAll();
        addresses.sort(CaAddress::compareTo);
        System.out.println(addresses);
        return addresses;
    }

    public CertificateDTOs getCertificates(String filePath) {
        List<Certificate[]> certificates = ksReader.readAllCertificates(filePath, keystorePass);
        List<CertificateDTO> certificateDTOs = certificates.stream().map(this::certificatesToCertificateDTO).collect(Collectors.toList());
        return new CertificateDTOs(certificateDTOs);
    }

    private List<CertificateDTO> readAllCertificates(String fileName, String keyStorePassword) {
        List<Certificate[]> certificates = ksReader.readAllCertificates(fileName, keyStorePassword);

        return certificates.stream().map(this::certificatesToCertificateDTO).collect(Collectors.toList());
    }

    public X509Certificate createCertificate(CreateCertificateRequest ccRequest) throws Exception {
        String privateKeyPasswordSalt = certificateDataRepository.findByPrincipalName(ccRequest.getIssuerPrincipalName()).getPrivateKeyPasswordSalt();

        if (privateKeyPasswordSalt == null) {
            privateKeyPasswordSalt = Base64Utility.encode(HashUtility.generateSalt());
        }

        String privateKeyPassword = Base64Utility.encode(HashUtility.hashPassword(ccRequest.getIssuerPrivateKeyPassword(),
                Base64Utility.decode(privateKeyPasswordSalt)));

        Certificate[] certificates = ksReader.readCertificates(keystorePath, keystorePass, ccRequest.getIssuerPrincipalName());

        if (certificates == null || certificates.length == 0) {
            return null;
        }

        PrivateKey privateKey = ksReader.readPrivateKey(keystorePath, keystorePass, ccRequest.getIssuerPrincipalName(), ccRequest.getIssuerPrivateKeyPassword());
        KeyPair keyPair = certGenerator.generateKeyPair();

        IssuerData issuerData = certGenerator.generateIssuerDate(privateKey,
                (X509Certificate) certificates[0]);
        SubjectData subjectData = certGenerator.generateSubjectData(keyPair.getPublic(), ccRequest);

        X509Certificate x509Certificate = certGenerator.generateCertificate(subjectData, issuerData);
        Certificate[] certsExtended = ArrayUtils.add(certificates, x509Certificate);

        ksWriter.loadKeyStore(issuedCertsPath, keystorePass.toCharArray());
        ksWriter.write(x509Certificate.getSubjectX500Principal().getName(), keyPair.getPrivate(), privateKeyPassword.toCharArray(), certsExtended);
        ksWriter.saveKeyStore(issuedCertsPath, keystorePass.toCharArray());

        CertificateData certificateData = certificateDataRepository.findBySerialNumber(x509Certificate.getSerialNumber().toString());
        if (certificateData == null) {
            certificateData = new CertificateData();
            certificateData.setSerialNumber(x509Certificate.getSerialNumber().toString());
            certificateData.setKeyStorePasswordSalt(privateKeyPasswordSalt);
            certificateData.setPrivateKeyPasswordSalt(privateKeyPasswordSalt);
            certificateData.setCertificateStatus(CertificateStatus.GOOD);
            certificateDataRepository.save(certificateData);
        }

        return x509Certificate;
    }

    public CertificateDTO certificatesToCertificateDTO(Certificate[] certificates) {

        CertificateDTO parent = null;
        CertificateDTO certDTO = null;

        for (Certificate cert : certificates) {
            X509Certificate x509Cert = (X509Certificate) cert;

            certDTO = new CertificateDTO();
            certDTO.setParentCertificate(parent);
            certDTO.setVersion(x509Cert.getVersion());
            certDTO.setIssuedBy(x509Cert.getIssuerX500Principal().getName());
            certDTO.setIssuedTo(x509Cert.getSubjectX500Principal().getName());
            certDTO.setPublicKey(x509Cert.getPublicKey().getEncoded());
            certDTO.setPrincipalName(x509Cert.getSubjectX500Principal().getName());
            certDTO.setSerialNumber(x509Cert.getSerialNumber().toString());
            certDTO.setValidFrom(x509Cert.getNotBefore());
            certDTO.setValidTo(x509Cert.getNotAfter());
            certDTO.setSignature(x509Cert.getSignature());

            parent = certDTO;
        }

        return certDTO;
    }


    public CertificateData revokeCertificate(String serialNumber) throws CertificateNotFoundException {
        CertificateData certificateData = certificateDataRepository.findBySerialNumber(serialNumber);

        if (certificateData == null) {
//            throw new CertificateNotFoundException();
            certificateData = new CertificateData();
            certificateData.setSerialNumber(serialNumber);
        }

        certificateData.setCertificateStatus(CertificateStatus.REVOKED);
        certificateDataRepository.save(certificateData);

        return certificateData;
    }


    private Certificate[] getCertificateChain(String fileName, String keyStorePassword) {
        return ksReader.readAllCertificates(fileName, keyStorePassword).get(0);
    }


    private Boolean checkCertificateDate(X509Certificate x509Certificate) {
        Date now = new Date(119, 9, 22);

        Date dateOfIssue = x509Certificate.getNotBefore();
        Date expirationDate = x509Certificate.getNotAfter();

        return now.after(dateOfIssue) && now.before(expirationDate);

        /*try {
            x509Certificate.checkValidity();
        } catch (CertificateExpiredException e) {
            return false;
        } catch (CertificateNotYetValidException e) {
            return false;
        }
        return true;*/
    }


    private Boolean verifyCertificateSignature(X509Certificate x509Certificate, PublicKey parentPublicKey) {
        try {
            x509Certificate.verify(parentPublicKey);
        } catch (CertificateException e) {
            return false;
        } catch (NoSuchAlgorithmException e) {
            return false;
        } catch (InvalidKeyException e) {
            return false;
        } catch (NoSuchProviderException e) {
            return false;
        } catch (SignatureException e) {
            return false;
        }
        return true;
    }

    public CertificateStatus getCertificateRevocationStatus(String serialNumber) {
        CertificateData certificateData = this.certificateDataRepository.findBySerialNumber(serialNumber);

        if (certificateData == null) {
            return CertificateStatus.UNKNOWN;
        }

        return certificateData.getCertificateStatus();
    }

    public Boolean checkValidity(String certificateAuthority) {
        Certificate[] certificates = getCertificateChain(Constants.KEY_STORE_FILE_CA_LONDON, "london");

        for (int i = certificates.length - 1; i >= 0; i--) {
            X509Certificate x509Certificate;
            PublicKey parentPublicKey;

            if (i == 0) {
                x509Certificate = (X509Certificate) certificates[i];
                parentPublicKey = x509Certificate.getPublicKey();

            } else {
                x509Certificate = (X509Certificate) certificates[i];
                parentPublicKey = certificates[i - 1].getPublicKey();
            }

            if (!checkCertificateDate(x509Certificate) || getCertificateRevocationStatus(x509Certificate.getSerialNumber().toString()) != CertificateStatus.GOOD
                    || !verifyCertificateSignature(x509Certificate, parentPublicKey)) {
                return false;
            }
        }

        return true;
    }

    public boolean addCaAddress(CaAddress caAddress) {
        caAddressRepository.save(caAddress);
        return true;
    }

    public boolean removeCaAddress(String serialNumber) {
        CaAddress caAddress1 = caAddressRepository.findCaAddressBySerialNumber(serialNumber);
        caAddressRepository.deleteById(caAddress1.getId());
        return true;
    }

    public void addCaAddressFromRequest(CreateCertificateRequest ccRequest) {
        CaAddress caAddress = new CaAddress();
        caAddress.setCaName(ccRequest.getCommonName());
        caAddress.setLvl(ccRequest.getLevel());
        caAddress.setAddress(ccRequest.getIpAddress());
        caAddress.setSerialNumber(ccRequest.getSerialNumber());
        caAddressRepository.save(caAddress);
    }
}
