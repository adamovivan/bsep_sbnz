package com.tim09.mega_travel.service;

import com.jcraft.jsch.*;
import org.springframework.stereotype.Service;

import java.io.InputStream;

@Service
public class SFTPService {


    public Boolean testSftp(){
        try {
            JSch jsch = new JSch();
            Session session = jsch.getSession("Filip-PC", "localhost", 22);
            //Session session = jsch.getSession("localhost");
            session.setPassword("zgadija");
            session.setConfig("StrictHostKeyChecking", "no");
            session.connect();

            Channel channel = session.openChannel( "sftp" );
            channel.connect();

            Channel channelGet = session.openChannel( "sftp" );
            channelGet.connect();

            ChannelSftp sftpChannel = (ChannelSftp) channel;
            ChannelSftp sftpChannelGet = (ChannelSftp) channelGet;

            //inputStream = getInputStream(fromFilePath, true);
            sftpChannel.put(sftpChannelGet.get( "prvi.txt" ), "proba.txt");

            //sftpChannel.put("sftp\\prvi.txt");

            //sftpChannel.get("C:\\Users\\Filip-PC\\Desktop\\IV godina\\II semestar\\BSEP\\prvi.txt", "C:\\Users\\Filip-PC\\Desktop\\IV godina\\II semestar\\BSEP\\drugi.txt" );
// OR
            //InputStream in = sftpChannel.get( "sftp\\prvi.txt" );
            // process inputstream as needed

            sftpChannel.exit();
            sftpChannelGet.exit();
            session.disconnect();
            return true;
        } catch (JSchException e) {
            e.printStackTrace();
        } catch (SftpException e) {
            e.printStackTrace();
        }
        return false;
    }
}
