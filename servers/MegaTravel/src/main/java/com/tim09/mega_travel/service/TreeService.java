package com.tim09.mega_travel.service;

import com.tim09.mega_travel.dto.CertificateDTO;
import com.tim09.mega_travel.dto.CertificateDTOs;
import com.tim09.mega_travel.model.CaAddress;
import com.tim09.mega_travel.model.CertificateData;
import com.tim09.mega_travel.model.Node;
import com.tim09.mega_travel.model.Tree;
import com.tim09.mega_travel.model.enums.CertificateStatus;
import com.tim09.mega_travel.model.enums.NodeType;
import com.tim09.mega_travel.repository.CertificateDataRepository;
import javafx.util.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Service
public class TreeService {

    @Autowired
    CertificateService certificateService;

    @Autowired
    private CertificateDataRepository certificateDataRepository;

    @Autowired
    RestTemplate restTemplate;

    public Tree loadTree() {
        // Load CAs
        ArrayList<Node> caCertificates = loadCasFromKeystores();
        Tree tree = new Tree(caCertificates.get(0));
        addNodesToTree(tree, caCertificates);
        // Load issued certificates
        ArrayList<Node> issuedCertificates = loadIssuedCertificates();
        addNodesToTree(tree, issuedCertificates);

        return tree;
    }

    public ArrayList<Node> loadIssuedCertificates() {
        ArrayList<CertificateDTOs> allCertificates = new ArrayList<>();
        List<CaAddress> addresses = certificateService.getSortedCaAddresses();
        for (CaAddress address : addresses) {
            allCertificates.add(restTemplate.getForObject(address.getAddress() + "/certificate/getIssuedCertificates",
                    CertificateDTOs.class));
        }
        return certificateDTOsToNodes(allCertificates, NodeType.CERTIFICATE);
    }

    public ArrayList<Node> loadCasFromKeystores() {
        ArrayList<CertificateDTOs> allCertificates = new ArrayList<>();
        List<CaAddress> addresses = certificateService.getSortedCaAddresses();
        for (CaAddress address : addresses) {
            allCertificates.add(restTemplate.getForObject(address.getAddress() + "/certificate/getCACertificate",
                    CertificateDTOs.class));
        }
        return certificateDTOsToNodes(allCertificates, NodeType.IM_CA);
    }

    public ArrayList<CertificateDTOs> loadTrustoreCertificates() {
        ArrayList<CertificateDTOs> allCertificates = new ArrayList<>();
        List<CaAddress> addresses = certificateService.getSortedCaAddresses();
        for (CaAddress address : addresses) {
            allCertificates.add(restTemplate.getForObject(address.getAddress() + "/certificate/getTruststore",
                    CertificateDTOs.class));
        }
        return allCertificates;
    }

    public ArrayList<Node> certificateDTOsToNodes(ArrayList<CertificateDTOs> allCertificateChains, NodeType nodeType) {
        ArrayList<Node> nodes = new ArrayList<>();
        // Loop CAs
        for (CertificateDTOs certChain : allCertificateChains) {
            // Loop chains
            for (CertificateDTO cert : certChain.getCerificateList()) {
                // Deserialize chain to nodes
                CertificateDTO current = cert;
                ArrayList<Node> chainReverseList = new ArrayList<>();
                do {
                    chainReverseList.add(0, new Node(current, nodeType));
                    current = current.getParentCertificate();
                } while (current != null);
                nodes.addAll(chainReverseList);
            }
        }
        return nodes;
    }


    public Tree addNodesToTree(Tree tree, List<Node> nodes) {
        for (Node node : nodes) {
            CertificateData certificateData = certificateDataRepository.findBySerialNumber(((CertificateDTO) node.getData()).getSerialNumber());
            if (certificateData != null) {
                node.setRevoked(certificateData.getCertificateStatus().equals(CertificateStatus.REVOKED));
            }
            tree.addNode(node);
        }
        return tree;
    }

    public void makePairs(ArrayList<Pair<String, String>> pairList) {
        ArrayList<CertificateDTOs> allCertificateChains = loadTrustoreCertificates();
        List<CaAddress> addresses = certificateService.getSortedCaAddresses();
        // Loop CAs from addresses
        for (int i = 0; i < addresses.size(); i++) {
            // Loop chain form i-th address
            for (CertificateDTO cert : allCertificateChains.get(i).getCerificateList()) {
                pairList.add(new Pair<>(addresses.get(i).getSerialNumber(), cert.getSerialNumber()));
            }
        }
    }


//    ...


}
