package com.tim09.mega_travel.util;

public class Constants {

    public static final String KEY_STORE_FILE_ROOT_CA = "./files/pki/keystores/rootCA.jks";
    public static final String KEY_STORE_FILE_CA_LONDON = "./files/pki/keystores/londonCA.jks";
    public static final String KEY_STORE_FILE_CA_HONG_KONG = "./files/pki/keystores/hongKongCA.jks";
    public static final String KEY_STORE_FILE_CA_ORGANIZATIONS = "./files/pki/keystores/organizations.jks";

    public static final String ROOT_CA = "root";
    public static final String LONDON_CA = "london";
    public static final String HONG_KONG_CA = "hong-kong";
    public static final String ORGANIZATIONS = "organizations";
}
