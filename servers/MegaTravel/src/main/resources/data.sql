INSERT INTO certificate_data (id,serial_number,principal_name, certificate_status, key_store_password_salt, private_key_password_salt)
values (1,'99847536135008743148084003396461513382', 'L=Boston,C=USA,O=MegaTravel Organization Boston,CN=MegaTravel Root CA', 'GOOD', 'iiHQ8pNbOpsf8Y0IRZWKTN/UKFOXqvpMeRzjUgRB6oo=', 'qzAEogZWo1D2VG32lAungLJaA9jNKfS+H527GT+5VCM=');
INSERT INTO certificate_data (id,serial_number,principal_name, certificate_status, key_store_password_salt, private_key_password_salt)
values (2,'153111714694277618974718688578965073852', 'CN=localhost,OU=london,O=megatravel,L=london,ST=london,C=en', 'GOOD', '/fCXavpbxeIEA/xe1NY5MdjvGJDksj7X9DPGi3yzAJc=', 'XhHmRLlIRwwmreYZLH95Lk3nerU14BIWcrlpB6K8at0=');
INSERT INTO certificate_data (id,serial_number,principal_name, certificate_status, key_store_password_salt, private_key_password_salt)
values (3,'263111714694277618974718688578965073851', 'CN=localhost,OU=hk,O=megatravel,L=hk,ST=hk,C=cn', 'GOOD', '/fCXavpbxeIEA/xe1NY5MdjvGJDksj7X9DPGi3yzAJc=', 'XhHmRLlIRwwmreYZLH95Lk3nerU14BIWcrlpB6K8at0=');
INSERT INTO certificate_data (id,serial_number,principal_name, certificate_status, key_store_password_salt, private_key_password_salt)
values (4,'1555352211', 'CN=localhost,OU=root,O=megatravel,L=root,ST=root,C=rt', 'GOOD', '/eBGavpbxeSGq/xe1NY5FddvGJDkjg7X9DPGi2yzEJp=', 'YhWmRLlIYewfreYZLH95Lk3nerU14BIWarlpB6K5ez6=');


-- -- CA's
-- INSERT INTO certificate_authority (node_type,parent_id,issuer_principal_name,common_name,organization,country,city)
-- VALUES ('ROOT_CA',null,'ROOT','ROOT','MEGA_TRAVEL','RS','NOVI SAD');
-- INSERT INTO certificate_authority (node_type,parent_id,issuer_principal_name,common_name,organization,country,city)
-- VALUES ('IM_CA',1,'LONDON CA','LON','MEGA_TRAVEL','EN','LONDON');
-- INSERT INTO certificate_authority (node_type,parent_id,issuer_principal_name,common_name,organization,country,city)
-- VALUES ('IM_CA',1,'BERLIN CA','BER','MEGA_TRAVEL','DE','BERLIN');
-- INSERT INTO certificate_authority (node_type,parent_id,issuer_principal_name,common_name,organization,country,city)
-- VALUES ('IM_CA',3,'BANGKOK CA','BAN','MEGA_TRAVEL','TH','BANGKOK');

-- CA addresses
INSERT INTO ca_address (id,ca_name,address,lvl,serial_number)
VALUES (1,'london','https://localhost:8081',1,'1555352311');
INSERT INTO ca_address (id,ca_name,address,lvl,serial_number)
VALUES (2,'hk','https://localhost:8082',1,'1555413278');

-- Users
INSERT INTO `user`(id, failed_sign_ins, last_failed_sign_in, last_password_reset, password, username)
VALUES ('1', '0', NULL, NULL, '$2a$04$Ik/Re7L7p0m/ld3pbXB9Q.nTgjSehxJIvj7FnCVbB1KqG2iv1tyAa', 'pera');
INSERT INTO `user`(id, failed_sign_ins, last_failed_sign_in, last_password_reset, password, username)
VALUES ('2', '0', NULL, NULL, '$2a$04$Ik/Re7L7p0m/ld3pbXB9Q.nTgjSehxJIvj7FnCVbB1KqG2iv1tyAa', 'mika');
INSERT INTO `user`(id, failed_sign_ins, last_failed_sign_in, last_password_reset, password, username)
VALUES ('3', '0', NULL, NULL, '$2a$04$Ik/Re7L7p0m/ld3pbXB9Q.nTgjSehxJIvj7FnCVbB1KqG2iv1tyAa', 'djura');

-- Roles
INSERT INTO role (id, name) VALUES ('1', 'IDENTITY_ADMIN');
INSERT INTO role (id, name) VALUES ('2', 'ACCESS_ADMIN');
INSERT INTO role (id, name) VALUES ('3', 'SIMPLE_USER');

-- Privileges
INSERT INTO privilege (id, name) VALUES ('1', 'CREATE_CERTIFICATE');
INSERT INTO privilege (id, name) VALUES ('2', 'READ_CERTIFICATE');

-- User_Role
INSERT INTO user_role (role_id, user_id) VALUES ('1', '1');
INSERT INTO user_role (role_id, user_id) VALUES ('2', '1');
INSERT INTO user_role (role_id, user_id) VALUES ('3', '2');

-- Role_Privilege
INSERT INTO role_privilege (privilege_id, role_id) VALUES ('1', '1');
INSERT INTO role_privilege (privilege_id, role_id) VALUES ('1', '2');
INSERT INTO role_privilege (privilege_id, role_id) VALUES ('2', '1');
INSERT INTO role_privilege (privilege_id, role_id) VALUES ('2', '2');
INSERT INTO role_privilege (privilege_id, role_id) VALUES ('2', '3');

