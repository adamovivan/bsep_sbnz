package com.tim9.siem;

import org.kie.api.runtime.KieSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SiemApplication {

    @Autowired
    static KieSession kSession;

    public static void main(String[] args) {
        SpringApplication.run(SiemApplication.class, args);

        System.out.println( "Bootstrapping the Rule Engine ..." );
        // Bootstrapping a Rule Engine Session
//        KieServices ks = KieServices.Factory.get();
//        KieContainer kContainer = ks.getKieClasspathContainer();
//        KieSession kSession =  kieContainer.newKieSession("log-filter-session");
//        Log l = new Log();
//        kSession.insert(l);
//        int fired = kSession.fireAllRules();
//        System.out.println( "Number of Rules executed = " + fired );

    }

}
