package com.tim9.siem.config;

import com.tim9.siem.repository.MaliciousIPRepository;
import com.tim9.siem.service.AlarmService;
import com.tim9.siem.service.MaliciousIPService;
import com.tim9.siem.service.UserService;
import org.kie.api.KieServices;
import org.kie.api.builder.KieScanner;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieRuntime;
import org.kie.api.runtime.KieSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DroolsConfig {

    @Autowired
    MaliciousIPService maliciousIPService;

    @Autowired
    AlarmService alarmService;

    @Autowired
    UserService userService;

    @Bean
    public KieSession kieSession() {
        final KieServices kieServices = KieServices.Factory.get();

        final KieContainer kieContainer = kieServices.newKieContainer(kieServices.newReleaseId("tim09", "siem-rules", "1.0-SNAPSHOT"));
        final KieScanner kieScanner = kieServices.newKieScanner(kieContainer);
        kieScanner.start(1000L);
        KieSession ks = kieContainer.newKieSession("kieSession");
        System.out.println("USER SERVICE!");
        System.out.println(userService);
        this.setGlobals(ks);
        return ks; //kieContainer.newKieSession("kieSession");

//        final KieServices kieServices = KieServices.Factory.get();
//        final KieContainer kieContainer = kieServices.getKieClasspathContainer();
//        KieSession ks = kieContainer.newKieSession("kieSession");
//        this.setGlobals(ks);
//        return ks;
    }

    private void setGlobals(KieSession kieSession) {
        System.out.println("Setting globals!");
        kieSession.setGlobal("maliciousIPService", maliciousIPService);
        kieSession.setGlobal("alarmService", alarmService);
        kieSession.setGlobal("userService", userService);
    }
}
