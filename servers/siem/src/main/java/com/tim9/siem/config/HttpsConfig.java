//package com.tim9.siem.config;
//import org.apache.http.client.HttpClient;
//import org.apache.http.conn.ssl.NoopHostnameVerifier;
//import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
//import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
//import org.apache.http.impl.client.HttpClients;
//import org.apache.http.ssl.SSLContexts;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.core.io.Resource;
//import org.springframework.http.client.ClientHttpRequestFactory;
//import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
//import org.springframework.stereotype.Component;
//import org.springframework.web.client.RestOperations;
//import org.springframework.web.client.RestTemplate;
//
//import javax.net.ssl.*;
//import java.io.File;
//import java.io.FileInputStream;
//import java.io.IOException;
//import java.io.InputStream;
//import java.security.KeyStore;
//import java.security.KeyStoreException;
//import java.security.NoSuchAlgorithmException;
//
//@Configuration
//public class HttpsConfig {
//    /** Application HTTP client connection pool size. */
//    @Value("${application.httpclient.pool.size:10}")
//    private int httpClientPoolSize;
//
//    /** Application HTTP client keepalive duration in seconds. */
//    @Value("${application.httpclient.keepalive:120}")
//    private int httpClientKeepAlive;
//
//    /** Application keystore path. */
//    @Value("${server.ssl.key-store}")
//    private String keystore;
//
//    /** Application keystore type. */
//    @Value("${server.ssl.key-store-type}")
//    private String keystoreType;
//
//    /** Application keystore password. */
//    @Value("${server.ssl.key-store-password}")
//    private String keystorePassword;
//
//    /** Keystore alias for application client credential. */
//    @Value("${server.ssl.key-alias}")
//    private String applicationKeyAlias;
//
//    /** Application truststore path. */
//    @Value("${server.ssl.trust-store}")
//    private String truststore;
//
//    /** Application truststore type. */
//    @Value("${server.ssl.trust-store-type}")
//    private String truststoreType;
//
//    /** Application truststore password. */
//    @Value("${server.ssl.trust-store-password}")
//    private String truststorePassword;
//
//    @Bean
//    public RestTemplate restTemplate() {
//        return new RestTemplate(new HttpComponentsClientHttpRequestFactory(
//                httpClient(keystoreType, keystore, keystorePassword, applicationKeyAlias,
//                        truststoreType, truststore, truststorePassword)));
//    }
//
//    @Bean
//    public HttpClient httpClient(String keystoreType,String keystore, String keystorePassword, String alias,
//                                 String truststoreType, String truststore, String truststorePassword) {
//        try {
//            KeyStore keyStore = KeyStore.getInstance(keystoreType);
//            keyStore.load(new FileInputStream(new File(keystore)),keystorePassword.toCharArray());
//
//            KeyStore trustStore = KeyStore.getInstance(truststoreType);
//            trustStore.load(new FileInputStream(new File(truststore)),truststorePassword.toCharArray());
//
//            SSLContext sslcontext = SSLContexts.custom().loadTrustMaterial(trustStore,  null)
//                    .loadKeyMaterial(keyStore, keystorePassword.toCharArray(), (aliases, socket) -> alias)
//                    .build();
//
//            SSLConnectionSocketFactory sslsFactory = new SSLConnectionSocketFactory(sslcontext,
//                    new String[]{"TLSv1.2"},
//                    null, SSLConnectionSocketFactory.getDefaultHostnameVerifier());
//
//            return HttpClients.custom().setSSLSocketFactory(sslsFactory).build();
//        } catch (Exception e) {
//            throw new IllegalStateException("Error while configuring SSL rest template", e);
//        }
//    }
//}
