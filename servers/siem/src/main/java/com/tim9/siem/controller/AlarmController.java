package com.tim9.siem.controller;

import com.tim9.siem.model.Alarm;
import com.tim9.siem.service.AlarmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/alarm")
public class AlarmController {

    @Autowired
    AlarmService alarmService;

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Alarm> getAlarms(){
        return alarmService.getAllAlarms();
    }

}
