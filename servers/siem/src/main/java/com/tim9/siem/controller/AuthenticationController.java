package com.tim9.siem.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tim9.siem.controller.validation.ValidationUtil;
import com.tim9.siem.dto.request.AuthenticationRequest;
import com.tim9.siem.dto.response.AuthenticationResponse;
import com.tim9.siem.model.Log;
import com.tim9.siem.model.User;
import com.tim9.siem.repository.UserRepository;
import com.tim9.siem.security.spring.TokenUtils;
import com.tim9.siem.service.UserService;

import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.rule.AgendaFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;


@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/auth")
public class AuthenticationController extends ValidationUtil {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private UserService userService;

    @Autowired
    private TokenUtils tokenUtils;

    @Autowired
    private KieSession kieSession;


    public AuthenticationController(){}

    /**
     * POST /api/auth
     */
    //@CrossOrigin
    @PostMapping
    public ResponseEntity<AuthenticationResponse> authenticate(@Valid @RequestBody String authenticationRequestJSON) {
        try {

            validateJSON(authenticationRequestJSON, "user.json");
            ObjectMapper mapper = new ObjectMapper();
            AuthenticationRequest authenticationRequest = mapper.readValue(authenticationRequestJSON, AuthenticationRequest.class);

            UsernamePasswordAuthenticationToken authToken =
                    new UsernamePasswordAuthenticationToken(authenticationRequest.getUsername(), authenticationRequest.getPassword());

            Authentication authentication = authenticationManager.authenticate(authToken);

            UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getUsername());
            //LoggedUserDTO user = UserConverter.fromLoggedEntity(userService.findByUsername(userDetails.getUsername()));
            User user = userService.findByUsername(userDetails.getUsername());
            String token = tokenUtils.generateToken(userDetails);

//            kieSession.getAgenda().getAgendaGroup("user-security").setFocus();
//            Log l = new Log();
//            kieSession.insert(l);
//            kieSession.insert(user);
//            int fired = kieSession.fireAllRules();
//            System.out.println( "Number of Rules executed = " + fired );

            return new ResponseEntity<>(new AuthenticationResponse(user.getId(), token), HttpStatus.OK);
        } catch (Exception e){
            Log unsuccessfulLogin = new Log();
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }
}
