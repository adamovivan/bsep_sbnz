package com.tim9.siem.controller;

import com.tim9.siem.dto.SearchDTO;
import com.tim9.siem.dto.request.LogQueryRequest;
import com.tim9.siem.model.Log;
import com.tim9.siem.model.LogSeverity;
import com.tim9.siem.model.LogType;
import com.tim9.siem.model.User;
import com.tim9.siem.model.charts.DataPoint;
import com.tim9.siem.model.events.AntivirusEvent;
import com.tim9.siem.model.events.LoginEvent;
import com.tim9.siem.model.events.RequestEvent;
import com.tim9.siem.service.LogService;
import org.drools.core.reteoo.PathMemory;
import org.kie.api.runtime.KieSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.*;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/log")
public class LogController {

    @Autowired
    private LogService logService;

    @Autowired
    private KieSession kieSession;

    @PostMapping(consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public Integer processLog(Log log) {
        System.out.println(log.toString());
        this.logService.saveLog(log);
        return this.logService.processLog(log);
    }

    @PostMapping("/search")
    public List<Log> searchLogs(@RequestBody SearchDTO searchDTO) throws Exception {
        return logService.search(searchDTO);
    }

    @PostMapping("/testAgent")
    public void testAgent(@Valid @RequestBody String log) {
        System.out.println(log);
    }

    @GetMapping("/test")
    public void zgadija() {
        AntivirusEvent avEvent1 = new AntivirusEvent("192.168.1.0", "ANTIVIRUS","pera");
        avEvent1.setResolved(Boolean.FALSE);
        LoginEvent l1 = new LoginEvent("192.168.1.0","pera","hostname1", new Date(),true);
        LoginEvent l2 = new LoginEvent("192.168.1.0","pera","hostname1", new Date(), false);
//        ???
//        kieSession.insert(avEvent1);
//        kieSession.insert(l1);
//        kieSession.insert(l2);
//        kieSession.insert(l3);
        try {
            kieSession.fireAllRules();
        }catch (ClassCastException c){
            System.out.println(c.getMessage());
        }
//        return this.logService.test();
    }

    // DoS only endpoint
    @GetMapping("/dos")
    public Integer processOtherRequest() {
        return this.logService.logRequest(RequestEvent.RequestLocation.DOS);
    }

    // Payment endpoint
    @GetMapping("/pay")
    public Integer processPaymentRequest() {
        return this.logService.logRequest(RequestEvent.RequestLocation.PAYMENT);
    }

    // Login endpoint
    @GetMapping("/login")
    public Integer processLoginRequest() {
        return this.logService.logRequest(RequestEvent.RequestLocation.LOGIN);
    }

    @GetMapping(value="/report/{stringDate1}/{stringDate2}", produces=MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Map<String, ArrayList<DataPoint>>> getReport(@PathVariable String stringDate1, @PathVariable String stringDate2) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        try {
            LocalDate date1 = LocalDate.parse(stringDate1, formatter);
            LocalDate date2 = LocalDate.parse(stringDate2, formatter);

            Map<String, ArrayList<DataPoint>> dataPoints = this.logService.getReport(date1, date2);
            return new ResponseEntity<>(dataPoints, HttpStatus.OK);
        } catch (DateTimeParseException de) {
            return new ResponseEntity<>(new HashMap<>(), HttpStatus.NOT_ACCEPTABLE);
        }
    }


    @GetMapping(value="/workingMemoryAll")
    public ResponseEntity<List<Log>> workingMemoryAll(){
        return ResponseEntity.ok().body(logService.getAllLogsFromWorkingMemory());
    }

    @PostMapping(value="/workingMemory")
    public ResponseEntity<List<Log>> workingMemory(@RequestBody LogQueryRequest logQueryRequest){

        System.out.println(logQueryRequest);

        return ResponseEntity.ok().body(logService.getParameterizedFromWorkingMemory(logQueryRequest.getIpAddress(),
                                                                                    logQueryRequest.getHostName(),
                                                                                    logQueryRequest.getSourceName(),
                                                                                    logQueryRequest.getProcessId(),
                                                                                    logQueryRequest.getLogSeverity(),
                                                                                    logQueryRequest.getLogType()));
    }
}
