package com.tim9.siem.controller;

import com.tim9.siem.dto.request.TemplateDTO;
import com.tim9.siem.dto.response.AuthenticationResponse;
import com.tim9.siem.service.TemplateService;
import org.kie.api.runtime.KieSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/createRule")
public class TemplateController {

    @Autowired
    private TemplateService templateService;

    @Autowired
    private KieSession kieSession;

    @PostMapping()
    public ResponseEntity testAgent(@Valid @RequestBody TemplateDTO templateDTO) {
        this.templateService.createDroolsTemplate(templateDTO);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
