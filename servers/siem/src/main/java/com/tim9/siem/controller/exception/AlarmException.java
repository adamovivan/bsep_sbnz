package com.tim9.siem.controller.exception;

public class AlarmException extends RuntimeException {

    public AlarmException() {
    }

    public AlarmException(String message) {
        super(message);
    }
}
