package com.tim9.siem.dto;

public class SearchDTO {

    private String filterValue;
    private String dateValue;
    private String fieldName;

    public String getDateValue() {
        return dateValue;
    }

    public void setDateValue(String dateValue) {
        this.dateValue = dateValue;
    }

    public String getFilterValue() {
        return filterValue;
    }

    public void setFilterValue(String filterValue) {
        this.filterValue = filterValue;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    @Override
    public String toString() {
        return "SearchDTO{" +
                "filterValue='" + filterValue + '\'' +
                ", dateValue='" + dateValue + '\'' +
                ", fieldName='" + fieldName + '\'' +
                '}';
    }
}
