package com.tim9.siem.dto.request;

import com.tim9.siem.model.LogSeverity;
import com.tim9.siem.model.LogType;

public class LogQueryRequest {

    private String ipAddress;
    private String hostName;
    private String sourceName;
    private Integer processId;
    private LogSeverity logSeverity;
    private LogType logType;

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    public String getSourceName() {
        return sourceName;
    }

    public void setSourceName(String sourceName) {
        this.sourceName = sourceName;
    }

    public Integer getProcessId() {
        return processId;
    }

    public void setProcessId(Integer processId) {
        this.processId = processId;
    }

    public LogSeverity getLogSeverity() {
        return logSeverity;
    }

    public void setLogSeverity(LogSeverity logSeverity) {
        this.logSeverity = logSeverity;
    }

    public LogType getLogType() {
        return logType;
    }

    public void setLogType(LogType logType) {
        this.logType = logType;
    }

    @Override
    public String toString() {
        return "LogQueryRequest{" +
                "ipAddress='" + ipAddress + '\'' +
                ", hostName='" + hostName + '\'' +
                ", sourceName='" + sourceName + '\'' +
                ", processId=" + processId +
                ", logSeverity=" + logSeverity +
                ", logType=" + logType +
                '}';
    }
}
