package com.tim9.siem.dto.request;

import com.tim9.siem.model.Condition;

import java.util.List;

public class TemplateDTO {
    private String name;
    private int repeats;
    private int intervalValue;
    private String intervalMeasure;
    private List<Condition> conditions;

    public TemplateDTO() {
    }

    public TemplateDTO(String name, int repeats, int intervalValue, String intervalMeasure, List<Condition> conditions) {
        this.name = name;
        this.repeats = repeats;
        this.intervalValue = intervalValue;
        this.intervalMeasure = intervalMeasure;
        this.conditions = conditions;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRepeats() {
        return repeats;
    }

    public void setRepeats(int repeats) {
        this.repeats = repeats;
    }

    public int getIntervalValue() {
        return intervalValue;
    }

    public void setIntervalValue(int intervalValue) {
        this.intervalValue = intervalValue;
    }

    public List<Condition> getConditions() {
        return conditions;
    }

    public void setConditions(List<Condition> conditions) {
        this.conditions = conditions;
    }

    public String getIntervalMeasure() {
        return intervalMeasure;
    }

    public void setIntervalMeasure(String intervalMeasure) {
        this.intervalMeasure = intervalMeasure;
    }
}
