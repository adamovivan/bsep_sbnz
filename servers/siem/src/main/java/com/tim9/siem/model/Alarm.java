package com.tim9.siem.model;


import org.joda.time.DateTime;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@Entity
public class Alarm implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "message", nullable = false)
    private String message;

    @NotNull
    @Column(name = "resolved", nullable = false)
    private Boolean resolved;

    @Column(name = "resolved_at")
    @DateTimeFormat(pattern="yyyy-MM-dd'T'HH:mm:ss")
    @Temporal(TemporalType.DATE)
    private Date resolvedAt;

    @Column(name = "resolved_by")
    private String resolvedBy;

    @Column(name = "username")
    private String username;

    @Column(name="alarm_severity")
    @Enumerated(EnumType.STRING)
    private AlarmSeverity alarmSeverity;

    @Column(name="alarm_type")
    @Enumerated(EnumType.STRING)
    private AlarmType alarmType;

    public Alarm() {
    }

    public Alarm(@NotNull String message, String username, @NotNull Boolean resolved, Date resolvedAt,
                 String resolvedBy, AlarmType alarmType, AlarmSeverity alarmSeverity) {
        this.message = message;
        this.resolved = resolved;
        this.resolvedAt = resolvedAt;
        this.resolvedBy = resolvedBy;
        this.alarmSeverity = alarmSeverity;
        this.username = username;
        this.alarmType = alarmType;
    }

    public Alarm(@NotNull String message, @NotNull Boolean resolved, Date resolvedAt, String resolvedBy, String username, AlarmType alarmType) {
        this.message = message;
        this.resolved = resolved;
        this.resolvedAt = resolvedAt;
        this.resolvedBy = resolvedBy;
        this.username = username;
        this.alarmType = alarmType;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public AlarmSeverity getAlarmSeverity() {
        return alarmSeverity;
    }

    public void setAlarmSeverity(AlarmSeverity alarmSeverity) {
        this.alarmSeverity = alarmSeverity;
    }

    public AlarmType getAlarmType() {
        return alarmType;
    }

    public void setAlarmType(AlarmType alarmType) {
        this.alarmType = alarmType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getResolved() {
        return resolved;
    }

    public void setResolved(Boolean resolved) {
        this.resolved = resolved;
    }

    public Date getResolvedAt() {
        return resolvedAt;
    }

    public void setResolvedAt(Date resolvedAt) {
        this.resolvedAt = resolvedAt;
    }

    public String getResolvedBy() {
        return resolvedBy;
    }

    public void setResolvedBy(String resolvedBy) {
        this.resolvedBy = resolvedBy;
    }



}
