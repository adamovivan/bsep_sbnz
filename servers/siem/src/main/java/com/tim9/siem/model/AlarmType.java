package com.tim9.siem.model;

public enum AlarmType {
    ANTIVIRUS,
    LOGIN,
    DIFFERENT_IP_ADDRESSES
}
