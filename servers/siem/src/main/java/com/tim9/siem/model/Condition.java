package com.tim9.siem.model;

import java.util.HashMap;
import java.util.Map;

public class Condition {
    private String fieldName;
    private Object value;
    private Condition.Operator conditionOperator;
    private Condition.LogicalOperator logicalOperator;

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public Condition.Operator getConditionOperator() {
        return conditionOperator;
    }

    public void setConditionOperator(Condition.Operator conditionOperator) {
        this.conditionOperator = conditionOperator;
    }

    public Condition.LogicalOperator getLogicalOperator() {
        return logicalOperator;
    }

    public void setLogicalOperator(Condition.LogicalOperator logicalOperator) {
        this.logicalOperator = logicalOperator;
    }

    public static enum Operator {
        NOT_EQUAL_TO("NOT_EQUAL_TO"),
        EQUAL_TO("EQUAL_TO"),
        GREATER_THAN("GREATER_THAN"),
        LESS_THAN("LESS_THAN"),
        GREATER_THAN_OR_EQUAL_TO("GREATER_THAN_OR_EQUAL_TO"),
        LESS_THAN_OR_EQUAL_TO("LESS_THAN_OR_EQUAL_TO");
        private final String value;
        private static Map<String, Operator> constants = new HashMap<String, Operator>();

        static {
            for (Condition.Operator c : values()) {
                constants.put(c.value, c);
            }
        }

        private Operator(String value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return this.value;
        }

        public static Condition.Operator fromValue(String value) {
            Condition.Operator constant = constants.get(value);
            if (constant == null) {
                throw new IllegalArgumentException(value);
            } else {
                return constant;
            }
        }
    }


    public static enum LogicalOperator {
        AND("AND"),
        OR("OR"),
        NOT("NOT"),
        NONE("NONE");
        private final String value;
        private static Map<String, LogicalOperator> constants = new HashMap<String, LogicalOperator>();

        static {
            for (Condition.LogicalOperator c : values()) {
                constants.put(c.value, c);
            }
        }

        private LogicalOperator(String value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return this.value;
        }

        public static Condition.LogicalOperator fromValue(String value) {
            Condition.LogicalOperator constant = constants.get(value);
            if (constant == null) {
                throw new IllegalArgumentException(value);
            } else {
                return constant;
            }
        }
    }
}
