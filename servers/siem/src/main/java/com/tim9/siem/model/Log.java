package com.tim9.siem.model;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
public class Log implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @DateTimeFormat(pattern="yyyy-MM-dd'T'HH:mm:ss")
    @Temporal(TemporalType.DATE)
    private Date timestamp;

    private String ipAddress;

    private String hostName;

    private String sourceName;

    private String processId;

    //@ManyToOne(fetch = FetchType.LAZY)
    @Enumerated(EnumType.STRING)
    private LogSeverity logSeverity;

    @Enumerated(EnumType.STRING)
    private LogType logType;

    private String content;

    public Log() { }

    public Log(Date timestamp, String ipAddress, String hostName, String sourceName, String processId,
               LogSeverity logSeverity, String content, LogType logType) {
        this.timestamp = timestamp;
        this.ipAddress = ipAddress;
        this.hostName = hostName;
        this.sourceName = sourceName;
        this.processId = processId;
        this.logSeverity = logSeverity;
        this.content = content;
        this.logType = logType;
    }

    public LogType getLogType() {
        return logType;
    }

    public void setLogType(LogType logType) {
        this.logType = logType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    public String getSourceName() {
        return sourceName;
    }

    public void setSourceName(String sourceName) {
        this.sourceName = sourceName;
    }

    public LogSeverity getLogSeverity() {
        return logSeverity;
    }

    public void setLogSeverity(LogSeverity logSeverity) {
        this.logSeverity = logSeverity;
    }

    public String getProcessId() {
        return processId;
    }

    public void setProcessId(String processId) {
        this.processId = processId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return  logSeverity.toString() + "        " +
                ipAddress + "        " +
                processId + "        " +
                hostName + "        " +
                sourceName + "        " +
                content;
    }
}
