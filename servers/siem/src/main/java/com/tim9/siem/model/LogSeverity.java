package com.tim9.siem.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;


public enum LogSeverity {
    INFO,
    WARNING,
    ERROR,
    CRITICAL,
    DEBUG,
    FATAL;
}


//@Entity
//public class LogSeverity implements Serializable {
//    @Id
//    @GeneratedValue(strategy = GenerationType.AUTO)
//    private Long id;
//
//    private String severity;
//
//    @OneToMany(mappedBy = "logSeverity")
//    private List<Log> logs;
//
//    public LogSeverity() { }
//
//    public LogSeverity(String severity) {
//        this.severity = severity;
//    }
//
//    public LogSeverity(String severity, List<Log> logs) {
//        this.severity = severity;
//        this.logs = logs;
//    }
//
//    @Override
//    public String toString() {
//        return severity;
//    }
//
//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//        LogSeverity that = (LogSeverity) o;
//        return Objects.equals(id, that.id) &&
//                Objects.equals(severity, that.severity) &&
//                Objects.equals(logs, that.logs);
//    }
//
//    @Override
//    public int hashCode() {
//        return Objects.hash(id, severity, logs);
//    }
//
//    public Long getId() {
//        return id;
//    }
//
//    public void setId(Long id) {
//        this.id = id;
//    }
//
//    public String getSeverity() {
//        return severity;
//    }
//
//    public void setSeverity(String severity) {
//        this.severity = severity;
//    }
//
//    public List<Log> getLogs() {
//        return logs;
//    }
//
//    public void setLogs(List<Log> logs) {
//        this.logs = logs;
//    }
//}