package com.tim9.siem.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

public enum LogType {
    SYSTEM,
    APPLICATION,
    ANTIVIRUS,
    TRANSACTION,
    USER,
    SECURITY;
}

//@Entity
//public class LogType implements Serializable {
//
//    @Id
//    @GeneratedValue(strategy = GenerationType.AUTO)
//    private Long id;
//
//    private String type;
//
//    @OneToMany(mappedBy = "logType")
//    private List<Log> logs;
//
//    public LogType() {
//    }
//
//    public LogType(String type) {
//        this.type = type;
//    }
//
//    public LogType(String type, List<Log> logs) {
//        this.type = type;
//        this.logs = logs;
//    }
//
//    public Long getId() {
//        return id;
//    }
//
//    public void setId(Long id) {
//        this.id = id;
//    }
//
//    public String getType() {
//        return type;
//    }
//
//    public void setType(String type) {
//        this.type = type;
//    }
//
//    public List<Log> getLogs() {
//        return logs;
//    }
//
//    public void setLogs(List<Log> logs) {
//        this.logs = logs;
//    }
//
//}
