package com.tim9.siem.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import java.util.Timer;

@Entity
public class User implements Serializable {

    public enum Risk {
        LOW(0), MODERATE(1), HIGH(2), EXTREME(3);

        private final int value;
        private Risk(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String username;

    private String password;

    private Date lastPasswordReset;

    private int failedSignIns;

    private Date lastFailedSignIn;

    private String lastIpAddress;

    private String risk;

    @ManyToMany(mappedBy = "users", fetch = FetchType.EAGER)
    private Set<Role> roles;

    public User() {
    }

    public User(String username, String password, Date lastPasswordReset, int failedSignIns, Date lastFailedSignIn, String lastIpAddress, Set<Role> roles) {
        this.username = username;
        this.password = password;
        this.lastPasswordReset = lastPasswordReset;
        this.failedSignIns = failedSignIns;
        this.lastFailedSignIn = lastFailedSignIn;
        this.lastIpAddress = lastIpAddress;
        this.roles = roles;
        this.risk = "LOW";
    }


    public String getRisk() {
        return risk;
    }

    public void setRisk(Risk risk) {
        this.risk = risk.toString();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getLastPasswordReset() {
        return lastPasswordReset;
    }

    public void setLastPasswordReset(Date lastPasswordReset) {
        this.lastPasswordReset = lastPasswordReset;
    }

    public int getFailedSignIns() {
        return failedSignIns;
    }

    public void setFailedSignIns(int failedSignIns) {
        this.failedSignIns = failedSignIns;
    }

    public Date getLastFailedSignIn() {
        return lastFailedSignIn;
    }

    public void setLastFailedSignIn(Date lastFailedSignIn) {
        this.lastFailedSignIn = lastFailedSignIn;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public String getLastIpAddress() {
        return lastIpAddress;
    }

    public void setLastIpAddress(String lastIpAddress) {
        this.lastIpAddress = lastIpAddress;
    }
}
