package com.tim9.siem.model.alarms;

import com.tim9.siem.model.Alarm;
import com.tim9.siem.model.AlarmSeverity;
import com.tim9.siem.model.AlarmType;
import org.joda.time.DateTime;
import org.kie.api.definition.type.Expires;
import org.kie.api.definition.type.Role;
import javax.validation.constraints.NotNull;
import java.util.Date;

// TODO change expires time to 6 months
@Role(Role.Type.EVENT)
@Expires("3s")
public class UserAccountAlarm extends Alarm {

    public UserAccountAlarm() {
    }

    public UserAccountAlarm(String message, String username, AlarmSeverity alarmSeverity, AlarmType alarmType) {
        this.setMessage(message);
        this.setUsername(username);
        this.setResolved(Boolean.FALSE);
        this.setAlarmSeverity(alarmSeverity);
        this.setAlarmType(alarmType);
    }

    public UserAccountAlarm(@NotNull String message, String username, @NotNull Boolean resolved, Date resolvedAt, String resolvedBy, AlarmSeverity alarmSeverity, AlarmType alarmType) {
        super(message, username, resolved, resolvedAt, resolvedBy, alarmType, alarmSeverity);
    }

    @Override
    public String getUsername() {
        return super.getUsername();
    }

    @Override
    public void setUsername(String username) {
        super.setUsername(username);
    }

    @Override
    public AlarmSeverity getAlarmSeverity() {
        return super.getAlarmSeverity();
    }

    public void setAlarmSeverity(AlarmSeverity alarmSeverity) {
        super.setAlarmSeverity(alarmSeverity);
    }

    @Override
    public AlarmType getAlarmType() {
        return super.getAlarmType();
    }

    @Override
    public void setAlarmType(AlarmType alarmType) {
        super.setAlarmType(alarmType);
    }

    @Override
    public Long getId() {
        return super.getId();
    }

    @Override
    public void setId(Long id) {
        super.setId(id);
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }

    @Override
    public void setMessage(String message) {
        super.setMessage(message);
    }

    @Override
    public Boolean getResolved() {
        return super.getResolved();
    }

    @Override
    public void setResolved(Boolean resolved) {
        super.setResolved(resolved);
    }

    @Override
    public Date getResolvedAt() {
        return super.getResolvedAt();
    }

    @Override
    public void setResolvedAt(Date resolvedAt) {
        super.setResolvedAt(resolvedAt);
    }

    @Override
    public String getResolvedBy() {
        return super.getResolvedBy();
    }

    @Override
    public void setResolvedBy(String resolvedBy) {
        super.setResolvedBy(resolvedBy);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
    }
}
