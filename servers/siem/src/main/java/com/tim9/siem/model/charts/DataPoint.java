package com.tim9.siem.model.charts;

public class DataPoint {
    private String indexLabel;
    private int y;

    public DataPoint() {
    }

    public DataPoint(String indexLabel, int y) {
        this.indexLabel = indexLabel;
        this.y = y;
    }

    public String getIndexLabel() {
        return indexLabel;
    }

    public void setIndexLabel(String indexLabel) {
        this.indexLabel = indexLabel;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
}
