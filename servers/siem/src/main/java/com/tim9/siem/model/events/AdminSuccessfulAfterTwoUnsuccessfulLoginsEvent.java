package com.tim9.siem.model.events;

import org.kie.api.definition.type.Expires;
import org.kie.api.definition.type.Role;

@Role(Role.Type.EVENT)
@Expires("5m")
public class AdminSuccessfulAfterTwoUnsuccessfulLoginsEvent {
    private String username;

    public AdminSuccessfulAfterTwoUnsuccessfulLoginsEvent(){
    }

    public AdminSuccessfulAfterTwoUnsuccessfulLoginsEvent(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
