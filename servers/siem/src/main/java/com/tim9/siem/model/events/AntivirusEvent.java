package com.tim9.siem.model.events;

import org.kie.api.definition.type.Expires;
import org.kie.api.definition.type.Role;

@Role(Role.Type.EVENT)
@Expires("10m")
public class AntivirusEvent {
    private String ipAddress;

    private Boolean resolved;

    private String username;

    private String type;

    public AntivirusEvent() {
    }

    public AntivirusEvent(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public AntivirusEvent(String ipAddress, Boolean resolved, String type) {
        this.ipAddress = ipAddress;
        this.resolved = resolved;
        this.type = type;
    }

    public AntivirusEvent(String ipAddress, String type, String username) {
        this.ipAddress = ipAddress;
        this.username = username;
        this.type = type;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Boolean getResolved() {
        return resolved;
    }

    public void setResolved(Boolean resolved) {
        this.resolved = resolved;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}

