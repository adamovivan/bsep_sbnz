package com.tim9.siem.model.events;

import org.kie.api.definition.type.Expires;
import org.kie.api.definition.type.Role;

import java.util.Date;

@Role(Role.Type.EVENT)
@Expires("10m")
public class LoginEvent implements Event {
    private String ipAddress;
    private String username;
    private String hostName;
    private Date occurredAt;
    private boolean successful;

    public LoginEvent() {
    }

    public LoginEvent(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public LoginEvent(String ipAddress, String username) {
        this.ipAddress = ipAddress;
        this.username = username;
        this.occurredAt = new Date();
    }

    public LoginEvent(String ipAddress, String username, Date date) {
        this.ipAddress = ipAddress;
        this.username = username;
        this.occurredAt = date;
    }

    public LoginEvent(String ipAddress, String username, String hostName) {
        this.ipAddress = ipAddress;
        this.username = username;
        this.hostName = hostName;
        this.occurredAt = new Date();
    }

    public LoginEvent(String ipAddress, String username, String hostName, Date occurredAt) {
        this.ipAddress = ipAddress;
        this.username = username;
        this.hostName = hostName;
        this.occurredAt = occurredAt;
    }

    public LoginEvent(String ipAddress, String username, String hostName, Date occurredAt, boolean successful) {
        this.ipAddress = ipAddress;
        this.username = username;
        this.hostName = hostName;
        this.occurredAt = occurredAt;
        this.successful = successful;
    }

    public boolean isSuccessful() {
        return successful;
    }

    public void setSuccessful(boolean successful) {
        this.successful = successful;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    public Date getOccurredAt() {
        return occurredAt;
    }

    public void setOccurredAt(Date occurredAt) {
        this.occurredAt = occurredAt;
    }
}
