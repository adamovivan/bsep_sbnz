package com.tim9.siem.model.events;

import org.kie.api.definition.type.Expires;
import org.kie.api.definition.type.Role;

@Role(Role.Type.EVENT)
@Expires("61s")
public class RequestEvent {

    private String hostname;
    private String atIpAddress;
    private RequestLocation requestLocation;

    public enum RequestLocation {
        LOGIN(0), PAYMENT(1), DOS(2);

        private final int value;
        private RequestLocation(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }

    };

    public RequestEvent(){
    }

    public RequestEvent(String hostname, String atIpAddress, RequestLocation requestLocation) {
        this.hostname = hostname;
        this.atIpAddress = atIpAddress;
        this.requestLocation = requestLocation;
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public String getAtIpAddress() {
        return atIpAddress;
    }

    public void setAtIpAddress(String atIpAddress) {
        this.atIpAddress = atIpAddress;
    }

    public RequestEvent(RequestLocation requestLocation){
        this.requestLocation = requestLocation;
    }

    public RequestLocation getRequestLocation() {
        return requestLocation;
    }

    public void setRequestLocation(RequestLocation requestLocation) {
        this.requestLocation = requestLocation;
    }

}
