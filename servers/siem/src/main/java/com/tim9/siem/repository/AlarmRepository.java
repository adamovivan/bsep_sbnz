package com.tim9.siem.repository;

import com.tim9.siem.model.Alarm;
import com.tim9.siem.model.AlarmType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;

@Repository
public interface AlarmRepository extends JpaRepository <Alarm, Long> {
//    @Query(value = "SELECT COUNT(*) FROM alarm a WHERE l.timestamp BETWEEN ?1 AND ?2", nativeQuery = true)
//    Integer getLogsBetween(LocalDate date1, LocalDate date2, int i);
//
//    @Query(value = "SELECT COUNT(*) FROM log l WHERE l.source_name = ?3 AND source l.timestamp BETWEEN ?1 AND ?2", nativeQuery = true)
//    Integer getLogsBetweenWithSourceName(LocalDate date1, LocalDate date2, String sourceName);
//
//    @Query(value = "SELECT COUNT(*) FROM log l WHERE l.source_name = ?3 AND source l.timestamp BETWEEN ?1 AND ?2", nativeQuery = true)
//    Integer getLogsBetweenWithHostName(LocalDate date1, LocalDate date2, String hostName);
//
//    @Query(value = "SELECT COUNT(*) FROM log l WHERE l.source_name = ?3 AND source l.timestamp BETWEEN ?1 AND ?2", nativeQuery = true)
//    Integer getLogsBetweenWithIPAddress(LocalDate date1, LocalDate date2, String ipAddress);
}
