package com.tim9.siem.repository;

import com.tim9.siem.model.Log;
import com.tim9.siem.model.LogSeverity;
import com.tim9.siem.model.LogType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

import java.time.LocalDate;

@Repository
public interface LogRepository extends JpaRepository<Log, Long> {

    @Query(value = "SELECT COUNT(*) FROM log l WHERE l.timestamp BETWEEN ?1 AND ?2", nativeQuery = true)
    Integer getLogsBetween(Date date1, Date date2);

    @Query(value = "SELECT COUNT(*) FROM log l WHERE l.source_name = ?3 AND l.timestamp BETWEEN ?1 AND ?2", nativeQuery = true)
    Integer getLogsBetweenWithSourceName(Date date1, Date date2, String sourceName);

    @Query(value = "SELECT COUNT(*) FROM log l WHERE l.host_name = ?3 AND l.timestamp BETWEEN ?1 AND ?2", nativeQuery = true)
    Integer getLogsBetweenWithHostName(Date date1, Date date2, String hostName);

    @Query(value = "SELECT COUNT(*) FROM log l WHERE l.ip_address = ?3 AND l.timestamp BETWEEN ?1 AND ?2", nativeQuery = true)
    Integer getLogsBetweenWithIPAddress(Date date1, Date date2, String ipAddress);

    @Query(value = "SELECT DISTINCT(l.host_name) FROM log l WHERE l.timestamp BETWEEN ?1 AND ?2", nativeQuery = true)
    List<String> getDistinctHostName(Date date1, Date date2);

    @Query(value = "SELECT DISTINCT(l.ip_address) FROM log l WHERE l.timestamp BETWEEN ?1 AND ?2", nativeQuery = true)
    List<String> getDistinctSystems(Date date1, Date date2);

    // timestamp
    public List<Log> findAllByTimestamp(Date date);

    @Query("select l from Log l where l.timestamp >= :searchDate")
    List<Log> findAllWithCreationDateTimeAfter(
            @Param("searchDate") Date searchDate);

    // ipAddress
    public List<Log> findByIpAddressContaining(String ipAddress);
    // hostname
    public List<Log> findByHostNameContaining(String hostname);
    // source name
    public List<Log> findBySourceNameContaining(String sourcename);
    // process id
    public List<Log> findByProcessIdContaining(String pid);
    // log severity
    public List<Log> findByLogSeverity(LogSeverity logSeverity);
    // log type
    public List<Log> findByLogType(LogType logType);
    // content
    public List<Log> findByContentContaining(String content);
}
