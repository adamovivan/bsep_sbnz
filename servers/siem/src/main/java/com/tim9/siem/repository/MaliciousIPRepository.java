package com.tim9.siem.repository;

import com.tim9.siem.model.IpAddress;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MaliciousIPRepository extends JpaRepository<IpAddress,Long> {
    public int countIPaddressByAddress(String address);
}
