package com.tim9.siem.repository;

import com.tim9.siem.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    //@Query(value = "SELECT * FROM user u WHERE u.username = ?1", nativeQuery = true)
    User findByUsername(String username);

}
