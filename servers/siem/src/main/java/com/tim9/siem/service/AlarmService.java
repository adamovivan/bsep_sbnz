package com.tim9.siem.service;

import com.tim9.siem.controller.exception.AlarmException;
import com.tim9.siem.model.Alarm;
import com.tim9.siem.model.AlarmType;
import com.tim9.siem.model.events.RequestEvent;
import com.tim9.siem.model.events.RequestEvent.RequestLocation;
import com.tim9.siem.repository.AlarmRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AlarmService {

    @Autowired
    private AlarmRepository alarmRepository;

    private int dosCoefficient = 1;
    private int paymentCoefficient = 3;
    private int loginCoefficient = 5;

    public void saveAlarm(Alarm alarm) {
        alarmRepository.save(alarm);
    }

    public Alarm getAlarmById(Long id) {
        if (alarmRepository.findById(id).isPresent())
            return (alarmRepository.findById(id)).get();
        else
            throw new AlarmException("Alarm id:" + id + " not found!");
    }

    public List<Alarm> getAllAlarms() {
        return alarmRepository.findAll();
    }

    public String requestAlarmDecision(ArrayList<RequestEvent> windowEvents) {
        ArrayList<Integer> requestCount = new ArrayList<>();
        requestCount.add(0); requestCount.add(0); requestCount.add(0);
        for (RequestEvent e : windowEvents) {
            int requestTypePosition = (e.getRequestLocation()).getValue();
            requestCount.set(requestTypePosition, requestCount.get(requestTypePosition) + 1);
        }
        // Apply coefficients
        requestCount.set(0,requestCount.get(0) * loginCoefficient);
        requestCount.set(1,requestCount.get(1) * paymentCoefficient);
        requestCount.set(2,requestCount.get(2) * dosCoefficient);
        int maxValue = requestCount.get(0);
        int maxIndex = 0;
        for (int i=1;i<requestCount.size();i++){
            if(requestCount.get(i) > maxValue){
                maxValue = requestCount.get(i);
                maxIndex = i;
            }
        }
        return (RequestLocation.values()[maxIndex]).toString();
    }

    public AlarmType getAlarmType(String type){
        return AlarmType.valueOf(type);
    }

}
