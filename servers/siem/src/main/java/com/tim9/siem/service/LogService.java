package com.tim9.siem.service;

import com.tim9.siem.dto.SearchDTO;
import com.tim9.siem.model.Log;
import com.tim9.siem.model.LogSeverity;
import com.tim9.siem.model.LogType;
import com.tim9.siem.model.charts.DataPoint;
import com.tim9.siem.model.events.AntivirusEvent;
import com.tim9.siem.model.events.LoginEvent;
import com.tim9.siem.model.events.RequestEvent;
import com.tim9.siem.repository.LogRepository;
import com.tim9.siem.model.events.RequestEvent.RequestLocation;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.rule.QueryResults;
import org.kie.api.runtime.rule.QueryResultsRow;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;

import java.text.SimpleDateFormat;
import java.util.Date;

@Service
public class LogService {

    @Autowired
    private LogRepository logRepository;

    @Autowired
    private KieSession kieSession;

    public Log saveLog(Log log){
        return this.logRepository.save(log);
    }

    public Integer processLog(Log log) {
        kieSession.getAgenda().getAgendaGroup("user-login").setFocus();

        switch (log.getLogType()){
            case SYSTEM:
                break;
            case APPLICATION:
                break;
            case ANTIVIRUS:
                kieSession.insert(new AntivirusEvent(log.getIpAddress()));
                break;
            case TRANSACTION:
                break;
            case USER:
                kieSession.insert(new LoginEvent(log.getIpAddress()));
                break;
            case SECURITY:
                break;
        }

        kieSession.insert(log);

        return kieSession.fireAllRules();
    }

    public Integer test() {
        kieSession.getAgenda().getAgendaGroup("user-login").setFocus();
        kieSession.insert(new LoginEvent("192.168.1.0", "pera.peric"));
        return kieSession.fireAllRules();
        }

    public Integer logRequest(RequestLocation requestLocation){
        kieSession.insert(new RequestEvent(requestLocation));
        return kieSession.fireAllRules();
    }

    public Map<String, ArrayList<DataPoint>> getReport(LocalDate date1, LocalDate date2) {

        Map<String, ArrayList<DataPoint>> prices = new HashMap<String, ArrayList<DataPoint>>();

        String[] reportTypes = {"logsPerHost", "logsPerSystem", "alarmsPerHost", "alarmsPerSystem", "totalLogs"};

        for (String reportType: reportTypes) {
            prices.put(reportType, null);
        }

        // If dates are not valid, return map with values -1
        if (date1.isBefore(date2)) {
            prices.put("logPerHost", this.getHostPairs(asDate(date1), asDate(date2)));
            prices.put("logsPerSystem", this.getSystemPairs(asDate(date1), asDate(date2)));
            //prices.put("totalLogs", this.logRepository.getLogsBetween(date1, date2));
        }

        return prices;

    }

    private ArrayList<DataPoint> getHostPairs(Date date1, Date date2){
        //this.logRepository.getLogsBetweenWithHostName(asDate(date1), asDate(date2), "zgadija")
        ArrayList<DataPoint> dataPoints = new ArrayList<DataPoint>();
        for (String s: this.logRepository.getDistinctHostName(date1, date2)) {
            dataPoints.add(new DataPoint(s, this.logRepository.getLogsBetweenWithHostName(date1, date2, s)));
        }
        return dataPoints;
    }


    private ArrayList<DataPoint> getSystemPairs(Date date1, Date date2){
        //this.logRepository.getLogsBetweenWithHostName(asDate(date1), asDate(date2), "zgadija")
        ArrayList<DataPoint> dataPoints = new ArrayList<DataPoint>();
        for (String s: this.logRepository.getDistinctSystems(date1, date2)) {
            dataPoints.add(new DataPoint(s, this.logRepository.getLogsBetweenWithIPAddress(date1, date2, s)));
        }
        return dataPoints;
    }

    private static Date asDate(LocalDate localDate) {
        return Date.from(localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
    }

    public List<Log> search(SearchDTO searchDTO) throws Exception {
        String searchValue = searchDTO.getFilterValue();
        // Return all logs if value empty
        if(searchValue.trim().equals("") && searchDTO.getDateValue().equals("")){
            return logRepository.findAll();
        }else{
            if(searchDTO.getFieldName().equalsIgnoreCase("timestamp")){
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                Date d = format.parse(searchDTO.getDateValue());
                return logRepository.findAllWithCreationDateTimeAfter(d);
            }else if(searchDTO.getFieldName().equalsIgnoreCase("ipAddress")){
                return logRepository.findByIpAddressContaining(searchValue);
            }else if(searchDTO.getFieldName().equalsIgnoreCase("hostname")){
                return logRepository.findByHostNameContaining(searchValue);
            }else if(searchDTO.getFieldName().equalsIgnoreCase("sourceName")){
                return logRepository.findBySourceNameContaining(searchValue);
            }else if(searchDTO.getFieldName().equalsIgnoreCase("logSeverity")){
                return logRepository.findByLogSeverity(LogSeverity.valueOf(searchValue.toUpperCase()));
            }else if(searchDTO.getFieldName().equalsIgnoreCase("logType")){
                return logRepository.findByLogType(LogType.valueOf(searchValue.toUpperCase()));
            }else if(searchDTO.getFieldName().equalsIgnoreCase("content")){
                return logRepository.findByContentContaining(searchValue);
            }else if(searchDTO.getFieldName().equalsIgnoreCase("processId")){
                return logRepository.findByProcessIdContaining(searchValue);
            }else if(searchDTO.getFieldName().equalsIgnoreCase("regex")){
                System.out.println("REGEX:" + searchValue);
                return null;
            }else{
                throw new Exception("Log field: "+searchDTO.getFilterValue()+" can't be handled!");
            }
        }
    }

    public List<Log> getAllLogsFromWorkingMemory(){
        QueryResults results = kieSession.getQueryResults("Get all logs");

        List<Log> logs = new ArrayList<>();

        for(QueryResultsRow queryResult: results){
            Log log = (Log) queryResult.get("$l");
            System.out.println(log);
            logs.add(log);
        }

        return logs;
    }

    public List<Log> getParameterizedFromWorkingMemory(String ipAddress, String hostName, String sourceName,
                                                       Integer processId, LogSeverity logSeverity, LogType logType){

        // TODO DELETE OVO je samo za test
        Log l1 = new Log(new Date(),"192.168.1.5","hostname1","sourceName1",
                "123", LogSeverity.INFO,"content", LogType.SYSTEM);
        Log l2 = new Log(new Date(),"192.168.1.4","hostname1","sourceName1",
                "1000",LogSeverity.WARNING,"content",LogType.SYSTEM);
        Log l3 = new Log(new Date(),"192.168.1.5","hostname3","sourceName2",
                "123",LogSeverity.INFO,"content",LogType.ANTIVIRUS);
        Log l4 = new Log(new Date(),"192.168.1.5","hostname3","sourceName3",
                "123",LogSeverity.INFO,"content",LogType.SYSTEM);
        Log l5 = new Log(new Date(),"192.168.1.5","hostname3","sourceName1",
                "123",LogSeverity.WARNING,"content",LogType.ANTIVIRUS);

        kieSession.insert(l1);
        kieSession.insert(l2);
        kieSession.insert(l3);
        kieSession.insert(l4);
        kieSession.insert(l5);
        // ###########################################################


        QueryResults results = kieSession.getQueryResults("Get parameterized logs",
                ipAddress, hostName, sourceName, processId, logSeverity, logType);

        List<Log> logs = new ArrayList<>();

        for(QueryResultsRow queryResult: results){
            Log log = (Log) queryResult.get("$l");
            System.out.println(log);
            logs.add(log);
        }

        return logs;
    }

}
