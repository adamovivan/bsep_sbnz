package com.tim9.siem.service;

import com.tim9.siem.model.IpAddress;
import com.tim9.siem.repository.MaliciousIPRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MaliciousIPService {

    @Autowired
    MaliciousIPRepository maliciousIPRepository;

    public boolean isMalicious(String ipAddress){
        return 0 != maliciousIPRepository.countIPaddressByAddress(ipAddress);
    }

    public IpAddress addMaliciousIpAddress(String ipAddress) {
        return maliciousIPRepository.save(new IpAddress(ipAddress));
    }
}
