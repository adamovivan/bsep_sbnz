package com.tim9.siem.service;

import org.apache.maven.shared.invoker.DefaultInvocationRequest;
import org.apache.maven.shared.invoker.DefaultInvoker;
import org.apache.maven.shared.invoker.InvocationRequest;
import org.apache.maven.shared.invoker.Invoker;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.Arrays;

@Service
public class MavenService {

    public void load() {
        File pom = new File("C:\\Users\\Filip-PC\\Desktop\\IV godina\\II semestar\\BSEP\\projekat\\bsep_sbnz\\servers\\siem-rules\\pom.xml");

        if(!pom.exists())
            return;
        InvocationRequest request = new DefaultInvocationRequest();
        request.setPomFile(pom);
        request.setGoals(Arrays.asList("clean"));

        Invoker invoker = new DefaultInvoker();
        //String mavenLocation = env.getProperty(new File(System.getenv("M2_HOME")));
        //if(mavenLocation == null)
        //   return;
        System.out.println(System.getenv("M2_HOME"));
//        System.out.println(System.getProperty("maven.home"));
        invoker.setMavenHome(new File(System.getenv("M2_HOME")));
//        invoker.setMavenHome(new File("C:\\Users\\Filip-PC\\.m2\\repository\\"));


        try {
            invoker.execute(request);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            request.setGoals(Arrays.asList("install"));
            invoker.execute(request);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
