package com.tim9.siem.service;

import com.tim9.siem.model.Condition;
import com.tim9.siem.model.Rule;
import com.tim9.siem.model.events.Event;
import com.tim9.siem.model.events.LoginEvent;
import org.drools.template.ObjectDataCompiler;
import org.kie.api.runtime.KieSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@Service
public class RuleService {

    @Autowired
    private KieSession kieSession;

    public void test() throws Exception {

        // Create an event that will be tested against the rule. In reality, the event would be read from some external store.
        LoginEvent orderEvent = new LoginEvent();
        orderEvent.setIpAddress("192.168.1.0");
        orderEvent.setUsername("zgadija");

        Rule highValueOrderWidgetsIncRule = new Rule();

        Condition highValueOrderCondition = new Condition();
        highValueOrderCondition.setFieldName("username");
        highValueOrderCondition.setConditionOperator(Condition.Operator.EQUAL_TO);
        highValueOrderCondition.setValue("le.username");

        Condition widgetsIncCustomerCondition = new Condition();
        widgetsIncCustomerCondition.setFieldName("customer");
        widgetsIncCustomerCondition.setConditionOperator(Condition.Operator.EQUAL_TO);
        widgetsIncCustomerCondition.setValue("le.ipAddress");

        // In reality, you would have multiple rules for different types of events.
        // The eventType property would be used to find rules relevant to the event
        highValueOrderWidgetsIncRule.setEventType(Rule.eventType.ORDER);

        highValueOrderWidgetsIncRule.setConditions(Arrays.asList(highValueOrderCondition, widgetsIncCustomerCondition));

        String drl = this.applyRuleTemplate(orderEvent, highValueOrderWidgetsIncRule);

        System.out.println(drl);
    }

    private void evaluate(String drl, Event event) throws Exception {
//        KieServices kieServices = KieServices.Factory.get();
//        KieFileSystem kieFileSystem = kieServices.newKieFileSystem();
//        kieFileSystem.write("src/main/resources/rule.drl", drl);
//        kieServices.newKieBuilder(kieFileSystem).buildAll();
//
//        KieContainer kieContainer = kieServices.newKieContainer(kieServices.getRepository().getDefaultReleaseId());
//        StatelessKieSession statelessKieSession = kieContainer.getKieBase().newStatelessKieSession();
//
//        AlertDecision alertDecision = new AlertDecision();
//        statelessKieSession.getGlobals().set("alertDecision", alertDecision);
//        statelessKieSession.execute(event);
//
//        return alertDecision;
    }

    public String applyRuleTemplate(Event event, Rule rule) throws Exception {
        Map<String, Object> data = new HashMap<String, Object>();
        ObjectDataCompiler objectDataCompiler = new ObjectDataCompiler();

        data.put("rule", rule);
        data.put("eventType", event.getClass().getName());

        return objectDataCompiler.compile(Arrays.asList(data), Thread.currentThread().getContextClassLoader().getResourceAsStream("template.drt"));
    }

}
