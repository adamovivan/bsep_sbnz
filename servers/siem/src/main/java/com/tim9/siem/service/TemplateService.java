package com.tim9.siem.service;

import com.tim9.siem.dto.request.TemplateDTO;
import com.tim9.siem.model.Condition;
import com.tim9.siem.model.Rule;
import com.tim9.siem.model.events.Event;
import com.tim9.siem.model.events.LoginEvent;
import org.drools.template.ObjectDataCompiler;
import org.kie.api.KieBaseConfiguration;
import org.kie.api.KieServices;
import org.kie.api.builder.KieFileSystem;
import org.kie.api.builder.Message;
import org.kie.api.builder.Results;
import org.kie.api.conf.EventProcessingOption;
import org.kie.api.io.ResourceType;
import org.kie.api.runtime.KieSession;
import org.kie.internal.utils.KieHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class TemplateService {

    @Autowired
    MavenService mavenService;

    public Boolean createDroolsTemplate(TemplateDTO templateDTO){
        try {
            LoginEvent orderEvent = new LoginEvent();
            orderEvent.setIpAddress("192.168.1.0");
            orderEvent.setUsername("zgadija");

            Rule rule = new Rule();

//            Condition condition1 = new Condition();
//            condition1.setName("username");
//            condition1.setConditionOperator(Condition.Operator.EQUAL_TO);
//            condition1.setLogicalOperator(Condition.LogicalOperator.OR);
//            condition1.setValue("pera");
//
//            Condition condition2 = new Condition();
//            condition2.setName("ipAddress");
//            condition2.setConditionOperator(Condition.Operator.EQUAL_TO);
//            condition2.setLogicalOperator(Condition.LogicalOperator.AND);
//            condition2.setValue("192.168.1.0");

            // In reality, you would have multiple rules for different types of events.
            // The eventType property would be used to find rules relevant to the event
            rule.setEventType(Rule.eventType.ORDER);

            rule.setConditions(templateDTO.getConditions());

            String drl = "\n";
            drl += this.applyRuleTemplate(orderEvent, rule, String.valueOf(templateDTO.getIntervalValue()) + templateDTO.getIntervalMeasure(), templateDTO.getRepeats());
            drl += "\n";
            System.out.println(drl);

            BufferedWriter buffer = new BufferedWriter(new FileWriter("C:\\Users\\Filip-PC\\Desktop\\IV godina\\II semestar\\BSEP\\projekat\\bsep_sbnz\\servers\\siem-rules\\src\\main\\resources\\rules\\" + templateDTO.getName(), false));
            buffer.append(drl);
            buffer.close();
            mavenService.load();

            this.evaluate(drl);
            return true;
        } catch(Exception e){
            return false;
        }
    }

    public String applyRuleTemplate(Event event, Rule rule, String interval, Integer numOfOccurrences) throws Exception {
        Map<String, Object> data = new HashMap<String, Object>();
        ObjectDataCompiler objectDataCompiler = new ObjectDataCompiler();

        data.put("rule", rule);
        data.put("eventType", event.getClass().getName());
        data.put("interval", interval);
        data.put("numOfOccurrences", numOfOccurrences);

        return objectDataCompiler.compile(Arrays.asList(data), Thread.currentThread().getContextClassLoader().getResourceAsStream("template/template.drt"));
    }

    private void evaluate(String drl) throws Exception {
        KieServices kieServices = KieServices.Factory.get();
        KieFileSystem kieFileSystem = kieServices.newKieFileSystem();
        kieServices.newKieBuilder(kieFileSystem).buildAll();

        KieSession kSession = createKieSessionFromDRL(drl);

        LoginEvent loginEvent1 = new LoginEvent("192.", "pera");
        LoginEvent loginEvent2 = new LoginEvent("192.", "pera1");
        LoginEvent loginEvent3 = new LoginEvent("192.", "pera2");

        LoginEvent loginEvent4 = new LoginEvent("192.", "pera");
        LoginEvent loginEvent5 = new LoginEvent("192.", "pera");
        LoginEvent loginEvent6 = new LoginEvent("192.", "pera");


        kSession.insert(loginEvent1);
        kSession.insert(loginEvent2);
        kSession.insert(loginEvent3);

        int fired = kSession.fireAllRules();
        System.out.println(fired);

        //Thread.sleep(7000);

        kSession.insert(loginEvent4);
        kSession.insert(loginEvent5);
        kSession.insert(loginEvent6);

        fired = kSession.fireAllRules();
        System.out.println(fired);
    }


    private KieSession createKieSessionFromDRL(String drl){
        KieServices kieServices = KieServices.Factory.get();

        KieBaseConfiguration kieBaseConfig = kieServices.newKieBaseConfiguration();
        kieBaseConfig.setOption(EventProcessingOption.STREAM);

        KieHelper kieHelper = new KieHelper();
        kieHelper.addContent(drl, ResourceType.DRL);

        Results results = kieHelper.verify();

        if (results.hasMessages(Message.Level.WARNING, Message.Level.ERROR)){
            List<Message> messages = results.getMessages(Message.Level.WARNING, Message.Level.ERROR);
            for (Message message : messages) {
                System.out.println("Error: "+message.getText());
            }

            throw new IllegalStateException("Compilation errors were found. Check the logs.");
        }
        return kieHelper.build(kieBaseConfig).newKieSession();
    }

}
