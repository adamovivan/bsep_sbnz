package com.tim9.siem.service;

import com.tim9.siem.controller.exception.ForbiddenException;
import com.tim9.siem.model.Role;
import com.tim9.siem.model.User;
import com.tim9.siem.model.User.Risk;
import com.tim9.siem.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.HashMap;
import java.util.Timer;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    private HashMap<String, Timer> userRiskMap = new HashMap<>();

    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }


    public Boolean checkDate(Date date1){//, Date date2){
        Date date2 = new Date();
        long diffInMillies = Math.abs(date2.getTime() - date1.getTime());
        long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);

        //return Math.toIntExact(diff);
        System.out.println(diff);

        return diff >= 90;
    }


    public User findCurrentUser() {
        final UsernamePasswordAuthenticationToken authentication = (UsernamePasswordAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null) {
            throw new ForbiddenException("User is not authenticated!");
        }

        final UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        return findByUsername(userDetails.getUsername());
    }

    public boolean isUserAdmin(String username){
        User user = userRepository.findByUsername(username);
        if (user!=null) {
            for (Role r : user.getRoles()) {
                System.out.println(r.getName());
                if (r.getName().toLowerCase().contains("admin"))
                    return true;
            }
        }
        return false;
    }

    // User risk reset timer
    private Timer makeNewTimer(Long id, long delay) {
        Timer t = new Timer();
        t.schedule(
                new java.util.TimerTask() {
                    @Override
                    public void run() {
                        User u = userRepository.findById(id).get();
                        u.setRisk(Risk.LOW);
                        userRepository.save(u);
                        System.out.println("User risk with id:" + id + " reset to: 'LOW'");
                    }
                },
                delay
        );
        return t;
    }

    // TODO change 5s delay to 90 days
    public void userTriggeredAlarm(String username) {
        User user = userRepository.findByUsername(username);
        // Cancel old timer and start a new one
        if (user!=null) {
            if (userRiskMap.get(user.getUsername()) != null) {
                Timer t = userRiskMap.get(user.getUsername());
                t.cancel();
                userRiskMap.put(user.getUsername(), this.makeNewTimer(user.getId(), 2000)); //2s
            } else {
                // Start new timer for risk reset period
                userRiskMap.put(user.getUsername(), this.makeNewTimer(user.getId(), 2000)); //2s
            }
        }
    }

    public void setUserRisk(String username, String riskStr){
        Risk newRisk = Risk.valueOf(riskStr);
        User user = userRepository.findByUsername(username);

        if (user != null) {
            // Update user risk only if the current is lower than new
            if (Risk.valueOf(user.getRisk()).getValue() < newRisk.getValue()) {
                user.setRisk(newRisk);
                userRepository.save(user);
            }
        }
    }

    public void testA(){
        System.out.println("ITS TEST");
    }

}