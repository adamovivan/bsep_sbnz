package com.tim9.siem.service.security;

import com.tim9.siem.model.User;
import com.tim9.siem.model.security.UserDetailsImpl;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.ArrayList;
import java.util.List;

/**
 * Factory for creating instance of {@link UserDetailsImpl}.
 */
public class UserDetailsFactory {

    private UserDetailsFactory() {
    }

    /**
     * Creates UserDetailsImpl from a user.
     *
     * @param user user model
     * @return UserDetailsImpl
     */
    public static UserDetailsImpl create(User user) {
        List<GrantedAuthority> auth = new ArrayList<>();

        user.getRoles().forEach(role ->
            role.getPrivileges().forEach(privilege ->
                auth.add(new SimpleGrantedAuthority(privilege.getName()))
            )
        );

        return new UserDetailsImpl(
                user.getId(),
                user.getUsername(),
                user.getPassword(),
                auth
        );
    }
}

