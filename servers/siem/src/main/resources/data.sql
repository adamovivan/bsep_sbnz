-- Users
INSERT INTO `user`(id, failed_sign_ins, last_failed_sign_in, last_password_reset, password, username, last_ip_address,risk)
VALUES ('1', '0', NULL, NULL, '$2a$04$Ik/Re7L7p0m/ld3pbXB9Q.nTgjSehxJIvj7FnCVbB1KqG2iv1tyAa', 'pera', '192.168.1.0','LOW');
INSERT INTO `user`(id, failed_sign_ins, last_failed_sign_in, last_password_reset, password, username, last_ip_address,risk)
VALUES ('2', '0', NULL, NULL, '$2a$04$Ik/Re7L7p0m/ld3pbXB9Q.nTgjSehxJIvj7FnCVbB1KqG2iv1tyAa', 'mika', '192.168.1.0','LOW');
INSERT INTO `user`(id, failed_sign_ins, last_failed_sign_in, last_password_reset, password, username, last_ip_address,risk)
VALUES ('3', '0', NULL, NULL, '$2a$04$Ik/Re7L7p0m/ld3pbXB9Q.nTgjSehxJIvj7FnCVbB1KqG2iv1tyAa', 'djura', '192.168.1.0','LOW');

-- Roles
INSERT INTO role (id, name) VALUES ('1', 'IDENTITY_ADMIN');
INSERT INTO role (id, name) VALUES ('2', 'ACCESS_ADMIN');
INSERT INTO role (id, name) VALUES ('3', 'SIMPLE_USER');

-- Privileges
INSERT INTO privilege (id, name) VALUES ('1', 'CREATE_CERTIFICATE');
INSERT INTO privilege (id, name) VALUES ('2', 'READ_CERTIFICATE');

-- User_Role
INSERT INTO user_role (role_id, user_id) VALUES ('1', '1');
INSERT INTO user_role (role_id, user_id) VALUES ('2', '1');
INSERT INTO user_role (role_id, user_id) VALUES ('3', '2');

-- Role_Privilege
INSERT INTO role_privilege (privilege_id, role_id) VALUES ('1', '1');
INSERT INTO role_privilege (privilege_id, role_id) VALUES ('1', '2');
INSERT INTO role_privilege (privilege_id, role_id) VALUES ('2', '1');
INSERT INTO role_privilege (privilege_id, role_id) VALUES ('2', '2');
INSERT INTO role_privilege (privilege_id, role_id) VALUES ('2', '3');

-- Malicious IP Addresses
INSERT INTO ip_address (id, address) VALUES ('1','111.111.111.111');
INSERT INTO ip_address (id, address) VALUES ('2','222.222.222.222');



INSERT INTO `drools`.`log` (`id`, `content`, `host_name`, `ip_address`, `log_severity`, `log_type`, `process_id`, `source_name`, `timestamp`) VALUES
 ('1', 'User with id = 6 logged in.', 'zgadija', '188.246.61.1', '2', '0', '321', 'source', '2018-01-14 22:00:00');
INSERT INTO `drools`.`log` (`id`, `content`, `host_name`, `ip_address`, `log_severity`, `log_type`, `process_id`, `source_name`, `timestamp`) VALUES
 ('2', 'User with id = 6 logged in.', 'zgadija', '192.168.1.0', '2', '0', '321', 'source', '2019-03-14 22:00:00');
INSERT INTO `drools`.`log` (`id`, `content`, `host_name`, `ip_address`, `log_severity`, `log_type`, `process_id`, `source_name`, `timestamp`) VALUES
 ('3', 'User with id = 6 logged in.', 'zgadija', '192.168.1.0', '2', '0', '321', 'source', '2018-06-14 22:00:00');
INSERT INTO `drools`.`log` (`id`, `content`, `host_name`, `ip_address`, `log_severity`, `log_type`, `process_id`, `source_name`, `timestamp`) VALUES
 ('4', 'User with id = 6 logged in.', 'zgadija', '192.168.1.1', '2', '0', '321', 'source', '2019-05-14 22:00:00');
INSERT INTO `drools`.`log` (`id`, `content`, `host_name`, `ip_address`, `log_severity`, `log_type`, `process_id`, `source_name`, `timestamp`) VALUES
 ('5', 'User with id = 6 logged in.', 'huncutarija', '188.246.61.1', '2', '0', '321', 'source', '2018-01-14 22:00:00');
INSERT INTO `drools`.`log` (`id`, `content`, `host_name`, `ip_address`, `log_severity`, `log_type`, `process_id`, `source_name`, `timestamp`) VALUES
 ('6', 'User with id = 6 logged in.', 'promincla', '188.246.61.1', '2', '0', '321', 'source', '2019-03-14 22:00:00');
INSERT INTO `drools`.`log` (`id`, `content`, `host_name`, `ip_address`, `log_severity`, `log_type`, `process_id`, `source_name`, `timestamp`) VALUES
 ('7', 'User with id = 6 logged in.', 'huncutarija', '188.246.61.1', '2', '0', '321', 'source', '2019-06-14 22:00:00');
INSERT INTO `drools`.`log` (`id`, `content`, `host_name`, `ip_address`, `log_severity`, `log_type`, `process_id`, `source_name`, `timestamp`) VALUES
 ('8', 'User with id = 6 logged in.', 'promincla', '192.168.1.0', '2', '0', '321', 'source', '2019-06-14 22:00:00');
INSERT INTO `drools`.`log` (`id`, `content`, `host_name`, `ip_address`, `log_severity`, `log_type`, `process_id`, `source_name`, `timestamp`) VALUES
 ('9', 'User with id = 6 logged in.', 'promincla', '188.246.61.1', '2', '0', '321', 'source', '2018-06-14 22:00:00');


-- Logs
-- INSERT INTO log (id, content, host_name, ip_address, log_severity, log_type, process_id, source_name, timestamp)
-- VALUES ('5', ' User with id = 6 logged in.', 'zgadija', '188.246.61.1', 'FATAL', 'SECURITY', '123', NULL, '2019-06-20 22:00:00');
-- INSERT INTO log (id, content, host_name, ip_address, log_severity, log_type, process_id, source_name, timestamp)
-- VALUES ('100', ' User with id = 6 logged in.', 'zgadija', '188.246.61.1', 'FATAL', 'SECURITY', '123', NULL, '2019-06-20 22:00:00');

-- Alarms
-- INSERT INTO alarm (id,message,resolved,resolved_at,resolved_by,username,alarm_severity,alarm_type)
-- VALUES ('1','message1',TRUE,'2019-06-20 22:00:00','motomaniac','motomaniac1234','NORMAL','ANTIVIRUS');
-- INSERT INTO alarm (id,message,resolved,resolved_at,resolved_by,username,alarm_severity,alarm_type)
-- VALUES ('2','message2',FALSE,'2019-08-20 22:00:00','asdasd','sdads','NORMAL','LOGIN');