package com.tim9.siem.account_rules;

import com.tim9.siem.model.User;
import com.tim9.siem.model.events.AntivirusEvent;
import com.tim9.siem.model.events.LoginEvent;
import com.tim9.siem.repository.UserRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.kie.api.runtime.KieSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AccountRulesTest {

    @Autowired
    KieSession kieSession;

    @Autowired
    UserRepository userRepository;


    @Test
    public void testModerate() {
        AntivirusEvent ae = new AntivirusEvent("1.0.0.1", "MALICIOUS", "djura");
        for (int i = 0; i < 6; i++) {
            kieSession.insert(new LoginEvent("1.0.0.1","djura","hostname",new Date(),false));
        }
        kieSession.insert(ae);
        kieSession.fireAllRules();

        User u = userRepository.findByUsername("djura");
        Assert.assertEquals(u.getRisk(),"MODERATE");

        // For LOW risk reset test
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        u = userRepository.findByUsername("djura");
        Assert.assertEquals(u.getRisk(),"LOW");
    }


    @Test
    public void testHigh() {
        // NOTE: Admin account!
        // 2 uns. login events
        for (int i = 0; i < 3; i++) {
            kieSession.insert(new LoginEvent("1.0.0.1","pera","hostname",new Date(),false));
        }
        // suc login event
        kieSession.insert(new LoginEvent("1.0.0.1","pera","hostname",new Date(),true));
        kieSession.fireAllRules();

        User u = userRepository.findByUsername("pera");
        Assert.assertEquals(u.getRisk(),"HIGH");

        // For LOW risk reset test
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        u = userRepository.findByUsername("pera");
        Assert.assertEquals(u.getRisk(),"LOW");
    }



}
