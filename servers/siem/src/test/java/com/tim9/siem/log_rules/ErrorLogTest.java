package com.tim9.siem.log_rules;

import com.tim9.siem.model.Log;
import com.tim9.siem.model.LogSeverity;
import com.tim9.siem.model.LogType;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.kie.api.runtime.KieSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ErrorLogTest {

    @Autowired
    KieSession kSession;

    @Test
    public void errorLogTest(){
        Log l1 = new Log(new Date(),"192.168.1.5","hostname1","sourceName1",
                "123",LogSeverity.ERROR,"content",LogType.SYSTEM);
        Log l2 = new Log(new Date(),"192.168.1.5","hostname1","sourceName1",
                "123",LogSeverity.ERROR,"content",LogType.SYSTEM);
        Log l3 = new Log(new Date(),"192.168.1.5","hostname1","sourceName1",
                "123",LogSeverity.ERROR,"content",LogType.SYSTEM);

        kSession.insert(l1);
        kSession.insert(l2);
        kSession.insert(l3);

        int fired = kSession.fireAllRules();
        System.out.println("Fired: " + fired);
        //Assert.assertEquals( 3,fired);
    }

}
