package com.tim9.siem.login_rules;

import com.tim9.siem.model.events.AntivirusEvent;
import com.tim9.siem.model.events.LoginEvent;
import com.tim9.siem.service.AlarmService;
import com.tim9.siem.service.MaliciousIPService;
import com.tim9.siem.service.UserService;
import org.drools.core.ClockType;
import org.drools.core.time.SessionPseudoClock;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.KieSessionConfiguration;
import org.kie.api.runtime.conf.ClockTypeOption;

import java.util.Date;
import java.util.concurrent.TimeUnit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AntivirusTests {
    @Before
    public void init(){

    }

    @Autowired
    KieSession kSession;

    @Autowired
    UserService userService;

    @Autowired
    private MaliciousIPService maliciousIPService;

    @Autowired
    private AlarmService alarmService;

    @Test
    public void multipleZgadija() {
//        KieServices ks = KieServices.Factory.get();
//        KieContainer kContainer = ks.getKieClasspathContainer();
//        KieSession kSession =  kContainer.newKieSession("login-session");
//        kSession.getAgenda().getAgendaGroup("login-rules").setFocus();

        kSession.setGlobal("userService", userService);
        kSession.setGlobal("alarmService", alarmService);
        kSession.setGlobal("maliciousIPService", maliciousIPService);

        AntivirusEvent loginEvent1 = new AntivirusEvent("192.168.1.0", "WARNING","pera");
        AntivirusEvent loginEvent2 = new AntivirusEvent("192.168.1.0", "WARNING","pera");
        AntivirusEvent loginEvent3 = new AntivirusEvent("192.168.1.0", "WARNING","pera");
        AntivirusEvent loginEvent4 = new AntivirusEvent("192.168.1.0", "WARNING","pera");
        AntivirusEvent loginEvent5 = new AntivirusEvent("192.168.1.0", "MALICIOUS","pera");
        AntivirusEvent loginEvent6 = new AntivirusEvent("192.168.1.0", "MALICIOUS","pera");
        AntivirusEvent loginEvent7 = new AntivirusEvent("192.168.1.0", "MALICIOUS","pera");

        //kSession.insert(loginEvent1);
        kSession.insert(loginEvent7);
        kSession.insert(loginEvent5);
        kSession.insert(loginEvent6);


        int fired = kSession.fireAllRules();
        System.out.println("Fired: " + fired);

        kSession.insert(loginEvent1);
        kSession.insert(loginEvent2);
        kSession.insert(loginEvent3);
        fired = kSession.fireAllRules();
        System.out.println("Fired: " + fired);
    }


    @Test
    public void antivirusDetectedAndNotResoledIn1h() {
//        KieServices ks = KieServices.Factory.get();
//        KieContainer kContainer = ks.getKieClasspathContainer();
//        KieSessionConfiguration ksconf = ks.newKieSessionConfiguration();
//        ksconf.setOption(ClockTypeOption.get(ClockType.PSEUDO_CLOCK.getId()));
//        KieSession kSession = kContainer.newKieSession("kieSession", ksconf);
//        kSession.getAgenda().getAgendaGroup("login-rules").setFocus();

        kSession.setGlobal("userService", userService);
        kSession.setGlobal("alarmService", alarmService);
        kSession.setGlobal("maliciousIPService", maliciousIPService);

        AntivirusEvent loginEvent1 = new AntivirusEvent("192.168.1.0", false, "MALICIOUS");
        loginEvent1.setUsername("pera");

        kSession.insert(loginEvent1);


        int fired = kSession.fireAllRules();
        System.out.println("Fired: " + fired);


//        SessionPseudoClock clock = kSession.getSessionClock();
//        clock.advanceTime(6, TimeUnit.SECONDS);
        AntivirusEvent loginEvent3 = new AntivirusEvent("192.168.1.0", false, "MALICIOUS");
        loginEvent3.setUsername("pera");
        AntivirusEvent loginEvent2 = new AntivirusEvent("192.168.1.0", false, "MALICIOUS");
        loginEvent2.setUsername("pera");

        kSession.insert(loginEvent2);
        kSession.insert(loginEvent3);
        fired = kSession.fireAllRules();
        System.out.println("Fired: " + fired);
    }
}
