package com.tim9.siem.login_rules;

import com.tim9.siem.model.events.LoginEvent;
import com.tim9.siem.service.AlarmService;
import com.tim9.siem.service.MaliciousIPService;
import com.tim9.siem.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Multiple unsuccessful attempts for a user with a same ip address
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class SameIpAddress {

    @Autowired
    UserService userService;

    @Autowired
    KieSession kSession;

    @Autowired
    private MaliciousIPService maliciousIPService;

    @Autowired
    private AlarmService alarmService;

    @Test
    public void multipleUnsuccessfulAttemptsIpAddress() {
        kSession.setGlobal("userService", userService);
        kSession.setGlobal("alarmService", alarmService);
        kSession.setGlobal("maliciousIPService", maliciousIPService);

        LoginEvent loginEvent1 = new LoginEvent("192.168.1.1", "pera", "www.test.com");
        LoginEvent loginEvent2 = new LoginEvent("192.168.1.1", "pera", "www.test.com");
        LoginEvent loginEvent3 = new LoginEvent("192.168.1.1", "pera", "www.test.com");
        LoginEvent loginEvent4 = new LoginEvent("192.168.1.1", "pera", "www.test8.com");
        LoginEvent loginEvent5 = new LoginEvent("192.168.1.0", "mika", "www.test.com");
        LoginEvent loginEvent6 = new LoginEvent("192.168.1.1", "pera", "www.test.com");

        kSession.insert(loginEvent1);
        kSession.insert(loginEvent2);
        kSession.insert(loginEvent3);
        kSession.insert(loginEvent4);
        kSession.insert(loginEvent5);
        kSession.insert(loginEvent6);

        int fired = kSession.fireAllRules();
        System.out.println("Fired: " + fired);

//        kSession.insert(loginEvent4);
//
//        fired = kSession.fireAllRules();
//        System.out.println("Fired: " + fired);
    }


    @Test
    public void loginAfter90Days() {
        kSession.setGlobal("userService", userService);
        kSession.setGlobal("alarmService", alarmService);
        kSession.setGlobal("maliciousIPService", maliciousIPService);

        LoginEvent loginEvent1 = new LoginEvent("192.168.1.0", "WARNING", "Host", new Date());
        LoginEvent loginEvent2 = new LoginEvent("192.168.1.0", "WARNING", "Host", new Date());

        //loginEvent1.setOccurredAt(new Date());
        System.out.println("From test " + loginEvent1.getOccurredAt());
        kSession.insert(loginEvent1);
        //kSession.insert(loginEvent2);
        int fired = kSession.fireAllRules();
        System.out.println("Fired: " + fired);
    }
}
