package com.tim9.siem.login_rules;

import com.tim9.siem.model.events.LoginEvent;
import com.tim9.siem.service.AlarmService;
import com.tim9.siem.service.MaliciousIPService;
import com.tim9.siem.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Unsuccessful login attempts to the different parts of information system with the same IP address
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class SameIpAddressIS {

    @Autowired
    UserService userService;

    @Autowired
    AlarmService alarmService;

    @Autowired
    MaliciousIPService maliciousIPService;

    @Autowired
    KieSession kSession;

    @Test
    public void multipleUnsuccessfulAttempts() {
//        KieServices ks = KieServices.Factory.get();
//        KieContainer kContainer = ks.getKieClasspathContainer();
//        KieSession kSession =  kContainer.newKieSession("login-session");
//        kSession.getAgenda().getAgendaGroup("login-rules").setFocus();

//        UserService userService = new UserService();
//        AlarmService alarmService = new AlarmService();
//        MaliciousIPService maliciousIPService = new MaliciousIPService();
        kSession.setGlobal("userService", userService);
        kSession.setGlobal("alarmService", alarmService);
        kSession.setGlobal("maliciousIPService", maliciousIPService);

        LoginEvent loginEvent1 = new LoginEvent("192.168.0.1", "a");
        LoginEvent loginEvent2 = new LoginEvent("192.168.0.1", "b");
        LoginEvent loginEvent3 = new LoginEvent("192.168.0.2", "c");
        LoginEvent loginEvent4 = new LoginEvent("192.168.0.1", "d");

        kSession.insert(loginEvent1);
        kSession.insert(loginEvent2);
        kSession.insert(loginEvent3);

        int fired = kSession.fireAllRules();
        System.out.println("Fired: " + fired);

        kSession.insert(loginEvent4);

        fired = kSession.fireAllRules();
        System.out.println("Fired: " + fired);
    }
}
