package com.tim9.siem.login_rules;

import com.tim9.siem.model.events.LoginEvent;
import com.tim9.siem.service.AlarmService;
import com.tim9.siem.service.MaliciousIPService;
import com.tim9.siem.service.UserService;
import org.apache.commons.collections.bag.SynchronizedSortedBag;
import org.drools.core.ClockType;
import org.drools.core.time.SessionPseudoClock;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.kie.api.KieBase;
import org.kie.api.KieBaseConfiguration;
import org.kie.api.KieServices;
import org.kie.api.conf.EventProcessingOption;
import org.kie.api.runtime.Environment;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.KieSessionConfiguration;
import org.kie.api.runtime.conf.ClockTypeOption;
import org.kie.internal.runtime.StatefulKnowledgeSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import java.util.Collection;

import static org.junit.Assert.assertEquals;

/**
 * Unsuccessful login attempts to a system with the same username
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class SameUsername {

    @Autowired
    UserService userService;

    @Autowired
    AlarmService alarmService;

    @Autowired
    MaliciousIPService maliciousIPService;

    @Autowired
    KieSession kSession;

    @Test
    public void multipleUnsuccessfulAttempts() {
//        KieServices ks = KieServices.Factory.get();
//        KieContainer kContainer = ks.getKieClasspathContainer();

//        KieSession kSession =  kContainer.newKieSession("kieSession");
//        kSession.getAgenda().getAgendaGroup("login-rules").setFocus();

//        KieSessionConfiguration ksconf = ks.newKieSessionConfiguration();
//        ksconf.setOption(ClockTypeOption.get(ClockType.PSEUDO_CLOCK.getId()));
//        KieSession kSession = kContainer.newKieSession("kieSession", ksconf);

//        UserService userService = new UserService();
//        AlarmService alarmService = new AlarmService();
//        MaliciousIPService maliciousIPService = new MaliciousIPService();
        kSession.setGlobal("userService", userService);
        kSession.setGlobal("alarmService", alarmService);
        kSession.setGlobal("maliciousIPService", maliciousIPService);
        System.out.println(alarmService);

        Date date1 = new Date(119, 7, 11);
        Date date2 = new Date(118, 10, 11);
        Date date3 = new Date(119, 1, 11);



        LoginEvent loginEvent1 = new LoginEvent("1", "pera", date1);
        LoginEvent loginEvent2 = new LoginEvent("2", "pera");
        LoginEvent loginEvent3 = new LoginEvent("3", "pera", date3);
        LoginEvent loginEvent4 = new LoginEvent("4", "mika", date2);

        LoginEvent loginEvent5 = new LoginEvent("5", "pera");
        LoginEvent loginEvent6 = new LoginEvent("6", "pera");
        LoginEvent loginEvent7 = new LoginEvent("7", "pera");

        kSession.insert(loginEvent1);
        kSession.insert(loginEvent2);
        kSession.insert(loginEvent4);

        int fired = kSession.fireAllRules();
        System.out.println("Fired: " + fired);

        Collection<Object> zgadija = (Collection<Object>) kSession.getObjects();

        //assertEquals(29, zgadija.size());

        //kSession.insert(loginEvent3);
        fired = kSession.fireAllRules();
        System.out.println("Fired: " + fired);
//        SessionPseudoClock clock = kSession.getSessionClock();
//        clock.advanceTime(6, TimeUnit.SECONDS);

        kSession.insert(loginEvent5);
        kSession.insert(loginEvent6);
        kSession.insert(loginEvent7);

        fired = kSession.fireAllRules();
        System.out.println("Fired: " + fired);
    }
}
