package com.tim9.siem.login_rules;

import com.tim9.siem.model.events.LoginEvent;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Unsuccessful login attempts to information system with the different IP addresses
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class SameUsernameDiffIp {

    @Autowired
    private KieSession kSession;

    @Test
    public void multipleUnsuccessfulAttempts() {
//        KieServices ks = KieServices.Factory.get();
//        KieContainer kContainer = ks.getKieClasspathContainer();
//        KieSession kSession =  kContainer.newKieSession("kieSession");
//        kSession.getAgenda().getAgendaGroup("login-rules").setFocus();

        LoginEvent loginEvent1 = new LoginEvent("192.168.0.1", "pera.peric", "www.test.com");
        LoginEvent loginEvent2 = new LoginEvent("192.168.0.1", "pera.peric", "www.test.com");
        LoginEvent loginEvent3 = new LoginEvent("192.168.0.1", "pera.peric", "www.test.com");
        LoginEvent loginEvent4 = new LoginEvent("192.168.0.2", "pera.peric", "www.test.com");

        kSession.insert(loginEvent1);
        kSession.insert(loginEvent2);

        int fired = kSession.fireAllRules();
        System.out.println("Fired: " + fired);

        kSession.insert(loginEvent3);

        fired = kSession.fireAllRules();
        System.out.println("Fired: " + fired);

        kSession.insert(loginEvent4);

        fired = kSession.fireAllRules();
        System.out.println("Fired: " + fired);
    }
}
