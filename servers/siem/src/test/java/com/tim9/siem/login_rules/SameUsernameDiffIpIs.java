package com.tim9.siem.login_rules;

import com.tim9.siem.model.events.LoginEvent;
import com.tim9.siem.service.AlarmService;
import com.tim9.siem.service.MaliciousIPService;
import com.tim9.siem.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

/**
 * Unsuccessful login attempts to 2 or more different parts of information system with the different IP addresses
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class SameUsernameDiffIpIs {

    @Autowired
    UserService userService;

    @Autowired
    AlarmService alarmService;

    @Autowired
    MaliciousIPService maliciousIPService;

    @Autowired
    private KieSession kSession;

    @Test
    public void multipleUnsuccessfulAttempts() {
        kSession.setGlobal("userService", userService);
        kSession.setGlobal("alarmService", alarmService);
        kSession.setGlobal("maliciousIPService", maliciousIPService);

        LoginEvent loginEvent1 = new LoginEvent("192.168.0.1", "pera", "www.test.com", new Date());
        LoginEvent loginEvent2 = new LoginEvent("192.168.0.1", "pera", "www.test.com", new Date());
        LoginEvent loginEvent3 = new LoginEvent("192.168.0.1", "pera", "www.test2.com", new Date());
        LoginEvent loginEvent4 = new LoginEvent("192.168.0.2", "pera", "www.test.com", new Date());

        kSession.insert(loginEvent1);
        kSession.insert(loginEvent2);

        int fired = kSession.fireAllRules();
        System.out.println("Fired: " + fired);

        kSession.insert(loginEvent3);

        fired = kSession.fireAllRules();
        System.out.println("Fired: " + fired);

        kSession.insert(loginEvent4);

        fired = kSession.fireAllRules();
        System.out.println("Fired: " + fired);
    }
}
