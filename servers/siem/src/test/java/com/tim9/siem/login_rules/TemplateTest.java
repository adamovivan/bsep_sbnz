package com.tim9.siem.login_rules;

import com.tim9.siem.model.Condition;
import com.tim9.siem.model.Rule;
import com.tim9.siem.model.events.Event;
import com.tim9.siem.model.events.LoginEvent;
import org.drools.template.ObjectDataCompiler;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.kie.api.KieBaseConfiguration;
import org.kie.api.KieServices;
import org.kie.api.builder.KieFileSystem;
import org.kie.api.builder.Message;
import org.kie.api.builder.Results;
import org.kie.api.conf.EventProcessingOption;
import org.kie.api.io.ResourceType;
import org.kie.api.runtime.KieSession;
import org.kie.internal.utils.KieHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TemplateTest {
    @Autowired
    private KieSession kieSession;

//    @Autowired
//    private RuleService ruleService;

    @Test
    public void test() throws Exception {

        // Create an event that will be tested against the rule. In reality, the event would be read from some external store.
        LoginEvent orderEvent = new LoginEvent();
        orderEvent.setIpAddress("192.168.1.0");
        orderEvent.setUsername("zgadija");

        Rule rule = new Rule();

        Condition condition1 = new Condition();
        condition1.setFieldName("username");
        condition1.setConditionOperator(Condition.Operator.EQUAL_TO);
        condition1.setLogicalOperator(Condition.LogicalOperator.OR);
        condition1.setValue("pera");

        Condition condition2 = new Condition();
        condition2.setFieldName("ipAddress");
        condition2.setConditionOperator(Condition.Operator.EQUAL_TO);
        condition2.setLogicalOperator(Condition.LogicalOperator.AND);
        condition2.setValue("192.168.1.0");

        // In reality, you would have multiple rules for different types of events.
        // The eventType property would be used to find rules relevant to the event
        rule.setEventType(Rule.eventType.ORDER);

        rule.setConditions(Arrays.asList(condition1, condition2));

        String drl = this.applyRuleTemplate(orderEvent, rule, "5s", 2);

        System.out.println(drl);

        this.evaluate(drl);
    }

    public String applyRuleTemplate(Event event, Rule rule, String interval, Integer numOfOccurrences) throws Exception {
        Map<String, Object> data = new HashMap<String, Object>();
        ObjectDataCompiler objectDataCompiler = new ObjectDataCompiler();

        data.put("rule", rule);
        data.put("eventType", event.getClass().getName());
        data.put("interval", interval);
        data.put("numOfOccurrences", numOfOccurrences);

        return objectDataCompiler.compile(Arrays.asList(data), Thread.currentThread().getContextClassLoader().getResourceAsStream("template/template.drt"));
    }

    private void evaluate(String drl) throws Exception {
        KieServices kieServices = KieServices.Factory.get();
        KieFileSystem kieFileSystem = kieServices.newKieFileSystem();
        //kieFileSystem.write("C:\\Users\\Filip-PC\\Desktop\\IV godina\\II semestar\\BSEP\\projekat\\bsep_sbnz\\servers\\siem\\src\\main\\resources\\rule.drl", drl);
        kieServices.newKieBuilder(kieFileSystem).buildAll();
//
//        KieContainer kieContainer = kieServices.newKieContainer(kieServices.getRepository().getDefaultReleaseId());
//        StatelessKieSession statelessKieSession = kieContainer.getKieBase().newStatelessKieSession();
//
//        AlertDecision alertDecision = new AlertDecision();
//        statelessKieSession.getGlobals().set("alertDecision", alertDecision);
//        statelessKieSession.execute(event);
//
//        return alertDecision;
//        KieServices ks = KieServices.Factory.get();
//        KieContainer kContainer = ks.getKieClasspathContainer();
//        KieSession kSession =  kContainer.newKieSession("kieSession");

        KieSession kSession = createKieSessionFromDRL(drl);

        LoginEvent loginEvent1 = new LoginEvent("192.", "pera");
        LoginEvent loginEvent2 = new LoginEvent("192.", "pera1");
        LoginEvent loginEvent3 = new LoginEvent("192.", "pera2");

        LoginEvent loginEvent4 = new LoginEvent("192.", "pera");
        LoginEvent loginEvent5 = new LoginEvent("192.", "pera");
        LoginEvent loginEvent6 = new LoginEvent("192.", "pera");


        kSession.insert(loginEvent1);
        kSession.insert(loginEvent2);
        kSession.insert(loginEvent3);

        int fired = kSession.fireAllRules();
        System.out.println(fired);

        //Thread.sleep(7000);

        kSession.insert(loginEvent4);
        kSession.insert(loginEvent5);
        kSession.insert(loginEvent6);

        fired = kSession.fireAllRules();
        System.out.println(fired);
    }

    private KieSession createKieSessionFromDRL(String drl){
        KieServices kieServices = KieServices.Factory.get();
//
        KieBaseConfiguration kieBaseConfig = kieServices.newKieBaseConfiguration();
        kieBaseConfig.setOption(EventProcessingOption.STREAM);
//
//        KieContainer kieContainer = kieServices.getKieClasspathContainer();
//        KieBase kieBase = kieContainer.newKieBase(kieBaseConfig);

        KieHelper kieHelper = new KieHelper();
        kieHelper.addContent(drl, ResourceType.DRL);

        Results results = kieHelper.verify();

        if (results.hasMessages(Message.Level.WARNING, Message.Level.ERROR)){
            List<Message> messages = results.getMessages(Message.Level.WARNING, Message.Level.ERROR);
            for (Message message : messages) {
                System.out.println("Error: "+message.getText());
            }

            throw new IllegalStateException("Compilation errors were found. Check the logs.");
        }
        //KieBase kieBase = kieContainer.newKieBase(kieBaseConfig);

        //return kieBase.newKieSession().;
        return kieHelper.build(kieBaseConfig).newKieSession();
    }
}
