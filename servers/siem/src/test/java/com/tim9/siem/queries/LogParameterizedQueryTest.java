package com.tim9.siem.queries;

import com.tim9.siem.model.Log;
import com.tim9.siem.model.LogSeverity;
import com.tim9.siem.model.LogType;
import com.tim9.siem.service.LogService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.kie.api.runtime.KieSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class LogParameterizedQueryTest {

    @Autowired
    private KieSession kSession;

    @Autowired
    private LogService logService;

    private static boolean loaded = false;

    @Before
    public void init(){

        if(loaded){
            return;
        }

        Log l1 = new Log(new Date(),"192.168.1.5","hostname1","sourceName1",
                "123", LogSeverity.INFO,"content", LogType.SYSTEM);
        Log l2 = new Log(new Date(),"192.168.1.4","hostname1","sourceName1",
                "1000",LogSeverity.WARNING,"content",LogType.SYSTEM);
        Log l3 = new Log(new Date(),"192.168.1.5","hostname3","sourceName2",
                "123",LogSeverity.INFO,"content",LogType.ANTIVIRUS);
        Log l4 = new Log(new Date(),"192.168.1.5","hostname3","sourceName3",
                "123",LogSeverity.INFO,"content",LogType.SYSTEM);
        Log l5 = new Log(new Date(),"192.168.1.5","hostname3","sourceName1",
                "123",LogSeverity.WARNING,"content",LogType.ANTIVIRUS);

        kSession.insert(l1);
        kSession.insert(l2);
        kSession.insert(l3);
        kSession.insert(l4);
        kSession.insert(l5);

        loaded = true;
    }

    @Test
    public void queryIpAddressTest(){
        List<Log> logs = logService.getParameterizedFromWorkingMemory("192.168.1.5",
                null, null, null, null, null);
        Assert.assertEquals(4, logs.size());
    }

    @Test
    public void queryHostNameTest(){
        List<Log> logs = logService.getParameterizedFromWorkingMemory(null,
                "hostname3", null, null, null, null);
        Assert.assertEquals(3, logs.size());
    }

    @Test
    public void querySourceNameTest(){
        List<Log> logs = logService.getParameterizedFromWorkingMemory(null,
                null, "sourceName1", null, null, null);
        Assert.assertEquals(3, logs.size());
    }

    @Test
    public void queryProcessIdTest(){
        List<Log> logs = logService.getParameterizedFromWorkingMemory(null,
                null, null, 1000, null, null);
        Assert.assertEquals(1, logs.size());
    }

    @Test
    public void queryLogSeverityTest(){
        List<Log> logs = logService.getParameterizedFromWorkingMemory(null,
                null, null, null, LogSeverity.WARNING, null);
        Assert.assertEquals(2, logs.size());
    }

    @Test
    public void queryLogTypeTest(){
        List<Log> logs = logService.getParameterizedFromWorkingMemory(null,
                null, null, null, null, LogType.ANTIVIRUS);
        Assert.assertEquals(2, logs.size());
    }

    @Test
    public void queryParameterizedLogsTest(){
        List<Log> logs = logService.getParameterizedFromWorkingMemory(null,
                "hostname1", "sourceName1", null, null, LogType.SYSTEM);
        Assert.assertEquals(2, logs.size());
    }
}
