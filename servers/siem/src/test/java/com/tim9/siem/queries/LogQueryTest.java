package com.tim9.siem.queries;


import com.tim9.siem.model.Log;
import com.tim9.siem.model.LogSeverity;
import com.tim9.siem.model.LogType;
import com.tim9.siem.service.LogService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.kie.api.runtime.KieSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class LogQueryTest {

    @Autowired
    private KieSession kSession;

    @Autowired
    private LogService logService;

    @Test
    public void queryAllLogsTest(){
        Log l1 = new Log(new Date(),"192.168.1.5","hostname1","sourceName1",
                "123", LogSeverity.INFO,"content", LogType.SYSTEM);
        Log l2 = new Log(new Date(),"192.168.1.5","hostname2","sourceName1",
                "123",LogSeverity.INFO,"content",LogType.SYSTEM);
        Log l3 = new Log(new Date(),"192.168.1.5","hostname3","sourceName1",
                "123",LogSeverity.INFO,"content",LogType.SYSTEM);

        kSession.insert(l1);
        kSession.insert(l2);
        kSession.insert(l3);

        List<Log> logs = logService.getAllLogsFromWorkingMemory();
        Assert.assertEquals(3, logs.size());

    }
}
