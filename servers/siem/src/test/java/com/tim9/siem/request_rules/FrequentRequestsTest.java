package com.tim9.siem.request_rules;

import com.tim9.siem.model.events.RequestEvent;
import com.tim9.siem.service.AlarmService;
import com.tim9.siem.service.MaliciousIPService;
import com.tim9.siem.service.UserService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.kie.api.runtime.KieSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class FrequentRequestsTest {

    @Autowired
    KieSession kieSession;

    @Autowired
    UserService userService;

    @Autowired
    AlarmService alarmService;

    @Autowired
    MaliciousIPService maliciousIPService;

    @Before
    public void setUpTests(){
        kieSession.setGlobal("userService", userService);
        kieSession.setGlobal("alarmService", alarmService);
        kieSession.setGlobal("maliciousIPService", maliciousIPService);
    }

    @Test
    public void lessRequestTest() {
        // Modifier is 5
        for (int i = 0; i < 40; i++) {
            kieSession.insert(new RequestEvent("hostname1","120.130.140.150",RequestEvent.RequestLocation.LOGIN));
        }
        int fired = kieSession.fireAllRules();
        //Assert.assertEquals(0,fired);
    }

    @Test
    public void loginRequestAttackTest() {
        // Modifier is 5
        for (int i = 0; i < 60; i++) {
            kieSession.insert(new RequestEvent("hostname1","120.130.140.150",RequestEvent.RequestLocation.LOGIN));
        }
        int fired = kieSession.fireAllRules();
        System.out.println("LOGIN");
        System.out.println(fired);
    }

    @Test
    public void DOSRequestAttackTest() {
        // Modifier is 1
        for (int i = 0; i < 60; i++) {
            kieSession.insert(new RequestEvent("hostname1","120.130.140.150",RequestEvent.RequestLocation.DOS));
        }
        int fired = kieSession.fireAllRules();
        System.out.println("DOS");
        System.out.println(fired);
    }

    @Test
    public void paymentRequestAttackTest() {
        // Modifier is 3
        for (int i = 0; i < 60; i++) {
            kieSession.insert(new RequestEvent("hostname1","120.130.140.150",RequestEvent.RequestLocation.PAYMENT));
        }
        int fired = kieSession.fireAllRules();
        System.out.println("PAYMENT");
        System.out.println(fired);
    }

    @Test
    public void mixedRequestAttackTest() {
        // Modifier is 1
        for (int i = 0; i < 20; i++) {
            kieSession.insert(new RequestEvent("hostname1","120.130.140.150",RequestEvent.RequestLocation.DOS));
        }
        // Modifier is 3
        for (int i = 0; i < 10; i++) {
            kieSession.insert(new RequestEvent("hostname1","120.130.140.150",RequestEvent.RequestLocation.PAYMENT));
        }
        // Modifier is 5
        for (int i = 0; i < 21; i++) {
            kieSession.insert(new RequestEvent("hostname1","120.130.140.150",RequestEvent.RequestLocation.LOGIN));
        }
        int fired = kieSession.fireAllRules();
        System.out.println(fired);
    }


}
