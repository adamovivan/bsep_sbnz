import re

from service.read import read_logs, read_logs_linux,  get_all_events
from test import ConfigManager
import platform


if __name__ == '__main__':
    if platform.system() == "Linux":
        confManager = ConfigManager("config_linux.json")
        read_logs_linux(confManager)

    elif platform.system() == "Windows":
        get_all_events(['System', "Application", "Security"])
        confManager = ConfigManager("config.json")
        read_logs()
    # read_logs()
    # ["System", "Application", "Security"]
    #test_regex()