import json


class Log:
    def __init__(self, timestamp=None, host=None, application_name=None, message=None):
        self.timestamp = timestamp
        self.host = host
        self.application_name = application_name
        self.message = message

    def to_json(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=True, indent=4)