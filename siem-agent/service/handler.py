from watchdog.events import PatternMatchingEventHandler
from service.notify import notify
import os
import re
from datetime import datetime
import socket

from model.log import Log


class LogEventHandler(PatternMatchingEventHandler):
    def __init__(self, log_name, offset, interval):
        pattern = '*{}{}'.format(os.sep, log_name)
        print("PATTERN:",pattern)
        super(LogEventHandler, self).__init__(patterns=[pattern], ignore_directories=False, case_sensitive=True)

        self.offset = offset
        self.interval = interval
        # "^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$"
        # (([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]))?(?: +)?
        # \b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b
        self.regex = "^(?P<timestamp>.*?)?(?: +)?" \
                     "(?P<logSeverity>INFO|WARNING|ERROR|CRITICAL|DEBUG|FATAL)?(?: +)" \
                     "(?P<logType>SYSTEM|APPLICATION|ANTIVIRUS|TRANSACTION|USER|SECURITY)?(?: +)" \
                     "(?P<ipAddress>\\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\\b)?(?: +)?" \
                     "(?P<processId>[0-9]+)?(?: +)?" \
                     "(?P<hostName>.*?)?(?: +)" \
                     "(?P<content> .*)$"

        #"(?P<source_name>.*?)?(?: +)" \

        #\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b

    def on_modified(self, event):
        try:
            file_path = event.src_path
            with open(file_path, 'r') as file:
                end_of_file = os.path.getsize(file.name)
                file.seek(self.offset)
                lines = file.read(end_of_file - self.offset)
                for line in lines.split('\n'):
                    self.parse_log(line)
                    #notify(line)
                self.offset = end_of_file + 2
        except IOError:
           pass

    def parse_log(self, line):
        match = re.match(re.compile(self.regex), line)
        if match:
            notify(match.groupdict())
        else:
            log = {
                'content': line,
                'timestamp': datetime.utcnow().isoformat(),
                'logSeverity': 'UNKNOWN',
                'ipAddress': socket.gethostbyname(socket.gethostname())
            }
            notify(log)

class LogEventHandlerLinux(PatternMatchingEventHandler):

    def __init__(self, log_name, offset, filters):
        pattern = '*{}'.format(log_name)
        super(LogEventHandlerLinux, self).__init__(patterns=[pattern], ignore_patterns="",
                                                   ignore_directories=False, case_sensitive=True)

        self.on_modified = self.on_modified_handler
        self.on_moved = self.on_moved_handler
        self.on_created = self.on_created_handler
        self.on_deleted = self.on_deleted_handler

        self.offset = offset
        self.filters = filters

    def on_modified_handler(self, event):
        self.read_log(event.src_path)

    def on_moved_handler(self, event):
        pass
        self.read_log(event.dest_path)


    def on_created_handler(self, event):
        pass

    def on_deleted_handler(self, event):
        pass

    def read_log(self, file_path):
        try:
            with open(file_path, 'r') as file:
                end_of_file = os.path.getsize(file.name)
                file.seek(self.offset)
                lines = file.read(end_of_file - self.offset)
                for line in lines.split('\n'):
                    try:
                        log = self.parse_log(line)
                        if self.filter_log(log):
                            notify(log)
                    except Exception:
                        continue
                self.offset = end_of_file
        except IOError:
           pass

    def parse_log(self, line):
        tokens = line.split(" ")

        dt = datetime.strptime(tokens[0] + ' ' + tokens[1] + ' ' + tokens[2], '%b %d %H:%M:%S')
        timestamp = dt.replace(year=datetime.now().year).isoformat()
        host = tokens[3]
        application_name = tokens[4][:-1]
        message = ''
        for i in range(5, len(tokens)-1):
            message += tokens[i] + ' '
        message += tokens[-1]

        return Log(timestamp, host, application_name, message)

    def filter_log(self, log):
        for filter in self.filters:
            if not re.search(filter.application_name, log.application_name) or \
                    not re.search(filter.host, log.host) or \
                    not re.search(filter.message, log.message):
                return False
        return True
