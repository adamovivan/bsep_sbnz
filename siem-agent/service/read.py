import time
import os
from datetime import datetime, timedelta
import threading

try:
    import win32evtlog      # does not work on linux
    import win32event
except ModuleNotFoundError:
    pass

from watchdog.observers import Observer
from watchdog.observers.polling import PollingObserver

from service.handler import LogEventHandler, LogEventHandlerLinux
from utils.utils import convert_date

from threading import Lock
lock = Lock()

# za svaki slucaj
# def read_file(path):
#     batch_size = 5
#     logs = []
#     with open(path) as file:
#         while True:
#             where = file.tell()
#             line = file.readline()
#             if not line:
#                 time.sleep(1.0)
#                 file.seek(where)
#             elif batch_size > 1:
#                 logs.append(line.replace('\n', ''))
#                 if len(logs) == batch_size:
#                     yield logs
#                     logs = []
#                     time.sleep(3.0)
#             else:
#                 time.sleep(1.0)
#                 yield line


def read_logs():
    observers = []

    files = ['logs.txt', 'logs1.txt']

    for file in files:
        path = '.\\' + file
        dir_name, log_name = os.path.split(path)
        print('Listening for changes on {} in {}'.format(log_name, dir_name))

        interval = 1.0
        offset = os.path.getsize(path) + 2

        if interval > 1.0:
            observer = PollingObserver(interval)
        else:
            observer = PollingObserver(0)

        event_handler = LogEventHandler(file, offset, interval)

        observer.schedule(event_handler, dir_name)
        observer.start()
        observers.append(observer)

    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        for observer in observers:
            observer.stop()

    for observer in observers:
        observer.join()


def get_all_events(logtypes):
    #ubaciti threadove za svaki tip loga
    #logtypes = ['System', 'Application', 'Security']

    #while True:
    for logtype in logtypes:
        t = threading.Thread(target=read_windows_logs, args=(logtype,))
        t.start()
        #read_windows_logs(logtype)


def read_windows_logs1(logtype):
    print(logtype)
    server = 'localhost'
    file_handler = win32evtlog.OpenEventLog(server, logtype)
    event_handler = win32event.CreateEvent(None, 1, 0, None)
    flags = win32evtlog.EVENTLOG_FORWARDS_READ | win32evtlog.EVENTLOG_SEEK_READ
    win32evtlog.NotifyChangeEventLog(file_handler, event_handler)

    start_time = datetime.now() - timedelta(minutes=20)
    current_datetime = start_time.isoformat()

    cursor_log = win32evtlog.GetNumberOfEventLogRecords(file_handler)
    cursor_log += 1

    while True:
        #
        result = win32event.WaitForSingleObject(event_handler, 1)
        if not result:
            cursor_log = win32evtlog.GetNumberOfEventLogRecords(file_handler)
            while True:
                with lock:
                    try:
                        read_log = win32evtlog.ReadEventLog(file_handler, flags, cursor_log)
                        print(len(read_log))
                        if len(read_log) == 0:
                            print('break')
                            break
                        for event in read_log:
                            #print(event.TimeGenerated.isoformat(), current_datetime)
                            print('usao ', logtype)
                            if event.TimeGenerated.isoformat() > current_datetime:
                                print(event.Level)
                                print("%s : [%s] : %s : %s" % (event.TimeGenerated.Format(), event.RecordNumber, event.SourceName, logtype))
                                current_datetime = convert_date(event.TimeGenerated.isoformat())
                        cursor_log += len(read_log)
                    except:
                        try:
                            cursor_log -= len(read_log)
                        except:
                            pass
                        #print('usao')
                        #time.sleep(4)


def read_windows_logs(logtype):
    server = 'localhost'
    hand = win32evtlog.OpenEventLog(server, logtype)
    flags = win32evtlog.EVENTLOG_BACKWARDS_READ | win32evtlog.EVENTLOG_SEQUENTIAL_READ

    start_time = datetime.now() - timedelta(minutes=20)
    current_datetime = start_time.isoformat()

    cursor_log = win32evtlog.GetNumberOfEventLogRecords(hand)
    cursor_max = cursor_log

    while True:
        # with lock:
        # try:
            hand = win32evtlog.OpenEventLog(server, logtype)
            events = win32evtlog.ReadEventLog(hand, flags, cursor_log)
            for event in events:
                if event.TimeGenerated.isoformat() > current_datetime:
                    print("%s : [%s] : %s : %s" % (
                    event.TimeGenerated.Format(), event.RecordNumber, event.SourceName, logtype))
                    current_datetime = convert_date(event.TimeGenerated.isoformat())
            # if cursor_max >= len(events) + cursor_log:
            #     cursor_max = cursor_log
            #     cursor_log += len(events)
        # except:
        #     pass


def read_logs_linux(config):
    observers = []

    for file in config.files:

        if int(file.interval) > 1:
            observer = PollingObserver(file.interval)
        else:
            observer = PollingObserver(0)

        offset = os.path.getsize(file.path)

        event_handler = LogEventHandlerLinux(file.path, offset, file.filters)
        log_dir = os.path.dirname(file.path)     # get dir

        observer.schedule(event_handler, log_dir)
        observer.start()
        observers.append(observer)

    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        for observer in observers:
            observer.stop()

    for observer in observers:
        observer.join()
