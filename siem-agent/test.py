from collections import defaultdict
import json
import os
import time
from datetime import datetime, timedelta

try:
    import win32evtlog  # does not work on linux
    import win32event
except ModuleNotFoundError:
    pass

from watchdog.observers import Observer
from watchdog.observers.polling import PollingObserver

from service.handler import LogEventHandler, LogEventHandlerLinux
from utils.utils import convert_date


class SiemAgent():
    def __init__(self, configManager):
        self.configManager = configManager
        self.observers = []

    def run(self):
        for file in self.configManager.getAllFiles():

            dir_name, log_name = os.path.split(file.path)
            print('Listening for changes on file: {} in folder: {}'.format(log_name, dir_name))

            interval = file.interval
            offset = os.path.getsize(os.path.abspath(file.path)) + 2

            if interval > 1:
                observer = PollingObserver(interval)
            else:
                observer = PollingObserver(0)

            event_handler = LogEventHandler(log_name, offset, interval)

            observer.schedule(event_handler, dir_name)
            observer.start()
            self.observers.append(observer)

        try:
            while True:
                time.sleep(1)
        except KeyboardInterrupt:
            for observer in self.observers:
                observer.stop()

        for observer in self.observers:
            observer.join()


# Consider for ex.(reading uncomplete configs
# , default and object specific behaviours,...)
class ConfigProxy:
    def __init__(self, config, o):
        self.config = config
        self.className = o.__class__.__name__

    def get(self, key):
        if (key in self.config.keys()):
            return self.config[key]
        else:
            if (self.className == 'Filter'):
                return self.getFilterMissingKey(key)
            elif (self.className == 'File'):
                return self.getFileMissingKey(key)
            elif (self.className == 'FileGroup'):
                return self.getFileGroupMissingKey(key)
            elif (self.className == 'ConfigManager'):
                return  self.getConfigManagerMissingKey(key)
            else:
                raise Exception('Class: ' + self.className + ' not configurable!')

    def getConfigManagerMissingKey(self, key):
        # Default behaviours for keys
        return []

    def getFilterMissingKey(self, key):
        # Default behaviours for keys
        return ''

    def getFileMissingKey(self, key):
        # Default behaviours for keys
        return ''

    def getFileGroupMissingKey(self, key):
        # Default behaviours for keys
        return ''


# Filter class
class Filter():
    def __init__(self, config):
        self.configProxy = ConfigProxy(config, self)
        self.name = self.configProxy.get('name')
        self.host = self.configProxy.get('host')
        self.application_name = self.configProxy.get('application_name')
        self.message = self.configProxy.get('message')
        self.regex = self.configProxy.get('regex')

    def __str__(self):
        return str(self.configProxy.config)


# File class
class File:
    def __init__(self, config, configManager):
        self.configProxy = ConfigProxy(config, self)
        self.configManager = configManager
        self.path = self.configProxy.get('path')
        self.name = self.configProxy.get('name')
        self.interval = self.configProxy.get('interval')
        self.filterNames = self.configProxy.get('filters')
        self.getFilters()

    def getFilters(self):
        self.filters = []
        for filterName in self.filterNames:
            filter = self.configManager.getFilter(filterName)
            if (filter != None):
                self.filters.append(filter)

    def __hash__(self):
        return hash(self.path)

    def __eq__(self, other):
        return self.path == other.path

    def __str__(self):
        return str(self.configProxy.config)


# Contains list of File classes
class FileGroup:
    def __init__(self, config, configManager):
        self.files = set()
        self.filters = []
        self.configManager = configManager
        self.configProxy = ConfigProxy(config, self)
        self.name = self.configProxy.get('name')
        self.folderPath = self.configProxy.get('path')
        self.interval = self.configProxy.get('interval')
        self.filterNames = self.configProxy.get('filters')
        # Load filters
        self.loadFilters()
        # Load included/excluded files
        self.loadFiles(config)

    def loadFilters(self):
        self.filters = []
        for filterName in self.filterNames:
            filter = self.configManager.getFilter(filterName)
            if (filter != None):
                self.filters.append(filter)

    def loadFiles(self, config):
        # if included and excluded -> load included, exclude excluded
        if ('includes' in config.keys() and 'excludes' in config.keys()):
            self.loadIncluded(config['includes'])
            self.excludeFiles(config['excludes'])
        # included only
        elif ('includes' in config.keys()):
            self.loadIncluded(config['includes'])
        # excluded only -> load all and exclude else
        elif ('excludes' in config.keys()):
            self.loadAllFolderFiles()
            self.excludeFiles(config['excludes'])
        # none -> all included
        else:
            self.loadAllFolderFiles()

    def loadAllFolderFiles(self):
        # List folder files and add them like 'included'
        fileNames = (file for file in os.listdir(self.folderPath)
                     if os.path.isfile(os.path.join(self.folderPath, file)))
        self.loadIncluded(fileNames)

    def loadIncluded(self, fileNames):
        for fileName in fileNames:
            fullPath = os.path.join(self.folderPath, fileName)
            file = self.configManager.getFile(fullPath)
            # No file found, init and send to configManager
            if (file is None):
                # New file loads folders config
                file = File({'name': fileName, 'path': fullPath, 'filters': self.filterNames
                                , 'interval': self.interval}, self.configManager)
                self.configManager.addNewFile(file)
                self.files.add(file)
            else:
                self.files.add(file)

    def excludeFiles(self, fileNames):
        # Exclude files from files set
        for fileName in fileNames:
            fullPath = os.path.join(self.folderPath, fileName)
            fakeConfig = defaultdict(str)
            fakeConfig['path'] = fullPath
            file = File(fakeConfig, None)
            if (self.files.__contains__(file)):
                self.files.remove(file)
                self.configManager.removeFile(file)

    def __str__(self):
        return str(self.configProxy.config)


class ConfigManager():
    def __init__(self, jsonPath):
        self.configPath = jsonPath
        self.config = None
        # Load config
        self.loadConfig()
        # Collections
        self.filters = []
        self.files = set()
        self.fileGroups = []
        # Parse config
        self.parseConfig()

    def loadConfig(self):
        with open(self.configPath) as json_file:
            self.config = json.load(json_file)

    def saveConfig(self):
        with open(self.configPath, 'w') as configFile:
            json.dump(self.config, configFile, indent=2)

    def parseConfig(self):
        # Parse Filters
        for f in self.config['filters']:
            self.filters.append(Filter(f))
        # Parse Files
        for f in self.config['files']:
            self.files.add(File(f, self))
        # Parse FileGroups
        for f in self.config['fileGroups']:
            self.fileGroups.append(FileGroup(f, self))

    def getAllFiles(self):
        return self.files

    def addNewFile(self, file):
        self.files.add(file)

    def removeFile(self, file):
        self.files.remove(file)

    def getFilter(self, name):
        for filter in self.filters:
            if name == filter.name:
                return filter
        return None

    def getFile(self, filePath):
        for file in self.files:
            if filePath == file.path:
                return file
        return None

    def getFileGroup(self, name):
        for fileGroup in self.fileGroups:
            if name == fileGroup.name:
                return fileGroup
        return None

    def __str__(self):
        print("FileGroups:")
        for f in confManager.fileGroups:
            print(f)
        print("\nFiles:")
        for f in confManager.files:
            print(f)
        print("\nFilters:")
        for f in confManager.filters:
            print(f)
        return ""


if __name__ == '__main__':
    confManager = ConfigManager("config.json")
    print(confManager)
    agent = SiemAgent(confManager)
    agent.run()
