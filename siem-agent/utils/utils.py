import dateutil.parser
import pytz
from datetime import timedelta


def convert_date(date_iso):
    date = dateutil.parser.parse(date_iso)
    date = date + timedelta(hours=2)
    return date.astimezone(pytz.utc).isoformat()