import json
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler

class ConfigManager(FileSystemEventHandler):

    def __init__(self, jsonPath):
        self.configPath = jsonPath
        self.newConfig = True
        self.loadConfig()
        # Observe config file
        self.changeHandler = self
        self.observer = Observer()
        self.observer.schedule(self, path='.', recursive=False)
        self.observer.start()
        # self.menuData = {
        #     '1': self.run,
        #     '2': self.changeState,
        #     '3': self.exit
        # }

    def on_modified(self, event):
        # print("Config Change!")
        # Notify for new config to load later
        self.newConfig = True

    def loadConfig(self):
        with open(self.configPath) as json_file:
            self.newConfig = False
            self.config = json.load(json_file)

    def saveConfig(self):
        with open(self.configPath, 'w') as configFile:
            json.dump(self.config, configFile, indent=2)

    # def menu(self):
    #     print(self.menuData)
    #     i = input()
    #     if i in self.menuData:
    #         self.menuData[i]()
    #         self.newConfig = True
    #     else:
    #         print("Input not valid!")
    #         self.menu()

    def exit(self):
        self.saveConfig()
        print("Config saved.")
        exit(0)

    def changeState(self):
        print("Change state from:", self.config['mainStates'][self.config['currentStateIndex']])

    def __str__(self):
        print(self.config)
