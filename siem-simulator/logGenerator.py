from random import randint
from datetime import datetime, timezone


class LogWriter:

    def addToLogFile(self, log, filePath):
        f = open(filePath, 'a+')
        f.write('\n' + log)
        f.close()


class LogGenerator:

    def __init__(self):
        self.logNumber = 0

    def generateLog(self, *args):
        line = ''
        for i in range(0, len(args) - 1):
            line += args[i]
            if i != len(args) - 2:
                line += 4 * ' '
                # Append log payload
        return line + 4 * ' ' + args[-1]

    def getLogNumber(self):
        self.logNumber += 1
        return str(self.logNumber)

    def getLogType(self, arg):
        if arg == 'info':
            return "INFO"
        elif arg == 'error':
            return "ERROR"
        elif arg == 'warning':
            return "WARNING"
        else:
            raise Exception('Invalid argument:' + arg)

    def getIPAddress(self):
        retVal = ''
        for i in range(4):
            n = randint(1, 255)
            if i == 3:
                n = randint(1, 9)
            retVal += str(n)
            if i != 3:
                retVal += '.'
        return retVal

    def getUTCTime(self):
        day = randint(1, 28)
        month = randint(5, 12)
        year = 2019
        hour = randint(0, 23)
        minute = randint(0, 59)
        return str(datetime(year, month, day, hour=hour, minute=minute, tzinfo=timezone.utc).isoformat())

    def getHostName(self):
        return 'hostname' + str(randint(1, 10))

    def getSourceName(self):
        return 'sourcename' + str(randint(1, 10))

    # def setPayload(self, **kwargs):
    #     retVal = ''
    #     for key, value in kwargs.items():
    #         retVal = key + '=' + value + ','
    #     return retVal
