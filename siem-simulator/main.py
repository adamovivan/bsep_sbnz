from simStateMachine import SimStateMachine
from config_manager import ConfigManager
from logGenerator import LogGenerator, LogWriter


def main():
    configManager = ConfigManager("config.json")
    logGenerator = LogGenerator()
    logWriter = LogWriter()
    simulator = SimStateMachine(configManager, logGenerator, logWriter)
    simulator.run()


if __name__ == '__main__':
    main()
