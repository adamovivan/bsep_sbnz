import states
import time
from random import randint


class SimStateMachine:
    def __init__(self, config, logGenerator, logWriter):
        self.setConfig(config)
        self.logGenerator = logGenerator
        self.logWriter = logWriter

    def setConfig(self, config):
        self.configManager = config
        self.config = self.configManager.config
        # load initial mode
        self.mode = self.config['modes'][self.config['currentModeIndex']]
        # mode specific config
        self.modeConfig = self.config[self.mode]
        # next possible states
        self.nextStateNames = self.modeConfig['states']

    def loadNewConfig(self):
        self.configManager.loadConfig()
        self.setConfig(self.configManager)

    def changeMode(self):
        if (self.mode == 'Normal'):
            self.mode = 'Attack'
        else:
            self.mode = 'Normal'

    def changeSimState(self):
        index = randint(0, len(self.nextStateNames) - 1)
        className = self.nextStateNames[index]
        class_ = getattr(states, className)
        # Can pass config to state here
        self.currentState = class_(self, self.config[className])

    def runNextState(self):
        self.changeSimState()
        self.currentState.run()

    # Simulate work
    def run(self):
        while True:
            time.sleep(self.modeConfig['logTime'])
            self.runNextState()
            # config changed -> load new config
            if (self.configManager.newConfig):
                self.loadNewConfig()
