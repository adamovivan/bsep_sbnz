class State:

    def __init__(self, stateMachine, config):
        self.stateMachine = stateMachine
        self.config = config
        self.logGenerator = stateMachine.logGenerator

    def run(self):
        assert 0, "run not implemented"

    def next(self):
        assert 0, "next not implemented"