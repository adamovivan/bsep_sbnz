from state import State

class InfoLogState(State):
    def __init__(self, stateMachine,  config):
        super(InfoLogState, self).__init__(stateMachine, config)

    def run(self):
        log = self.logGenerator.generateLog(
            self.logGenerator.getUTCTime(),
            self.logGenerator.getLogType('info'),
            self.logGenerator.getIPAddress(),
            self.logGenerator.getLogNumber(),
            # self.logGenerator.getHostName(),
            self.logGenerator.getSourceName(),
            'hello info!'
        )
        self.stateMachine.logWriter.addToLogFile(log, self.config['filePath'])
        print("I am InfoLogState")

    def next(self):
        self.stateMachine.getNextState(self)


class ErrorLogState(State):
    def __init__(self, stateMachine, config):
        super(ErrorLogState, self).__init__(stateMachine, config)

    def run(self):
        log = self.logGenerator.generateLog(
            self.logGenerator.getUTCTime(),
            self.logGenerator.getLogType('error'),
            self.logGenerator.getIPAddress(),
            self.logGenerator.getLogNumber(),
            # self.logGenerator.getHostName(),
            self.logGenerator.getSourceName(),
            'hello error!'
        )
        self.stateMachine.logWriter.addToLogFile(log, self.config['filePath'])
        print("I am ErrorLogState")

    def next(self):
        self.stateMachine.getNextState(self)


# class State3(State):
#     def __init__(self, stateMachine, config):
#         super(State3, self).__init__(stateMachine, config)
#
#     def run(self):
#         print("I am State4")
#
#     def next(self):
#         self.stateMachine.getNextState(self)
#
#
# class State4(State):
#     def __init__(self, stateMachine, config):
#         super(State4, self).__init__(stateMachine, config)
#
#     def run(self):
#         print("I am State4")
#
#     def next(self):
#         self.stateMachine.getNextState(self)
